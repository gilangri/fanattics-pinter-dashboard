/* eslint-disable @typescript-eslint/no-unused-vars */
import Axios from "axios";
import { DecodeToken } from "../../configs/jwt";
import { API_URL, optionsFormUrlencoded } from "../../constants";
import { SIDEBAR_ROLE } from "../sidebar/types";
import { AUTH_REQUEST, AUTH_LOGIN, AUTH_FAILED, AUTH_RESET } from "./types";

interface Props {
  payload?: any;
}

export const handleLogin = ({ payload }: Props) => {
  return async (dispatch: any) => {
    dispatch({ type: AUTH_REQUEST });
    try {
      if (!payload.email || !payload.password) {
        let errMsg = "Email / Password tidak boleh kosong";
        return dispatch({ type: AUTH_FAILED, payload: errMsg });
      }

      const { data } = await Axios.post(`${API_URL}/admins/login`, payload);

      if (data.result.role === "super-pinter") {
        dispatch({ type: SIDEBAR_ROLE, payload: data.result.role });
        dispatch({ type: AUTH_LOGIN, payload: data.result });
        localStorage.setItem("token", JSON.stringify(data.result));
      } else {
        throw new Error("Only accessible for Pinter Admin");
      }
    } catch (err) {
      console.log("err", err.response);
      dispatch({
        type: AUTH_FAILED,
        payload: err?.response?.data?.message || err?.message || "failed",
      });
    }
  };
};

export const ReloginAction = (token: any) => {
  return (dispatch: any) => {
    if (token !== undefined) {
      let payload = JSON.parse(token);
      // console.log("payload", payload);
      dispatch({ type: SIDEBAR_ROLE, payload: payload.role });
      return dispatch({ type: AUTH_LOGIN, payload });
    } else {
      return dispatch({ type: AUTH_FAILED, payload: "User not authorized" });
    }
  };
};

export const LogoutAction = () => {
  return (dispatch: any) => {
    localStorage.removeItem("token");
    dispatch({ type: AUTH_RESET });
  };
};
