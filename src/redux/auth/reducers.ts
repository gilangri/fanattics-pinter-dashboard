import {
  AUTH_FAILED,
  AUTH_LOGIN,
  AUTH_REQUEST,
  AUTH_RESET,
  AUTH_SET,
  AUTH_SET_VALUE,
  IAuthAction,
  IAuthState,
} from "./types";

const init_state: IAuthState = {
  email: "",
  password: "",
  user: undefined,

  loadingLogin: false,
  loadingForgot: false,
  error: "",
  cookieChecked: false,
};

export function authReducer(
  state = init_state,
  { payload, type }: IAuthAction
): IAuthState {
  switch (type) {
    case AUTH_REQUEST:
      return { ...state, loadingLogin: true, error: "" };
    case AUTH_FAILED:
      return { ...state, loadingLogin: false, error: payload };

    case AUTH_SET:
      return { ...state, ...payload };
    case AUTH_SET_VALUE:
      return { ...state, [payload.names]: payload.value };

    case AUTH_LOGIN:
      return { ...state, loadingLogin: false, user: payload };

    case AUTH_RESET:
      return init_state;
    default:
      return state;
  }
}
