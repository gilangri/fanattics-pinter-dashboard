import { AnyAction } from "redux";
import { IAdmin } from "../_interface/admin";

export interface IAuthState {
  email: string;
  password: string;

  user?: IAdmin;

  loadingLogin: boolean;
  loadingForgot: boolean;
  error: string;
  cookieChecked: boolean;
}

export const AUTH_REQUEST = "AUTH_REQUEST";
export const AUTH_FAILED = "AUTH_FAILED";
export const AUTH_RESET = "AUTH_RESET";
export const AUTH_LOGIN = "AUTH_LOGIN";
export const AUTH_SET = "AUTH_SET";
export const AUTH_SET_VALUE = "AUTH_SET_VALUE";

export interface IAuthAction extends AnyAction {
  payload: any;
  name: string;
  type:
    | typeof AUTH_REQUEST
    | typeof AUTH_FAILED
    | typeof AUTH_RESET
    | typeof AUTH_LOGIN
    | typeof AUTH_SET
    | typeof AUTH_SET_VALUE;
}
