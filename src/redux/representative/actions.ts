
import axios from "axios";
import { API_URL } from "../../constants";
import {
  REPRESENTATIVE_FAILED,
  REPRESENTATIVE_REQUEST,
} from "./types";

export const createRepresentative = (body: object) => async (dispatch: any) => {
  try {
    dispatch({ type: REPRESENTATIVE_REQUEST });

    await axios.post(`${API_URL}/representatives`, body);
  } catch (err) {
    console.log(err);
    dispatch({ type: REPRESENTATIVE_FAILED, payload: err });
  }
};
