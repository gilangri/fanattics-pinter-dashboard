import {
  IRepresentativeAction,
  IRepresentativeState,
  REPRESENTATIVE_FAILED,
  REPRESENTATIVE_REQUEST,
  REPRESENTATIVE_RESET,
  REPRESENTATIVE_SET,
  REPRESENTATIVE_SET_REPRESENTATIVE,
  REPRESENTATIVE_SET_REPRESENTATIVES,
  REPRESENTATIVE_SET_VALUE,
} from "./types";

const init_state: IRepresentativeState = {
  representatives: [],
  representative: undefined,
  loading: false,
  error: "",
};

export function representativeReducer(
  state = init_state,
  { payload, name, type }: IRepresentativeAction
): IRepresentativeState {
  switch (type) {
    case REPRESENTATIVE_REQUEST:
      return { ...state, loading: true, error: "" };
    case REPRESENTATIVE_FAILED:
      return { ...state, loading: false, error: payload };
    case REPRESENTATIVE_SET_REPRESENTATIVES:
      return { ...state, loading: false, representatives: payload };
    case REPRESENTATIVE_SET_REPRESENTATIVE:
      return { ...state, loading: false, representative: payload };
    case REPRESENTATIVE_SET:
      return { ...state, ...payload };
    case REPRESENTATIVE_SET_VALUE:
      return { ...state, [name]: payload };
    case REPRESENTATIVE_RESET:
      return init_state;
    default:
      return state;
  }
}
