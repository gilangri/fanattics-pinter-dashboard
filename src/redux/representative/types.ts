import { AnyAction } from "redux";
import { IRepresentative } from "../_interface/representative";

export type TCreateRepresentative = {
  distributor: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  jobPosition: string;
  createdAt: string;
  updatedAt: string;
};

export interface IRepresentativeState {
  representatives: IRepresentative[];
  representative?: IRepresentative;

  loading: boolean;
  error: string;
}

export const REPRESENTATIVE_REQUEST = "REPRESENTATIVE_REQUEST";
export const REPRESENTATIVE_FAILED = "REPRESENTATIVE_FAILED";
export const REPRESENTATIVE_RESET = "REPRESENTATIVE_RESET";

export const REPRESENTATIVE_SET_REPRESENTATIVES =
  "REPRESENTATIVE_SET_REPRESENTATIVES";
export const REPRESENTATIVE_SET_REPRESENTATIVE = "REPRESENTATIVE_SET_REPRESENTATIVE";

export const REPRESENTATIVE_SET = "REPRESENTATIVE_SET";
export const REPRESENTATIVE_SET_VALUE = "REPRESENTATIVE_SET_VALUE";

export interface IRepresentativeAction extends AnyAction {
  payload: any;
  name: string;
  type:
    | typeof REPRESENTATIVE_REQUEST
    | typeof REPRESENTATIVE_FAILED
    | typeof REPRESENTATIVE_RESET

    //
    | typeof REPRESENTATIVE_SET_REPRESENTATIVE
    | typeof REPRESENTATIVE_SET_REPRESENTATIVES
    //
    | typeof REPRESENTATIVE_SET
    | typeof REPRESENTATIVE_SET_VALUE;
}
