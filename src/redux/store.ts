import { applyMiddleware, combineReducers, createStore, Store } from "redux";
import thunk from "redux-thunk";
import { sidebarReducer } from "./sidebar/reducers";
import { authReducer } from "./auth/reducers";
import { partnersReducer } from "./partners/reducers";
import { representativeReducer } from "./representative/reducers";
import { coPartnersReducer } from "./coPartners/reducers";

//

import { distributorReducer } from "../pages/Distributor/_state/reducers/distributor";
import { distributorFormReducer } from "../pages/Distributor/_state/reducers/distributorForm";

import { companyPartnerReducer } from "../pages/CompanyPartner/_state/reducers/companyPartner";
import { companyPartnerFormReducer } from "../pages/CompanyPartner/_state/reducers/companyPartnerForm";
import { adminReducer } from "../pages/Admin/_state/reducers/admin";
import { adminFormReducer } from "../pages/Admin/_state/reducers/adminForm";
import { userReducer } from "../pages/Member/_state/reducers/member";
import { userFormReducer } from "../pages/Member/_state/reducers/memberForm";
import { representativeFormReducer } from "../pages/Distributor/_state/reducers/representativeForm";

const rootReducer = combineReducers({
  sidebar: sidebarReducer,
  auth: authReducer,

  distributor: distributorReducer,
  distributorForm: distributorFormReducer,
  representativeForm: representativeFormReducer,

  admin: adminReducer,
  adminForm: adminFormReducer,

  member: userReducer,
  memberForm: userFormReducer,

  partners: partnersReducer,
  representative: representativeReducer,
  coPartners: coPartnersReducer,
  companyPartner: companyPartnerReducer,
  companyPartnerForm: companyPartnerFormReducer,
});

export type RootState = ReturnType<typeof rootReducer>;

export function configureStore(): Store<RootState> {
  const store = createStore(rootReducer, undefined, applyMiddleware(thunk));
  return store;
}

export * from "./_interface";
