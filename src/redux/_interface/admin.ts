import { ICompanyPartner } from "./companyPartner";
import { IDistributor } from "./distributor";

/**
 * role: "super" | "product" | "sale" | "courier"
 */
export interface IAdmin {
  _id: string;
  pinterId: string;
  name: string;
  phone: string;
  email: string;
  password: string;
  role: string;
  active: boolean;
  companyPartner?: ICompanyPartner;
  distributor?: IDistributor;
  createdAt: string;
  updatedAt: string;
}
