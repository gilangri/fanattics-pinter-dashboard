import { IAddress } from "./address";
import { IBank } from "./bank";
import { ICompanyPartner } from "./companyPartner";
// import { IPartner } from "./partner";
import { IRepresentative } from "./representative";

export interface IDistributor {
  _id: string;
  pinterId: string;
  companyPartner: ICompanyPartner;
  name: string;
  email: string;
  phoneNumber: string;
  profilePicture: string | null;
  address: IAddress;
  bank: IBank & string;
  bankAccountNumber: string;
  // partners: IPartner[];
  revenueSharePercentage: number
  representatives: IRepresentative[];
  createdAt: string;
  updatetAt: string;
}
