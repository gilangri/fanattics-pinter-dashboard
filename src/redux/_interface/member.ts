import { IAddress } from "./address";
import { IBank } from "./bank";
import { ICompanyPartner } from "./companyPartner";
import { IDistributor } from "./distributor";

export interface IMember {
  _id: string;
  pinterId: string;
  fullName: string;
  email: string;
  phoneNumber: string;
  profilePicture: string | null;
  ktpNumber: number;
  ktpUrl: string | null;
  npwpNumber: string;

  address: string;
  province: string;
  city: string;
  district: string;
  subdistrict: string;
  postalCode: number;

  storeName: string;
  storeAddress: IAddress | null;
  bank: IBank;
  creditRequests: any[];
  active: boolean;
  distributor: IDistributor;
  companyPartner: ICompanyPartner;
  createdAt: string;
  updatedAt: string;
}
