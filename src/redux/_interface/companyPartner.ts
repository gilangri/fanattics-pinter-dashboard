import { IDistributor } from "./distributor";

export interface ICompanyPartner {
  _id: string;
  name: string;
  abbreviation: string;
  contactNumber: string;
  collectablePercentage: number;
  profilePicture: string;
  serverDomain: string;
  distributors: IDistributor[];
  createdAt: string;
  updatedAt: string;
}
