import { IMember } from "./member";

export interface IAddress {
  _id: string;
  user: IMember;
  address: string;
  province: string;
  city: string;
  district: string;
  subdistrict: string;
  postalCode: string;
  default: boolean;
}
