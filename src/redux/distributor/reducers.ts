import {
  IDistributorAction,
  IDistributorState,
  DISTRIBUTOR_FAILED,
  DISTRIBUTOR_REQUEST,
  DISTRIBUTOR_RESET,
  DISTRIBUTOR_SEARCH,
  DISTRIBUTOR_SEARCH_DONE,
  DISTRIBUTOR_SEARCH_RESET,
  DISTRIBUTOR_SET,
  DISTRIBUTOR_SET_DISTRIBUTOR,
  DISTRIBUTOR_SET_DISTRIBUTORS,
  DISTRIBUTOR_SET_VALUE,
} from "./types";

const init_state: IDistributorState = {
  distributors: [],
  distributor: undefined,
  filtered: [],

  searchQuery: "",
  searched: [],
  isSearching: false,

  loading: false,
  error: "",
};

export function distributorReducer(
  state = init_state,
  { payload, name, type }: IDistributorAction
): IDistributorState {
  switch (type) {
    case DISTRIBUTOR_REQUEST:
      return { ...state, loading: true, error: "" };
    case DISTRIBUTOR_FAILED:
      return { ...state, loading: false, error: payload };

    case DISTRIBUTOR_SEARCH:
      return { ...state, isSearching: true };
    case DISTRIBUTOR_SEARCH_DONE:
      return { ...state, searched: payload };
    case DISTRIBUTOR_SEARCH_RESET:
      return { ...state, isSearching: false, searched: [] };

    case DISTRIBUTOR_SET_DISTRIBUTORS:
      return { ...state, loading: false, distributors: payload };
    case DISTRIBUTOR_SET_DISTRIBUTOR:
      return { ...state, loading: false, distributor: payload };

    case DISTRIBUTOR_SET:
      return { ...state, ...payload };
    case DISTRIBUTOR_SET_VALUE:
      return { ...state, [name]: payload };

    case DISTRIBUTOR_RESET:
      return init_state;
    default:
      return state;
  }
}
