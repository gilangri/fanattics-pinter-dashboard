import axios from "axios";
import { API_URL, optionsFormData } from "../../constants";
import { sortArrByKey } from "../../utils/helpers/sort";
import {
  DISTRIBUTOR_FAILED,
  DISTRIBUTOR_REQUEST,
  DISTRIBUTOR_SET_DISTRIBUTOR,
  DISTRIBUTOR_SET_DISTRIBUTORS,
  TCreateDistributor,
} from "./types";

export const getAllDistributor = (id: string = "") => async (dispatch: any) => {
  try {
    dispatch({ type: DISTRIBUTOR_REQUEST });

    if (id) {
      const { data } = await axios.get(`${API_URL}/distributors/${id}`);
      const payload = data.result;
      dispatch({ type: DISTRIBUTOR_SET_DISTRIBUTOR, payload });
    } else {
      const { data } = await axios.get(`${API_URL}/distributors`);
      const payload = sortArrByKey(data.result, "fullName");
      dispatch({ type: DISTRIBUTOR_SET_DISTRIBUTORS, payload });
    }
  } catch (err) {
    console.log(err);
    dispatch({ type: DISTRIBUTOR_FAILED, payload: err });
  }
};

export const createDistributor = ({ images, ...body }: TCreateDistributor) => async (
  dispatch: any
) => {
  try {
    dispatch({ type: DISTRIBUTOR_REQUEST });

    const formdata = new FormData();
    images.forEach((val) => formdata.append("profilePicture", val));
    formdata.append("fullName", body.fullName);
    formdata.append("name", body.name);
    formdata.append("email", body.email);
    formdata.append("phoneNumber", body.phoneNumber);
    formdata.append("address", body.address);
    formdata.append("city", body.city);
    formdata.append("province", body.province);
    formdata.append("postalCode", body.postalCode);
    formdata.append("bankName", body.bankName);
    formdata.append("bankAccountNumber", body.bankAccountNumber);
    formdata.append("district", body.district);
    formdata.append("subdistrict", body.subdistrict);
    formdata.append("postalCode", body.postalCode);
    // formdata.append("representativeFirstName", body.representativeFirstName);
    // formdata.append("representativeLastName", body.representativeLastName);
    // formdata.append("representativePhoneNumber", body.representativePhoneNumber);
    // formdata.append("representativeEmail", body.representativeEmail);
    // formdata.append("representativeJobPosition", body.representativeJobPosition);

    await axios.post(`${API_URL}/distributors`, formdata, optionsFormData);
    await Promise.all([dispatch(getAllDistributor())]);
  } catch (err) {
    console.log(err);
    dispatch({ type: DISTRIBUTOR_FAILED, payload: err });
  }
};
