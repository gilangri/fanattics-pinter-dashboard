import { AnyAction } from "redux";
import { IDistributor } from "../_interface/distributor";

export type TCreateDistributor = {
  images: File[];
  name: string;
  fullName: string;
  email: string;
  phoneNumber: string;
  address: string;
  city: string;
  province: string;
  bankName: string;
  bankAccountNumber: string;
  district: string;
  subdistrict: string;
  postalCode: string;
  // profilePicture: string;
  // isAvailable: boolean;
  // representativeFirstName: string;
  // representativeLastName: string;
  // representativePhoneNumber: string;
  // representativeEmail: string;
  // representativeJobPosition: string;
};

export interface IDistributorState {
  distributors: IDistributor[];
  distributor?: IDistributor;

  filtered: IDistributor[];

  searchQuery: string;
  searched: IDistributor[];
  isSearching: boolean;

  loading: boolean;
  error: string;
}

export const DISTRIBUTOR_REQUEST = "DISTRIBUTOR_REQUEST";
export const DISTRIBUTOR_FAILED = "DISTRIBUTOR_FAILED";
export const DISTRIBUTOR_RESET = "DISTRIBUTOR_RESET";

export const DISTRIBUTOR_SEARCH = "DISTRIBUTOR_SEARCH";
export const DISTRIBUTOR_SEARCH_DONE = "DISTRIBUTOR_SEARCH_DONE";
export const DISTRIBUTOR_SEARCH_RESET = "DISTRIBUTOR_SEARCH_RESET";

export const DISTRIBUTOR_SET_DISTRIBUTORS = "DISTRIBUTOR_SET_DISTRIBUTORS";
export const DISTRIBUTOR_SET_DISTRIBUTOR = "DISTRIBUTOR_SET_DISTRIBUTOR";

export const DISTRIBUTOR_SET = "DISTRIBUTOR_SET";
export const DISTRIBUTOR_SET_VALUE = "DISTRIBUTOR_SET_VALUE";

export interface IDistributorAction extends AnyAction {
  payload: any;
  name: string;
  type:
    | typeof DISTRIBUTOR_REQUEST
    | typeof DISTRIBUTOR_FAILED
    | typeof DISTRIBUTOR_RESET
    //
    | typeof DISTRIBUTOR_SEARCH
    | typeof DISTRIBUTOR_SEARCH_DONE
    | typeof DISTRIBUTOR_SEARCH_RESET
    //
    | typeof DISTRIBUTOR_SET_DISTRIBUTOR
    | typeof DISTRIBUTOR_SET_DISTRIBUTORS
    //
    | typeof DISTRIBUTOR_SET
    | typeof DISTRIBUTOR_SET_VALUE;
}
