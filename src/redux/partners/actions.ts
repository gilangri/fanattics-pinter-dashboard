import axios from "axios";
import { API_URL } from "../../constants";
import { sortArrByKey } from "../../utils/helpers/sort";
import {
  PARTNERS_FAILED,
  PARTNERS_REQUEST,
  PARTNERS_SET_PARTNERS,
  PARTNERS_SET_PARTNER,
} from "./types";

export const getPartners = (id: string = "") => async (dispatch: any) => {
  try {
    dispatch({ type: PARTNERS_REQUEST });

    if (id) {
      const { data } = await axios.get(`${API_URL}/partners/${id}`);
      const payload = data.result;
      dispatch({ type: PARTNERS_SET_PARTNER, payload });
    } else {
      const { data } = await axios.get(`${API_URL}/partners`);
      const payload = sortArrByKey(data.result, "name");
      dispatch({ type: PARTNERS_SET_PARTNERS, payload });
    }
  } catch (err) {
    console.error(err);
    dispatch({ type: PARTNERS_FAILED, payload: err });
  }
};
