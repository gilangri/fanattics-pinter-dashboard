import { AnyAction } from "redux";
import { IPartner } from "../_interface/partner";

export interface IPARTNERSState {
  partners: IPartner[];
  partner?: IPartner;
  filtered: IPartner[];

  searchQuery: string;
  searched: IPartner[];
  isSearching: boolean;

  loading: boolean;
  error: string;
}

export const PARTNERS_REQUEST = "PARTNERS_REQUEST";
export const PARTNERS_FAILED = "PARTNERS_FAILED";
export const PARTNERS_RESET = "PARTNERS_RESET";

export const PARTNERS_SEARCH = "PARTNERS_SEARCH";
export const PARTNERS_SEARCH_DONE = "PARTNERS_SEARCH_DONE";
export const PARTNERS_SEARCH_RESET = "PARTNERS_SEARCH_RESET";

export const PARTNERS_SET_PARTNERS = "PARTNERS_SET_PARTNERS";
export const PARTNERS_SET_PARTNER = "PARTNERS_SET_PARTNER";

export const PARTNERS_SET = "PARTNERS_SET";
export const PARTNERS_SET_VALUE = "PARTNERS_SET_VALUE";

export interface IPARTNERSAction extends AnyAction {
  payload: any;
  name: string;
  type:
    | typeof PARTNERS_REQUEST
    | typeof PARTNERS_FAILED
    | typeof PARTNERS_RESET
    //
    | typeof PARTNERS_SEARCH
    | typeof PARTNERS_SEARCH_DONE
    | typeof PARTNERS_SEARCH_RESET
    //
    | typeof PARTNERS_SET_PARTNER
    | typeof PARTNERS_SET_PARTNERS
    //
    | typeof PARTNERS_SET
    | typeof PARTNERS_SET_VALUE;
}
