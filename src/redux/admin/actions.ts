import axios from "axios";
import { API_URL, optionsFormUrlencoded } from "../../constants";
import { sortArrByKey } from "../../utils/helpers/sort";
import { IAdmin } from "../_interface/admin";
import {
  ADMIN_FAILED,
  ADMIN_REQUEST,
  ADMIN_SET_ADMIN,
  ADMIN_SET_ADMINS,
} from "./types";

/**
 * fetching admin(s) data
 * @param id (optional) - provide to get singular admin
 */
export const getAdmin = (id: string = "") => async (dispatch: any) => {
  try {
    dispatch({ type: ADMIN_REQUEST });
    if (id) {
      const { data } = await axios.get(`${API_URL}/admins/${id}`);
      const selected = data.result.filter((val: IAdmin) => val._id === id);
      const payload = selected.length ? selected[0] : undefined;
      dispatch({ type: ADMIN_SET_ADMIN, payload });
    } else {
      const { data } = await axios.get(`${API_URL}/admins`);
      const payload = sortArrByKey(data.result, "name");
      dispatch({ type: ADMIN_SET_ADMINS, payload });
    }
  } catch (err) {
    console.error(err);
    dispatch({ type: ADMIN_FAILED, payload: err });
  }
};

/**
 * create new admin
 * @param id selected admin's id
 * @param body body of edited admin
 */
export const createAdmin = (body: Object) => async (dispatch: any) => {
  try {
    dispatch({ type: ADMIN_REQUEST });
    await axios.post(`${API_URL}/admins`, body, optionsFormUrlencoded);
    await Promise.all([dispatch(getAdmin())]);
  } catch (err) {
    console.error(err);
    dispatch({ type: ADMIN_FAILED, payload: err });
  }
};

/**
 * Edit admin information
 * @param id selected admin's id
 * @param body body of edited admin
 */
export const editAdmin = (id: string, body: Object) => async (dispatch: any) => {
  try {
    dispatch({ type: ADMIN_REQUEST });
    await axios.put(`${API_URL}/admins/${id}`, body);
    await Promise.all([dispatch(getAdmin())]);
  } catch (err) {
    console.error(err);
    dispatch({ type: ADMIN_FAILED, payload: err });
  }
};
