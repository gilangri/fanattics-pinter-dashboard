import { IAdmin } from "../_interface/admin";

export interface IAdminState {
  admins: IAdmin[];
  admin?: IAdmin;

  filtered: IAdmin[];

  searchQuery: string;
  searched: IAdmin[];
  isSearching: boolean;

  loading: boolean;
  error: string;
}

export const ADMIN_REQUEST = "ADMIN_REQUEST";
export const ADMIN_FAILED = "ADMIN_FAILED";
export const ADMIN_RESET = "ADMIN_RESET";

export const ADMIN_SEARCH = "ADMIN_SEARCH";
export const ADMIN_SEARCH_DONE = "ADMIN_SEARCH_DONE";
export const ADMIN_SEARCH_RESET = "ADMIN_SEARCH_RESET";

export const ADMIN_SET_ADMINS = "ADMIN_SET_ADMINS";
export const ADMIN_SET_ADMIN = "ADMIN_SET_ADMIN";

export const ADMIN_SET = "ADMIN_SET";
export const ADMIN_SET_VALUE = "ADMIN_SET_VALUE";

export interface IAdminAction {
  payload: any;
  name: string;
  type:
    | typeof ADMIN_REQUEST
    | typeof ADMIN_FAILED
    | typeof ADMIN_RESET
    //
    | typeof ADMIN_SEARCH
    | typeof ADMIN_SEARCH_DONE
    | typeof ADMIN_SEARCH_RESET
    //
    | typeof ADMIN_SET_ADMINS
    | typeof ADMIN_SET_ADMIN
    //
    | typeof ADMIN_SET
    | typeof ADMIN_SET_VALUE;
}
