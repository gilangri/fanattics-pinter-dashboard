import {
  ADMIN_FAILED,
  ADMIN_REQUEST,
  ADMIN_RESET,
  ADMIN_SEARCH,
  ADMIN_SEARCH_DONE,
  ADMIN_SEARCH_RESET,
  ADMIN_SET,
  ADMIN_SET_ADMIN,
  ADMIN_SET_ADMINS,
  ADMIN_SET_VALUE,
  IAdminAction,
  IAdminState,
} from "./types";

const init_state: IAdminState = {
  admins: [],
  admin: undefined,

  filtered: [],

  searchQuery: "",
  searched: [],
  isSearching: false,

  loading: false,
  error: "",
};

export function adminReducer(
  state = init_state,
  { payload, name, type }: IAdminAction
): IAdminState {
  switch (type) {
    case ADMIN_REQUEST:
      return { ...state, loading: true, error: "" };
    case ADMIN_FAILED:
      return { ...state, loading: false, error: payload };

    case ADMIN_SEARCH:
      return { ...state, isSearching: true };
    case ADMIN_SEARCH_DONE:
      return { ...state, searched: payload };
    case ADMIN_SEARCH_RESET:
      return { ...state, isSearching: false, searched: [] };

    case ADMIN_SET_ADMINS:
      return { ...state, loading: false, admins: payload };
    case ADMIN_SET_ADMIN:
      return { ...state, loading: false, admin: payload };

    case ADMIN_SET:
      return { ...state, ...payload };
    case ADMIN_SET_VALUE:
      return { ...state, [name]: payload };

    case ADMIN_RESET:
      return init_state;
    default:
      return state;
  }
}
