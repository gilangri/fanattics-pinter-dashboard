import {
  ICOPARTNERSAction,
  ICOPARTNERSState,
  PARTNERS_FAILED,
  PARTNERS_REQUEST,
  PARTNERS_RESET,
  PARTNERS_SEARCH,
  PARTNERS_SEARCH_DONE,
  PARTNERS_SEARCH_RESET,
  PARTNERS_SET,
  PARTNERS_SET_PARTNER,
  PARTNERS_SET_PARTNERS,
  PARTNERS_SET_VALUE,
} from "./types";

const init_state: ICOPARTNERSState = {
  partners: [],
  partner: undefined,
  filtered: [],
  searchQuery: "",
  searched: [],
  isSearching: false,
  loading: false,
  error: "",
};

export function coPartnersReducer(
  state = init_state,
  { payload, name, type }: ICOPARTNERSAction
): ICOPARTNERSState {
  switch (type) {
    case PARTNERS_REQUEST:
      return { ...state, loading: true, error: "" };
    case PARTNERS_FAILED:
      return { ...state, loading: false, error: payload };

    case PARTNERS_SEARCH:
      return { ...state, isSearching: true };
    case PARTNERS_SEARCH_DONE:
      return { ...state, searched: payload };
    case PARTNERS_SEARCH_RESET:
      return { ...state, isSearching: false, searched: [] };

    case PARTNERS_SET_PARTNERS:
      return { ...state, loading: false, partners: payload };

    case PARTNERS_SET_PARTNER:
      return { ...state, loading: false, partner: payload };

    case PARTNERS_SET:
      return { ...state, ...payload };
    case PARTNERS_SET_VALUE:
      return { ...state, [name]: payload };

    case PARTNERS_RESET:
      return init_state;
    default:
      return state;
  }
}
