import axios from "axios";
import { API_URL, optionsFormData } from "../../constants";
import {
  PARTNERS_FAILED,
  PARTNERS_REQUEST,
  PARTNERS_SET_PARTNERS,
  PARTNERS_SET_PARTNER,
  TCreatePartner,
} from "./types";
import { sortArrByKey } from "../../utils/helpers/sort";

export const getCoPartners = (id: string = "") => async (dispatch: any) => {
  try {
    dispatch({ type: PARTNERS_REQUEST });

    if (id) {
      const { data } = await axios.get(`${API_URL}/company-partners/${id}`);
      const payload = data.result;
      dispatch({ type: PARTNERS_SET_PARTNER, payload });
    } else {
      const { data } = await axios.get(`${API_URL}/company-partners`);
      const payload = sortArrByKey(data.result, "name");
      dispatch({ type: PARTNERS_SET_PARTNERS, payload });
    }
  } catch (err) {
    console.error(err);
    dispatch({ type: PARTNERS_FAILED, payload: err });
  }
};

export const createCoPartner = ({ images, ...body }: TCreatePartner) => async (
  dispatch: any
) => {
  try {
    dispatch({ type: PARTNERS_REQUEST });

    const formdata = new FormData();
    images.forEach((val) => formdata.append("profilePicture", val));
    formdata.append("name", body.name);
    formdata.append("abbreviation", body.abbreviation);
    formdata.append("contactNumber", body.contactNumber);

    await axios.post(`${API_URL}/company-partners`, formdata, optionsFormData);
    await Promise.all([dispatch(getCoPartners())]);
  } catch (err) {
    console.error(err);
    dispatch({ type: PARTNERS_FAILED, payload: err });
  }
};

/**
 * Edit admin information
 * @param id selected admin's id
 * @param body body of edited admin
 */

export const editCoPartners = (id: string, body: Object) => async (
  dispatch: any
) => {
  try {
    dispatch({ type: PARTNERS_REQUEST });
    await axios.put(`${API_URL}/company-partners/${id}`, body);
    await Promise.all([dispatch(getCoPartners())]);
  } catch (err) {
    console.error(err);
    dispatch({ type: PARTNERS_FAILED, payload: err });
  }
};

// export const editCoPartners = (id: string, body: Object) => async (
//   dispatch: any
// ) => {
//   try {
//     dispatch({ type: PARTNERS_REQUEST });
//     await axios.put(`${API_URL}/company-partners/${id}`, body);
//     await Promise.all([dispatch(getCoPartners())]);
//   } catch (err) {
//     console.error(err);
//     dispatch({ type: PARTNERS_FAILED, payload: err });
//   }
// };
