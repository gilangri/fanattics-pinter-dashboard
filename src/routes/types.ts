export type routeTypes = {
    path: string;
    exact?: boolean;
    sidebar?: any;
    main: any;
  };
  