import Sidebar from "../components/Sidebar/Admin";
import CompanyPartnerPage from "../pages/CompanyPartner";
import CompanyPartnerDetailPage from "../pages/CompanyPartner/Detail";
import CompanyPartnerFormPage from "../pages/CompanyPartner/Form";
import { routeTypes } from "./types";

const slug = "/company-partner";

export const routeCompanyPartner: routeTypes[] = [
  {
    exact: true,
    path: `${slug}`,
    sidebar: () => <Sidebar />,
    main: () => <CompanyPartnerPage />,
  },
  {
    exact: true,
    path: `${slug}/form`,
    sidebar: () => <Sidebar />,
    main: () => <CompanyPartnerFormPage />,
  },
  {
    exact: true,
    path: `${slug}/detail`,
    main: () => <CompanyPartnerDetailPage />,
  },
];
