import * as React from "react";
import Sidebar from "../components/Sidebar/Transaction";
import { routeTypes } from "./types";
import Settlement from "../pages/Settlement";
import PendingTransaction from "../pages/Transaction/PendingTransaction";
import VerifiedTransaction from "../pages/Transaction/VerifiedTransaction";
import FailedTransaction from "../pages/Transaction/FailedTransaction";
import KURTransaction from "../pages/Transaction/KURTransaction";

const slugTrx = "/transaction";

export const routeTransaction: routeTypes[] = [
  {
    exact: true,
    path: `${slugTrx}/settlement`,
    sidebar: () => <Sidebar />,
    main: () => <Settlement />,
  },
  {
    exact: true,
    path: `${slugTrx}/report/pending`,
    sidebar: () => <Sidebar />,
    main: () => <PendingTransaction />,
  },
  {
    exact: true,
    path: `${slugTrx}/report/verified`,
    sidebar: () => <Sidebar />,
    main: () => <VerifiedTransaction />,
  },
  {
    exact: true,
    path: `${slugTrx}/report/failed`,
    sidebar: () => <Sidebar />,
    main: () => <FailedTransaction />,
  },
  {
    exact: true,
    path: `${slugTrx}/report/kur`,
    sidebar: () => <Sidebar />,
    main: () => <KURTransaction />,
  },
];
