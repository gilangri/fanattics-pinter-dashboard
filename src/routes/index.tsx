import { routeTypes } from "./types";

import Login from "../pages/Login";

import { routeAdmin } from "./Admin";
import { routeTransaction } from "./Transaction";
import { routeCompanyPartner } from "./CompanyPartner";

export const AppRoutes: routeTypes[] = [
  {
    exact: true,
    path: `/login`,
    main: () => <Login />,
  },
  ...routeAdmin,
  ...routeTransaction,
  ...routeCompanyPartner,
];
