import Sidebar from "../components/Sidebar/Admin";
import AdminCatalog from "../pages/Admin";

import MemberPage from "../pages/Member";
import MemberDetailPage from "../pages/Member/Detail";
import MemberFormPage from "../pages/Member/Form";

import Distributor from "../pages/Distributor";
import DistributorFormPage from "../pages/Distributor/Form";
import DistributorDetailPage from "../pages/Distributor/Detail";
import Partner from "../pages/Partner";
import CreateNewPartner from "../pages/Partner/CreateNewPartner";
import DetailPartner from "../pages/Partner/PartnerDetail";
import { routeTypes } from "./types";

const slug = "/admin";
const slugMembers = "/admin/members";
const slugDistributors = "/admin/distributors";

export const routeAdmin: routeTypes[] = [
  {
    exact: true,
    path: `${slugMembers}`,
    sidebar: () => <Sidebar />,
    main: () => <MemberPage />,
  },
  {
    exact: true,
    path: `${slugMembers}/form`,
    main: () => <MemberFormPage />,
  },
  {
    exact: true,
    path: `${slugMembers}/detail`,
    main: () => <MemberDetailPage />,
  },
  //====== DISTRIBUTOR =======//
  {
    exact: true,
    path: `${slugDistributors}`,
    sidebar: () => <Sidebar />,
    main: () => <Distributor />,
  },
  {
    exact: true,
    path: `${slugDistributors}/detail`,
    main: () => <DistributorDetailPage />,
  },
  {
    exact: true,
    path: `${slugDistributors}/form`,
    main: () => <DistributorFormPage />,
  },

  //====== ADMINS INTERNAL =======//
  {
    exact: true,
    path: `${slug}/internals`,
    sidebar: () => <Sidebar />,
    main: () => <AdminCatalog />,
  },

  //====== PARTNERS ======//
  {
    exact: true,
    path: `${slug}/partners`,
    sidebar: () => <Sidebar />,
    main: () => <Partner />,
  },
  {
    exact: true,
    path: `${slug}/partners/detail`,
    // sidebar: () => <Sidebar />,
    main: () => <DetailPartner />,
  },
  {
    exact: true,
    path: `${slug}/partners/create-partner`,
    // sidebar: () => <Sidebar />,
    main: () => <CreateNewPartner />,
  },
];
