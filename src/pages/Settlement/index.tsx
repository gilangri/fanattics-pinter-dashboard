/* eslint-disable @typescript-eslint/no-unused-vars */
import React from "react";
import { Settings16, Repeat16, DocumentDownload16 } from "@carbon/icons-react";
import {
  ButtonIcon,
  Button,
  Select,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
  Panel,
  ParentContainer,
} from "../../components";

export default function Settlement() {
  return (
    <ParentContainer title="Settlement" className="admin-catalogue-container">
      <div className="settlement-container">
        <div className="settlement-page">
          <div className="renew-data">
            <span>Distributor Settlement</span>

            <div className="perbarui-data">
              <Button theme="outline">
                Perbarui Data
                <Repeat16 />
              </Button>
              <span className="last-update">
                Update terakhir : 15:00:00, 12 Nov 2020
              </span>
            </div>
          </div>

          <div className="fillter-download">
            <div className="transaction-wrapp">
              <div className="urutkan-transaksi">
                <span>RECIPIENT</span>
                <select className="select-transaction">
                  <option value="">PT Unilever</option>
                  <option value="">Transaksi Terbaru</option>
                  <option value="">Transaksi Terbaru</option>
                </select>
              </div>
              <div className="semua-transaksi">
                <span>TANGGAL</span>
                <Select className="select-transaction">
                  <option value="">13 September 2021</option>
                  <option value="">Belum dikonfirmasi</option>
                  <option value="">Dikonfirmasi</option>
                  <option value="">Dalam Pengiriman</option>
                </Select>
              </div>
            </div>
            <Button theme="icon">
              Download Excel File &nbsp; &nbsp;
              <DocumentDownload16 color="#7C4DFF" />
            </Button>
          </div>

          <Panel>
            <Table>
              <TableHead>
                <TableRow>
                  <TableHeadCell>Tanggal Transaksi</TableHeadCell>
                  <TableHeadCell>UKM</TableHeadCell>
                  <TableHeadCell>Nominal</TableHeadCell>
                  <TableHeadCell>Fee 1</TableHeadCell>
                  <TableHeadCell>Fee 2</TableHeadCell>
                  <TableHeadCell>Fee 3</TableHeadCell>
                  <TableHeadCell>Total Transaksi</TableHeadCell>
                </TableRow>
              </TableHead>

              <TableBody>
                <TableCell>30 Jun 2019</TableCell>
                <TableCell>PT Unilever</TableCell>
                <TableCell>200.000</TableCell>
                <TableCell>2.000.000</TableCell>
                <TableCell>200.000</TableCell>
                <TableCell>2.000.000</TableCell>
                <TableCell>2.000.000</TableCell>
              </TableBody>
            </Table>
          </Panel>
        </div>
        <div className="tab-bottom">
          <div className="pt-name">
            <span>PT Unilever</span>
          </div>

          <div className="table-body">
            <div className="table-column">
              <span className="head">Transaction</span>
              <span>200.000</span>
            </div>

            <div className="table-column">
              <span className="head">Nominal</span>
              <span>200.000</span>
            </div>

            <div className="table-column">
              <span className="head">ABC (7%)</span>
              <span>200.000</span>
            </div>

            <div className="table-column">
              <span className="head">BCT (7%)</span>
              <span>200.000</span>
            </div>
            <div className="table-column">
              <span className="head">BCT (7%)</span>
              <span>200.000</span>
            </div>
            <div className="table-column">
              <span className="head">Transaction</span>
              <span>200.000</span>
            </div>
          </div>
        </div>
      </div>
    </ParentContainer>
  );
}
