/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { RootState } from "../../../redux/store";
import { FiArrowLeft } from "react-icons/fi";
import { Button } from "../../../components";
import { getCoPartners, editCoPartners } from "../../../redux/coPartners/actions";
import { ICompanyPartner } from "../../../redux/_interface/companyPartner";
// import { ModalEditPartner } from "../_modalEditPartner";

export default function PartnerDetail() {
  const dispatch = useDispatch();
  const history = useHistory();

  //* GLOBAL STATE
  const partner = useSelector((state: RootState) => state.coPartners.partner);

  //* LOCAL STATE
  const [isOpen, setIsOpen] = useState(false);
  const [isEditing, setIsEditing] = useState<ICompanyPartner | undefined>(undefined);

  //* TOGGLE
  const togglePopup = () => setIsOpen(!isOpen);
  const cancelPopup = () => {
    setIsOpen(false);
    setIsEditing(undefined);
  };

  //* FUNCTION
  const handleSimpan = async (item: Object) => {
    try {
      if (isEditing) {
        await Promise.all([dispatch(editCoPartners(isEditing._id, item))]);
      }
      cancelPopup();
    } catch (err) {
      console.error(err);
    }
  };

  return (
    <div className="partner-detail">
      {/* <ModalEditPartner
        editingData={isEditing}
        isOpen={isOpen}
        toggle={cancelPopup}
        handleSimpan={handleSimpan}
      /> */}

      <div className="container-head">
        <button onClick={history.goBack} className="button-back">
          <FiArrowLeft color="#0c70d0" /> &nbsp; Kembali
        </button>
        <div className="container-header-wrapp">
          <div className="image-content">
            <img src={partner?.profilePicture || ""} alt={partner?.name} />
          </div>
          <div className="name-content">
            <span className="partner-name">
              {partner?.name} ({partner?.abbreviation})
            </span>

            <div className="partner-info">
              <div className="partner-id">
                <span className="partner-name-left">Partner ID</span>
                <span className="partner-name-right">{partner?._id}</span>
              </div>
              <div className="partner-id">
                <span className="partner-name-left">Contact Info</span>
                <span className="partner-name-right">{partner?.contactNumber}</span>
              </div>
            </div>
          </div>
        </div>
        <Button
          onClick={(item: ICompanyPartner) => {
            setIsOpen(true);
            setIsEditing(item);
          }}
          theme="primary-radius"
        >
          Edit
        </Button>
      </div>
    </div>
  );
}
