/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect, useState } from "react";
import {
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
  Panel,
  Form,
  Searchbar,
  usePagination,
  Pagination,
  TableFooter,
  ContainerBody,
  ParentContainer,
} from "../../components";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { RootState } from "../../redux/store";
import { getCoPartners, editCoPartners } from "../../redux/coPartners/actions";
import { PARTNERS_SET_PARTNER } from "../../redux/coPartners/types";
import { ICompanyPartner } from "../../redux/_interface/companyPartner";
import { AiOutlineUserAdd } from "react-icons/ai";
// import { ModalEditPartner } from "./_modalEditPartner";

export default function Partner() {
  const dispatch = useDispatch();
  const history = useHistory();

  //* GLOBAL STATE
  const { partners } = useSelector((state: RootState) => state.coPartners);

  //* LOCAL STATE
  const [isOpen, setIsOpen] = useState(false);
  const [isEditing, setIsEditing] = useState<ICompanyPartner | undefined>(undefined);

  //* TOGGLE
  const togglePopup = () => setIsOpen(!isOpen);
  const cancelPopup = () => {
    setIsOpen(false);
    setIsEditing(undefined);
  };
  //* FETCH DATA
  useEffect(() => {
    dispatch(getCoPartners());
  }, [dispatch]);

  const goToDetail = (payload: ICompanyPartner) => {
    dispatch({ type: PARTNERS_SET_PARTNER, payload });
    history.push(`${history.location.pathname}/detail`);
  };

  //* FUNCTION
  const handleSimpan = async (item: Object) => {
    try {
      if (isEditing) {
        await Promise.all([dispatch(editCoPartners(isEditing._id, item))]);
      }
      cancelPopup();
    } catch (err) {
      console.error(err);
    }
  };

  //* Create Partner Button
  const goToCreateMember = () =>
    history.push(`${history.location.pathname}/create-partner`);

  return (
    <div className="user-wrapp">
      {/* <ModalEditPartner
        editingData={isEditing}
        isOpen={isOpen}
        toggle={cancelPopup}
        handleSimpan={handleSimpan}
      /> */}

      <div className="container-head">
        <h2>Daftar Partners</h2>

        <div className="button-wrapp">
          <Button theme="primary" onClick={goToCreateMember}>
            <AiOutlineUserAdd /> &nbsp;Tambah Partner
          </Button>
        </div>
      </div>

      <div className="user-page">
        <Table bordered>
          <TableHead dark>
            <TableRow>
              <TableHeadCell className="">Nama Perusahaan</TableHeadCell>
              <TableHeadCell className="">Singkatan</TableHeadCell>
              <TableHeadCell>Nomor Kontak</TableHeadCell>
              <TableHeadCell>Total Distributor</TableHeadCell>
            </TableRow>
          </TableHead>

          <RenderBody
            data={partners}
            onClick={goToDetail}
            // onClick={(item) => {
            //   setIsOpen(true);
            //   setIsEditing(item);
            // }}
          />
        </Table>
      </div>
    </div>
  );
}

const RenderBody = ({
  data,
  onClick,
}: {
  data: ICompanyPartner[];
  onClick: (item: ICompanyPartner) => void;
}) => {
  return (
    <TableBody>
      {!data.length
        ? null
        : data.map((item, i) => {
            return (
              <TableRow key={i} onClick={() => onClick(item)}>
                <TableCell>{item.name}</TableCell>
                <TableCell>{item.abbreviation}</TableCell>
                <TableCell>{item.contactNumber}</TableCell>
                <TableCell>10</TableCell>
              </TableRow>
            );
          })}
    </TableBody>
  );
};
