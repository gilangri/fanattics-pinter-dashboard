/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState, useEffect, useCallback } from "react";
import {
  ParentContainer,
  ContainerBody,
  ContainerHead,
  Button,
} from "../../../components";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { RootState } from "../../../redux/store";
import { TCreatePartner } from "../../../redux/coPartners/types";
import { createCoPartner } from "../../../redux/coPartners/actions";
import { useDropzone } from "react-dropzone";
import { MdClose } from "react-icons/md";
import { InputText } from "./_inputText";

export default function CreateNewPartner() {
  const dispatch = useDispatch();
  const history = useHistory();

  const { loading } = useSelector((state: RootState) => state.coPartners);

  //* State Create New User
  const [images, setImages] = useState<File[]>([]);
  const [name, setName] = useState("");
  const [abbreviation, setAbbreviation] = useState("");
  const [contactNumber, setContactNumber] = useState("");

  const handleAddFile = (value: File[]) => setImages([...images, ...value]);
  const handleDeleteFile = (lastModified: number) => {
    if (!images.length) return;
    const newFiles = images.filter((val) => val.lastModified !== lastModified);
    setImages(newFiles);
  };

  const handleSimpan = async () => {
    try {
      const body: TCreatePartner = {
        images,
        name,
        abbreviation,
        contactNumber,
      };

      await Promise.all([dispatch(createCoPartner(body))]);
      history.goBack();
      console.log(body);
    } catch (err) {
      console.error(err, "error");
    }
  };

  return (
    <div className="create-new-partner">
      <div className="cnp-wrapp">
        <ContainerHead backButton title="Tambah Partner" />

        <ImageUploader
          files={images}
          onChange={handleAddFile}
          handleDelete={handleDeleteFile}
        />

        <InputText
          name="name"
          title="Nama Company Partner"
          placeholder="Masukkan Nama  Company Partner"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <InputText
          name="abbreviation"
          title="Nama Abbreviation"
          placeholder="Masukkan Nama Abbreviation"
          value={abbreviation}
          onChange={(e) => setAbbreviation(e.target.value)}
        />
        <InputText
          name="contactNumber"
          title="Masukan Nomor Kontak"
          placeholder="Masukkan Nomor Kontak"
          value={contactNumber}
          onChange={(e) => setContactNumber(e.target.value)}
        />

        <div className="button-save-cancle">
          <Button theme="outline-radius">Batalkan</Button>
          &nbsp; &nbsp; &nbsp;
          <Button
            theme="primary-radius"
            className="button-icon"
            disabled={loading}
            isLoading={loading}
            onClick={handleSimpan}
          >
            Simpan
          </Button>
        </div>
      </div>
    </div>
  );
}

const ImageUploader = ({
  files,
  handleDelete,
  onChange,
}: {
  files: File[];
  handleDelete: (lastModified: number) => void;
  onChange: (value: File[]) => void;
}) => {
  const { getRootProps, getInputProps } = useDropzone({
    multiple: true,
    accept: [".jpg", ".png", ".jpeg"],
    noKeyboard: true,
    onDrop: useCallback((acceptedFiles: File[]) => onChange(acceptedFiles), [
      onChange,
    ]),
  });

  return (
    <div className="image-upload-container">
      <div className="image-uploader">
        <div {...getRootProps({ className: "dropzone" })}>
          <div className="image-catalog">
            <label className="component-btn component-btn-primary">Unggah</label>
          </div>
          <input {...getInputProps()} />
        </div>

        {files.map((file, i) => {
          return (
            <div key={i} className="image-catalog">
              <Button theme="text" onClick={() => handleDelete(file.lastModified)}>
                <MdClose size={16} />
              </Button>
              <img src={URL.createObjectURL(file)} alt="profilePicture" />
            </div>
          );
        })}
      </div>
    </div>
  );
};
