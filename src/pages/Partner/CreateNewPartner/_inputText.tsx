import { Input, TextCounter } from "../../../components";

interface Props {
  name: string;
  placeholder?: string;
  title: string;
  subtitle?: string;
  value?: any;
}

interface PropsText extends Props {
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}
export const InputText = ({
  name,
  onChange,
  placeholder,
  title,
  subtitle,
  value,
}: PropsText) => {
  return (
    <div className={`input__group ${name}`}>
      <div className="input__label">
        <div className="input__label-title">{title}</div>
        {!subtitle ? null : <div className="input__label-subtitle">{subtitle}</div>}
      </div>

      <Input
        value={value}
        onChange={onChange}
        name={name}
        className="input__item"
        placeholder={placeholder}
      />
    </div>
  );
};

interface PropsTextarea extends Props {
  maxLength?: number;
  onChange?: React.ChangeEventHandler<HTMLTextAreaElement>;
  rows?: number;
}
export const InputTextArea = ({
  maxLength,
  name,
  onChange,
  placeholder,
  rows = 5,
  title,
  subtitle,
  value,
}: PropsTextarea) => {
  return (
    <div className={`input__group ${name}`}>
      <div className="input__label">
        <div className="input__label-title">{title}</div>
        {!subtitle ? null : <div className="input__label-subtitle">{subtitle}</div>}
      </div>

      <div className="form-group">
        <div className="input-group">
          <textarea
            rows={rows}
            value={value}
            maxLength={maxLength}
            onChange={onChange}
            className="component-input form-control input__item"
            placeholder={placeholder}
          />
        </div>

        {!maxLength ? null : (
          <TextCounter length={value.length} maxLength={maxLength} />
        )}
      </div>
    </div>
  );
};

interface PropsSelect extends Props {
  children: React.ReactNode;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}
export const InputSelect = ({
  children,
  name,
  onChange,
  title,
  value,
}: PropsSelect) => {
  return (
    <div className="input__group name">
      <div className="input__label">
        <div className="input__label-title">{title}</div>
      </div>

      <Input
        value={value}
        onChange={onChange}
        name={name}
        className="input__item"
        type="select"
      >
        {children}
      </Input>
    </div>
  );
};
