import { ReactNode, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import {
  Button,
  Form,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  SubjectTitle,
} from "../../components";
import { RootState } from "../../redux/store";
import { ICompanyPartner } from "../../redux/_interface/companyPartner";

interface Props {
  isOpen?: boolean;
  editingData?: ICompanyPartner;
  toggle?: () => void;
  closeButton?: boolean;
  handleSimpan: (item: Object) => void;
}

export const ModalEditPartner = ({
  isOpen,
  editingData,
  toggle,
  closeButton,
  handleSimpan,
}: Props) => {
  //* GLOBAL STATE
  const isLoading = useSelector((state: RootState) => state.coPartners.loading);

  //* LOCAL STATE
  const [name, setName] = useState(editingData?.name || "");
  const [abbreviation, setAbbreviation] = useState(editingData?.abbreviation || "");
  const [contactNumber, setContactNumber] = useState(
    editingData?.contactNumber || ""
  );

  //* FUNCTION
  const resetState = () => {
    setName("");
  };

  const onCancel = () => {
    if (!toggle) return;
    resetState();
    toggle();
  };

  const onSimpan = async () => {
    if (!toggle || isLoading) return;
    try {
      const body = { name, contactNumber, abbreviation };
      await Promise.all([handleSimpan(body)]);
      resetState();
      toggle();
    } catch (err) {
      console.error(err);
    }
  };

  useEffect(() => {
    if (editingData) {
      setName(editingData.name);
      setAbbreviation(editingData.abbreviation);
      setContactNumber(editingData.contactNumber);
    }
  }, [editingData]);

  return (
    <Modal
      className="modal__add-admin"
      size="lg"
      isOpen={isOpen}
      // toggle={!closeButton ? toggle : undefined}
    >
      <ModalHeader title="Edit Partner" toggle={closeButton ? toggle : undefined} />
      <ModalBody>
        <ContentItem title="Informasi Akun" isRow={false}>
          <Input
            label="Nama Partner"
            value={name}
            onChange={({ target }) => setName(target.value)}
            placeholder="Nama User"
          />
          <Input
            label="Abbreviation"
            value={abbreviation}
            onChange={({ target }) => setAbbreviation(target.value)}
            placeholder="Abbreviation"
          />
          <Input
            label="Nomor Telepon"
            value={contactNumber}
            onChange={({ target }) => setContactNumber(target.value)}
            placeholder="Nomor Telepon"
          />
        </ContentItem>

        <div className="button-action d-flex justify-content-end">
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={onCancel}
            minWidth={80}
            className="mt-3"
            theme="secondary"
          >
            Batalkan
          </Button>
          &nbsp;
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={onSimpan}
            minWidth={80}
            className="mt-3"
            theme="primary"
          >
            Simpan
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

const ContentItem = ({
  children,
  title,
  isRow = true,
}: {
  children: ReactNode;
  title: string;
  isRow?: boolean;
}) => {
  return (
    <div className="content-item">
      <div className="header">
        <SubjectTitle bold>{title}</SubjectTitle>
      </div>

      <Form className={`body ${isRow && "row"}`}>{children}</Form>
    </div>
  );
};
