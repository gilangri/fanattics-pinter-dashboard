import React, { useState } from "react";
import LoginBackground from "../../assets/images/login-bg.jpg";
import Logo from "../../assets/images/logo-pinter-v.png";
import { Input } from "../../components";
import { useSelector, useDispatch } from "react-redux";
import { handleLogin } from "../../redux/auth/actions";
import { RootState } from "../../redux/store";
import { AUTH_SET_VALUE } from "../../redux/auth/types";
import { Spinner } from "reactstrap";

export default function Login() {
  const dispatch = useDispatch();
  const auth = useSelector((state: RootState) => state.auth);
  const loading = useSelector((state: RootState) => state.auth.loadingLogin);

  const [loginForm, setLoginForm] = useState({ email: "", password: "" });

  const valueOnchange = (e: any) => {
    let name = e.target.name;
    let value = e.target.value;
    setLoginForm({ ...loginForm, [name]: value });
    dispatch({ type: AUTH_SET_VALUE, payload: { name, value } });
  };

  const submitLogin = () => {
    dispatch(handleLogin({ payload: loginForm }));
  };

  return (
    <div className="login-page">
      <div className="image-wrapper">
        <img src={LoginBackground} alt="pinter-bg" />
      </div>

      <div className="content-wrapper">
        <div className="logo-wrapper">
          <img className="img-logo" src={Logo} alt="pinter" />
        </div>

        <div className="text-head">
          <div className="subtitle">Selamat Datang di Pinter Dashboard</div>
          <div className="title">Login Ke Akun Anda</div>
        </div>

        <div className="input-wrapp">
          <Input
            className="input"
            label="Email Anda"
            name="email"
            onChange={valueOnchange}
            placeholder="Masukkan Nama Anda"
            type="text"
          />
        </div>
        <div className="input-wrapp">
          <Input
            className="input"
            label="Password"
            name="password"
            onChange={valueOnchange}
            onKeyPress={(e) => e.key === "Enter" && submitLogin()}
            placeholder="Masukkan Password"
            type="password"
          />
        </div>

        <button className="button-login" onClick={submitLogin}>
          {loading ? <Spinner size="sm" /> : "Login Sekarang"}
        </button>

        <br />
        <br />
        <ErrorMessage value={auth.error} />
      </div>
    </div>
  );
}

const ErrorMessage = ({ value = "" }) => {
  return <div className="">{value}</div>;
};
