/* eslint-disable @typescript-eslint/no-unused-vars */
import Axios from "axios";
import moment from "moment";
import React, { useState, useEffect } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
  Panel,
  ParentContainer,
  BodyText,
  Input,
} from "../../components";
import { API_URL } from "../../constants";

export default function KURTransaction() {
  const [kurTransactions, setKURTransactions] = useState<any[]>([]);

  const fetchKURTransaction = async () => {
    try {
      const { data } = await Axios.get(`${API_URL}/transactions`);

      setKURTransactions(data.result);
    } catch (err) {
      console.log(err)
      alert("Server error")
    }
  }

  const renderKURTransactions = () => {
    return kurTransactions.map((val) => {
      return (
        <TableRow>
          <TableCell>{moment(val?.createdAt).format("DD MMM YYYY hh:mm:ss")}</TableCell>
          <TableCell>{val?.buyer?.storeName}</TableCell>
          <TableCell>{val?.distributor?._id}</TableCell>
          <TableCell>{val?._id}</TableCell>
          <TableCell>{val?.buyer?.bank}</TableCell>
          <TableCell>Credit</TableCell>
          <TableCell>Requested</TableCell>
          <TableCell>{new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR'}).format(val?.price).slice(0,-3)}</TableCell>
        </TableRow>
      )
    })
  }

  useEffect(() => {
    fetchKURTransaction();
  }, [])

  return (
    <ParentContainer title="Transaction" className="admin-catalogue-container">
      <div className="renew-data font-weight-bold">
        <span>KUR Transaction Ledger</span>
      </div>
      <div className="transaction-container">
        <div className="transaction-page">
          <Panel>
            <div className="d-flex flex-row align-items-center justify-content-between">
              <BodyText className="font-weight-bold">Total Transaksi: {kurTransactions.length}</BodyText>
              <div className="d-flex flex-row align-items-center">
                <Input containerClassName="mx-2" type="text" placeholder="Cari Transaksi"/>
                <Input containerClassName="mx-2" label="URUTKAN TRANSAKSI" type="select">
                  <option>Transaksi Terbaru</option>
                </Input>
                <Input containerClassName="mx-2" label="FILTER" type="select">
                  <option>Transaksi Terbaru</option>
                </Input>
                <Input containerClassName="mx-2" label="TANGGAL" type="date" />
              </div>
            </div>
            <Table>
              <TableHead>
                <TableRow>
                  <TableHeadCell>Transaction Date</TableHeadCell>
                  <TableHeadCell>User</TableHeadCell>
                  <TableHeadCell>User ID</TableHeadCell>
                  <TableHeadCell>Transaction ID</TableHeadCell>
                  <TableHeadCell>Bank</TableHeadCell>
                  <TableHeadCell>Category</TableHeadCell>
                  <TableHeadCell>Status</TableHeadCell>
                  <TableHeadCell>Nominal</TableHeadCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {renderKURTransactions()}
              </TableBody>
            </Table>
          </Panel>
        </div>
      </div>
    </ParentContainer>
  );
}
