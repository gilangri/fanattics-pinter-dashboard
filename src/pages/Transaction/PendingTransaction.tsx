/* eslint-disable @typescript-eslint/no-unused-vars */
import Axios from "axios";
import moment from "moment";
import React, { useState, useEffect } from "react";
import { DocumentAdd16 } from "@carbon/icons-react";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
  Panel,
  ParentContainer,
  Input,
  Button,
} from "../../components";
import { API_URL } from "../../constants";

export default function PendingTransaction() {
  const [pendingTransactions, setPendingTransactions] = useState<any[]>([]);
  const [filterDate, setFilterDate] = useState<Date | string | null>("");

  const fetchPendingTransaction = async () => {
    try {
      const { data } = await Axios.get(`${API_URL}/transactions`, {
        params: {
          data: filterDate
        }
      });

      setPendingTransactions(data.result);
    } catch (err) {
      console.log(err);
      alert("Server error");
    }
  };

  const fetchFilteredData = () => {
    fetchPendingTransaction();
  }

  const renderPendingTransactions = () => {
    return pendingTransactions.map((val) => {
      return (
        <TableRow>
          <TableCell>{val?.buyer?.storeName}</TableCell>
          <TableCell>{val?.distributor?.bankAccountNumber}</TableCell>
          <TableCell>{val?.distributor?.name}</TableCell>
          <TableCell>{val?.invoiceCode}</TableCell>
          <TableCell>
            {new Intl.NumberFormat("id-ID", {
              style: "currency",
              currency: "IDR",
            })
              .format(val?.price)
              .slice(0, -3)}
          </TableCell>
          <TableCell>
            {new Intl.NumberFormat("id-ID", {
              style: "currency",
              currency: "IDR",
            })
              .format(val?.taxFee)
              .slice(0, -3)}
          </TableCell>
          <TableCell>
            {new Intl.NumberFormat("id-ID", {
              style: "currency",
              currency: "IDR",
            })
              .format(val?.price + val?.taxFee)
              .slice(0, -3)}
          </TableCell>
        </TableRow>
      );
    });
  };

  const generateExcelFile = () => {
    // Generate workbook
  };

  useEffect(() => {
    fetchPendingTransaction();
  }, []);

  return (
    <ParentContainer title="Transaction" className="admin-catalogue-container">
      <div className="renew-data font-weight-bold">
        <span>Pending Transaction Ledger</span>
      </div>
      <div className="transaction-container">
        <div className="transaction-page">
          <Panel>
            <div className="d-flex flex-row align-items-center justify-content-between">
              <div className="d-flex flex-row align-items-center">
                <Input
                  containerClassName="mx-2"
                  label="RECIPIENT"
                  type="select"
                >
                  <option>All</option>
                  <option>PT Agricon</option>
                </Input>
                <Input containerClassName="mx-2" label="TANGGAL" type="date" onChange={(e) => setFilterDate(e.target.valueAsDate)} />
              </div>
              <Button theme="text">
                Download Excel File &nbsp;&nbsp;&nbsp;&nbsp;{" "}
                <DocumentAdd16 color="purple" />
              </Button>
            </div>
            <Table>
              <TableHead>
                <TableRow>
                  <TableHeadCell>Origin Name</TableHeadCell>
                  <TableHeadCell>Virtual Account</TableHeadCell>
                  <TableHeadCell>Recipient Name</TableHeadCell>
                  <TableHeadCell>Invoice ID</TableHeadCell>
                  <TableHeadCell>Nominal</TableHeadCell>
                  <TableHeadCell>Tax</TableHeadCell>
                  <TableHeadCell>Nominal + Tax</TableHeadCell>
                </TableRow>
              </TableHead>

              <TableBody>{renderPendingTransactions()}</TableBody>
            </Table>
          </Panel>
        </div>
      </div>
    </ParentContainer>
  );
}
