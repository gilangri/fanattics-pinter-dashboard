import "./form.scss";

import {
  Button,
  ContainerBody,
  ContainerBodyContent,
  ContainerHead,
  Input,
  ParentContainer,
  SingleImageUploader,
} from "../../../components";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../redux/store";
import { COMPANY_PARTNER_FORM } from "../_state/interfaces/companyPartnerForm";
import { useHistory } from "react-router-dom";
import { createCompanyPartner } from "../_state/actions/companyPartnerForm";

export default function CompanyPartnerFormPage() {
  const dispatch = useDispatch();
  const history = useHistory();

  const { loading } = useSelector((state: RootState) => state.companyPartner);
  const {
    abbreviation,
    collectablePercentage,
    contactNumber,
    name,
    profilePicture,
    serverDomain,
  } = useSelector((state: RootState) => state.companyPartnerForm);
  const form = useSelector((state: RootState) => state.companyPartnerForm);

  return (
    <ParentContainer title="Tambah Company Partner">
      <ContainerHead backButton />

      <ContainerBody>
        <ContainerBodyContent className="company-partner-form body">
          <div>Tambah Company Partner Baru</div>
          <div className="form-panel">
            <div className="panel-content">
              <SingleImageUploader
                className="mb-4"
                file={profilePicture}
                onChange={(payload) =>
                  dispatch({
                    type: COMPANY_PARTNER_FORM.SET_VALUE,
                    name: "profilePicture",
                    payload,
                  })
                }
                handleDelete={() =>
                  dispatch({
                    type: COMPANY_PARTNER_FORM.SET_VALUE,
                    name: "profilePicture",
                    payload: undefined,
                  })
                }
              />

              <Input
                label="Nama Company Partner"
                name="name"
                placeholder="Masukkan Nama Company Partner"
                value={name}
                onChange={({ target }) =>
                  dispatch({
                    type: COMPANY_PARTNER_FORM.SET_VALUE,
                    name: target.name,
                    payload: target.value,
                  })
                }
              />

              <Input
                label="Inisial Nama Perusahaan"
                name="abbreviation"
                placeholder="Masukkan Inisial Nama Perusahaan"
                value={abbreviation}
                onChange={({ target }) =>
                  dispatch({
                    type: COMPANY_PARTNER_FORM.SET_VALUE,
                    name: target.name,
                    payload: target.value,
                  })
                }
              />

              <Input
                label="Nomor Telepon"
                name="contactNumber"
                placeholder="Masukkan Nomor Telepon"
                value={contactNumber}
                onChange={({ target }) =>
                  dispatch({
                    type: COMPANY_PARTNER_FORM.SET_VALUE,
                    name: target.name,
                    payload: target.value,
                  })
                }
              />

              <Input
                label="Nama Domain Server"
                name="serverDomain"
                placeholder="Masukkan Nama Domain Server"
                value={serverDomain}
                onChange={({ target }) =>
                  dispatch({
                    type: COMPANY_PARTNER_FORM.SET_VALUE,
                    name: target.name,
                    payload: target.value,
                  })
                }
              />

              <div className="row">
                <Input
                  label="Nama Perusahaan Afiliasi"
                  containerClassName="col-6"
                  value="PT Fanatik Teknologi"
                  disabled
                />

                <Input
                  containerClassName="col-6"
                  append="%"
                  label="Collectable Percentage"
                  name="collectablePercentage"
                  placeholder="Masukkan Collectable Percentage"
                  value={collectablePercentage}
                  onChange={({ target }) =>
                    dispatch({
                      type: COMPANY_PARTNER_FORM.SET_VALUE,
                      name: target.name,
                      payload: !target.value ? "" : target.value,
                    })
                  }
                />
              </div>

              <div className="button-action">
                <Button
                  minWidth={100}
                  disabled={loading}
                  onClick={history.goBack}
                  theme="secondary"
                >
                  Batalkan
                </Button>
                &nbsp; &nbsp;
                <Button
                  minWidth={100}
                  disabled={loading}
                  isLoading={loading}
                  onClick={() =>
                    dispatch(createCompanyPartner({ body: form, history: history }))
                  }
                  theme="primary"
                >
                  Simpan
                </Button>
              </div>
            </div>
          </div>
        </ContainerBodyContent>
      </ContainerBody>
    </ParentContainer>
  );
}
