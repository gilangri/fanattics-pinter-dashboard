/* eslint-disable @typescript-eslint/no-unused-vars */
import "./companyPartnerDetail.scss";

import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  ContainerBody,
  ContainerBodyContent,
  ContainerHead,
  DetailContainer,
  Panel,
  Tabs,
} from "../../../components";
import { RootState } from "../../../redux/store";
import { COMPANY_PARTNER } from "../_state/interfaces/companyPartner";
import { PanelDistributorCompanyPartner } from "./_components";
import { FormRevenueSharePercentage } from "./FormRevenueSharePercentage";
import {
  deleteCompanyPartner,
  updateCollectablePercentage,
  updateRevenueSharePercentage,
} from "../_state/actions/companyPartner";
import { sortArrByKey } from "../../../utils";
import Confirmation from "../../../components/Popup/Confirmation";
import { useHistory } from "react-router-dom";
import { FormCollectablePercentage } from "./FormCollectablePercentage";
import PanelPartnerCompanyPartner from "./_components/PanelPartner";
import { useEffect } from "react";

export default function CompanyPartnerDetailPage() {
  const dispatch = useDispatch();
  const history = useHistory();

  const {
    isDeleting,
    isMenuOpen,
    isEditCollectable,
    item,
    revenueSharePercentage,
    selectedDistributor,
    tab,
    loading,
  } = useSelector((state: RootState) => state.companyPartner);

  useEffect(() => {
    dispatch({ type: COMPANY_PARTNER.SET_TAB, payload: "distributors" });
  }, [dispatch]);

  return (
    <DetailContainer title="Company Partner Detail">
      <Confirmation
        isOpen={isDeleting}
        title={`Hapus ${item?.name}?`}
        confirmButtonTheme="danger"
        isLoading={loading}
        toggle={() => dispatch({ type: COMPANY_PARTNER.TOGGLE_DELETING })}
        onCancel={() => dispatch({ type: COMPANY_PARTNER.TOGGLE_DELETING })}
        onConfirm={async () => {
          if (!item) return;
          await Promise.all([dispatch(deleteCompanyPartner(item._id))]);
          history.goBack();
        }}
      />

      <FormRevenueSharePercentage
        isLoading={loading}
        isOpen={revenueSharePercentage !== 0}
        toggle={() => {
          dispatch({
            type: COMPANY_PARTNER.SET,
            payload: { revenueSharePercentage: 0 },
          });
        }}
        companyName={selectedDistributor?.name}
        onConfirm={(value) =>
          dispatch(
            updateRevenueSharePercentage({
              companyId: item?._id || "",
              distributorId: selectedDistributor?._id || "",
              body: { revenueSharePercentage: value },
            })
          )
        }
      />

      <FormCollectablePercentage
        isLoading={loading}
        isOpen={isEditCollectable}
        companyName="PT Fanatik Teknologi"
        toggle={() => dispatch({ type: COMPANY_PARTNER.TOGGLE_COLLECTABLE })}
        onConfirm={(value) => {
          dispatch(
            updateCollectablePercentage({
              companyId: item?._id || "",
              body: { collectablePercentage: value },
            })
          );
        }}
      />

      <ContainerHead
        title={item?.name || ""}
        backButton
        imageLogo={item?.profilePicture}
        subtitle={[
          { "Partner ID": item?._id || "" },
          { "Contact Info": item?.contactNumber || "" },
        ]}
      >
        <Button
          theme="danger"
          minWidth={100}
          disabled={isDeleting}
          onClick={() => dispatch({ type: COMPANY_PARTNER.TOGGLE_DELETING })}
        >
          DELETE
        </Button>
      </ContainerHead>

      <ContainerBody className="company-partner-detail">
        <ContainerBodyContent>
          <Panel padding="16">
            <Tabs
              activeTab={tab}
              options={[
                "Afiliasi Distributor",
                "Afiliasi Partner",
                "Profil Perusahaan",
              ]}
              slug={["distributors", "partner", "profile"]}
              setActiveTab={(payload) =>
                dispatch({ type: COMPANY_PARTNER.SET_TAB, payload })
              }
              textAlign="center"
              width={175}
            />

            {tab === "distributors" ? (
              <PanelDistributorCompanyPartner
                data={sortArrByKey(item?.distributors || [], "name")}
                isMenuOpen={isMenuOpen}
                selectedData={selectedDistributor}
              />
            ) : tab === "partner" ? (
              <PanelPartnerCompanyPartner item={item} />
            ) : null}
          </Panel>
        </ContainerBodyContent>
      </ContainerBody>
    </DetailContainer>
  );
}
