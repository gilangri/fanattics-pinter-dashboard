import "./companyPartnerDetail.scss";

import { Button, Input, Modal, ModalBody, ModalHeader } from "../../../components";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../redux/store";
import { COMPANY_PARTNER } from "../_state/interfaces/companyPartner";

interface Props {
  companyName?: string;
  isOpen?: boolean;
  isLoading?: boolean;
  onConfirm?: (value: number) => void;
  toggle?: () => void;
}
export function FormRevenueSharePercentage({
  companyName = "",
  isOpen,
  isLoading,
  onConfirm,
  toggle,
}: Props) {
  const dispatch = useDispatch();

  const { revenueSharePercentage } = useSelector(
    (state: RootState) => state.companyPartner
  );

  const onChange = (string: string) => {
    var payload = 0;
    !string ? (payload = 0) : (payload = parseInt(string));
    dispatch({
      type: COMPANY_PARTNER.SET_VALUE,
      name: "revenueSharePercentage",
      payload,
    });
  };

  const handleSimpan = () => {
    if (onConfirm) onConfirm(revenueSharePercentage / 100);
  };

  return (
    <Modal isOpen={isOpen} size="md">
      <ModalHeader title="Edit Revenue Sharing" toggle={toggle} />
      <ModalBody className="popup-form-sharing-partner-distributor">
        <div className="row">
          <Input
            containerClassName="col-12"
            label="Nama Perusahaan"
            value={companyName}
            disabled
          />
          <Input
            containerClassName="col-6"
            label="Revenue Share Percentage"
            value={revenueSharePercentage.toString()}
            onChange={({ target }) => onChange(target.value)}
            append="%"
          />
        </div>

        <div className="button-action">
          <Button
            disabled={isLoading}
            isLoading={isLoading}
            minWidth={150}
            onClick={handleSimpan}
            theme="primary-radius"
          >
            Simpan
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
}
