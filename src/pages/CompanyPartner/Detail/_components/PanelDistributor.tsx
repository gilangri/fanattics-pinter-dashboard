/* eslint-disable @typescript-eslint/no-unused-vars */
import { FC } from "react";
import moment from "moment";
import {
  Dropdown,
  DropdownItem,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../../../components";
import { IDistributor } from "../../../../redux/_interface";
import { useDispatch } from "react-redux";
import { COMPANY_PARTNER } from "../../_state/interfaces/companyPartner";

interface P {
  data?: IDistributor[];
  isMenuOpen?: IDistributor;
  selectedData?: IDistributor;
}
export const PanelDistributorCompanyPartner: FC<P> = ({
  data,
  isMenuOpen,
  selectedData,
}) => {
  const dispatch = useDispatch();

  return (
    <div className="panel-distributor-company-partner row">
      <div className="col-9 mt-4">
        <Table>
          <TableHead>
            <TableRow>
              <TableHeadCell width="50%">Distributor</TableHeadCell>
              <TableHeadCell width="20%">Persentase</TableHeadCell>
              <TableHeadCell width="20%">Aktif Sejak</TableHeadCell>
              <TableHeadCell width="10%">&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {!data || !data.length
              ? null
              : data.map((item, i) => {
                  return (
                    <TableRow key={i}>
                      <TableCell>{item.name}</TableCell>
                      <TableCell>{item.revenueSharePercentage * 100}%</TableCell>
                      <TableCell>
                        {moment(item.createdAt).format("DD MMMM YYYY")}
                      </TableCell>
                      <TableCell>
                        <Dropdown
                          isOpen={isMenuOpen?._id === item._id}
                          toggle={() => {
                            dispatch({
                              type: COMPANY_PARTNER.TOGGLE_DROPDOWN_MENU,
                              payload: item,
                            });
                          }}
                        >
                          <DropdownItem
                            onClick={() => {
                              dispatch({
                                type: COMPANY_PARTNER.SET,
                                payload: {
                                  selectedDistributor: isMenuOpen,
                                  revenueSharePercentage:
                                    (item.revenueSharePercentage || 0) *
                                    100,
                                },
                              });
                            }}
                          >
                            Edit Persentase
                          </DropdownItem>
                        </Dropdown>
                      </TableCell>
                    </TableRow>
                  );
                })}
          </TableBody>
        </Table>
      </div>
    </div>
  );
};
