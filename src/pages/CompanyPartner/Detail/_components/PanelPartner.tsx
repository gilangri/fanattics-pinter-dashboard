import moment from "moment";
import { useState } from "react";
import { useDispatch } from "react-redux";
import {
  Dropdown,
  DropdownItem,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
} from "../../../../components";
import { ICompanyPartner } from "../../../../redux/_interface";
import { COMPANY_PARTNER } from "../../_state/interfaces/companyPartner";

interface P {
  item?: ICompanyPartner;
}
export default function PanelPartnerCompanyPartner({ item }: P) {
  const dispatch = useDispatch();

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="panel-partner-company-partner row mt-4">
      <div className="col-6">
        <Table>
          <TableHead>
            <TableRow>
              <TableHeadCell width="50%">Partner Perusahaan</TableHeadCell>
              <TableHeadCell width="20%">Persentase</TableHeadCell>
              <TableHeadCell width="20%">Aktif Sejak</TableHeadCell>
              <TableHeadCell width="10%">&nbsp;</TableHeadCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>PT Fanatik Teknologi</TableCell>
              <TableCell>{(item?.collectablePercentage || 0) * 100}%</TableCell>
              <TableCell>{moment(item?.createdAt).format("DD MMMM YYYY")}</TableCell>
              <TableCell>
                <Dropdown isOpen={isOpen} toggle={toggle}>
                  <DropdownItem
                    onClick={() => {
                      let collectable = (item?.collectablePercentage || 0) * 100;
                      dispatch({
                        type: COMPANY_PARTNER.SET,
                        payload: {
                          isEditCollectable: true,
                          collectablePercentage: collectable,
                        },
                      });
                    }}
                  >
                    Edit Persentase
                  </DropdownItem>
                </Dropdown>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    </div>
  );
}
