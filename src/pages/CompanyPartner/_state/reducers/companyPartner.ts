import {
  COMPANY_PARTNER,
  ICompanyPartnerAction,
  ICompanyPartnerState,
} from "../interfaces/companyPartner";

const init_state: ICompanyPartnerState = {
  data: [],
  filtered: [],

  item: undefined,

  tab: "distributors",
  isMenuOpen: undefined,
  isEditCollectable: false,

  selectedDistributor: undefined,
  revenueSharePercentage: 0,
  collectablePercentage: 0,

  isDeleting: false,

  searchQuery: "",
  lastUpdate: new Date(),
  loading: false,
  error: "",
};

export function companyPartnerReducer(
  state = init_state,
  { payload, name, type }: ICompanyPartnerAction
): ICompanyPartnerState {
  switch (type) {
    case COMPANY_PARTNER.REQUEST:
      return { ...state, loading: true, error: "" };
    case COMPANY_PARTNER.FAILED:
      return { ...state, loading: false, error: payload };

    case COMPANY_PARTNER.SET_COMPANY_PARTNERS:
      return { ...state, loading: false, data: payload };
    case COMPANY_PARTNER.SET_COMPANY_PARTNER:
      return { ...state, loading: false, item: payload };

    case COMPANY_PARTNER.SET:
      return { ...state, ...payload };
    case COMPANY_PARTNER.SET_VALUE:
      return { ...state, [name]: payload };
    case COMPANY_PARTNER.SET_TAB:
      return { ...state, tab: payload };

    case COMPANY_PARTNER.TOGGLE_DROPDOWN_MENU:
      return payload._id === state.isMenuOpen?._id
        ? { ...state, isMenuOpen: undefined }
        : { ...state, isMenuOpen: payload };
    case COMPANY_PARTNER.TOGGLE_DELETING:
      return { ...state, isDeleting: !state.isDeleting };
    case COMPANY_PARTNER.TOGGLE_COLLECTABLE:
      return { ...state, isEditCollectable: !state.isEditCollectable };

    case COMPANY_PARTNER.ON_REFRESH:
      return { ...state, lastUpdate: new Date() };

    case COMPANY_PARTNER.RESET:
      return init_state;
    default:
      return state;
  }
}
