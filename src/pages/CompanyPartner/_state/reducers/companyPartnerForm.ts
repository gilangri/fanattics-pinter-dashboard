import {
  COMPANY_PARTNER_FORM,
  ICompanyPartnerFormAction,
  ICompanyPartnerFormState,
} from "../interfaces/companyPartnerForm";

const init_state: ICompanyPartnerFormState = {
  name: "",
  abbreviation: "",
  contactNumber: "",
  profilePicture: undefined,
  serverDomain: "",
  collectablePercentage: "",
};

export function companyPartnerFormReducer(
  state = init_state,
  { payload, name, type }: ICompanyPartnerFormAction
): ICompanyPartnerFormState {
  switch (type) {
    case COMPANY_PARTNER_FORM.SET:
      return { ...state, ...payload };
    case COMPANY_PARTNER_FORM.SET_VALUE:
      return { ...state, [name]: payload };

    case COMPANY_PARTNER_FORM.RESET:
      return init_state;
    default:
      return state;
  }
}
