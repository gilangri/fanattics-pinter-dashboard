import { AnyAction } from "redux";
import { ICompanyPartner, IDistributor } from "../../../../redux/_interface";

export interface ICompanyPartnerState {
  data: ICompanyPartner[];
  filtered: ICompanyPartner[];

  item?: ICompanyPartner;

  tab: "distributors" | "partner" | "profile";
  isMenuOpen?: IDistributor;
  isEditCollectable: boolean;

  selectedDistributor?: IDistributor;
  revenueSharePercentage: number;
  collectablePercentage: number;

  isDeleting: boolean;

  searchQuery: string;
  lastUpdate: Date;
  loading: boolean;
  error: string;
}

export enum COMPANY_PARTNER {
  REQUEST = "COMPANY_PARTNER_REQUEST",
  FAILED = "COMPANY_PARTNER_FAILED",
  RESET = "COMPANY_PARTNER_RESET",
  SET_COMPANY_PARTNER = "COMPANY_PARTNER_SET_COMPANY_PARTNER",
  SET_COMPANY_PARTNERS = "COMPANY_PARTNER_SET_COMPANY_PARTNERS",
  SET = "COMPANY_PARTNER_SET",
  SET_VALUE = "COMPANY_PARTNER_SET_VALUE",
  SET_TAB = "COMPANY_PARTNER_SET_TAB",

  TOGGLE_DROPDOWN_MENU = "COMPANY_PARTNER_DROPDOWN_MENU",
  TOGGLE_DELETING = "COMPANY_PARTNER_DELETING",
  TOGGLE_COLLECTABLE = "COMPANY_PARTNER_COLLECTABLE",

  ON_REFRESH = "COMPANY_PARTNER_ON_REFRESH",
}

export interface ICompanyPartnerAction extends AnyAction {
  payload: any;
  name: string;
  type: COMPANY_PARTNER;
}
