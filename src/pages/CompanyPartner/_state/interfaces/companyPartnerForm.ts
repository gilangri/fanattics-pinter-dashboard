import { AnyAction } from "redux";

export interface ICompanyPartnerFormState {
  name: string;
  abbreviation: string;
  contactNumber: string;
  profilePicture?: File;
  serverDomain: string;
  collectablePercentage: string;
}

export enum COMPANY_PARTNER_FORM {
  RESET = "COMPANY_PARTNER_FORM_RESET",
  SET = "COMPANY_PARTNER_FORM_SET",
  SET_VALUE = "COMPANY_PARTNER_FORM_SET_VALUE",
}

export interface ICompanyPartnerFormAction extends AnyAction {
  payload: any;
  name: string;
  type: COMPANY_PARTNER_FORM;
}
