import axios from "axios";
import { API_URL, optionsFormData } from "../../../../constants";
import { ICompanyPartner } from "../../../../redux/_interface";
import { sortArrByKey } from "../../../../utils";
import { COMPANY_PARTNER } from "../interfaces/companyPartner";
import { COMPANY_PARTNER_FORM } from "../interfaces/companyPartnerForm";

export const getCompanyPartner =
  (id: string = "") =>
  async (dispatch: any) => {
    try {
      dispatch({ type: COMPANY_PARTNER.REQUEST });

      if (id) {
        const { data } = await axios.get(`${API_URL}/company-partners/${id}`);
        const payload = data.result;
        dispatch({ type: COMPANY_PARTNER.SET_COMPANY_PARTNER, payload });
        // do something here
      } else {
        const { data } = await axios.get(`${API_URL}/company-partners`);
        const payload = !data.result.length ? [] : sortArrByKey(data.result, "name");

        dispatch({ type: COMPANY_PARTNER.SET_COMPANY_PARTNERS, payload });
      }
    } catch (err) {
      console.error(err);
      dispatch({ type: COMPANY_PARTNER.FAILED, payload: err });
    }
  };

/**
 * Filter data by search query based on it's name
 * @param data fetched data
 * @param searchQuery inputted search query string
 * @returns filtered data based on searchQuery
 */
export const filterCompanyPartner =
  (data: ICompanyPartner[] = [], searchQuery: string = "") =>
  async (dispatch: any) => {
    const payload = !data.length
      ? []
      : data && !searchQuery
      ? data
      : data.filter((item) => {
          const query = searchQuery.toLowerCase();
          const name = item.name.toLowerCase();
          const initial = item.abbreviation.toLowerCase();
          return name.includes(query) || initial.includes(query);
        });

    dispatch({ type: COMPANY_PARTNER.SET_VALUE, name: "filtered", payload });
  };

export const postCompanyPartner =
  (body: FormData, history: any) => async (dispatch: any) => {
    try {
      dispatch({ type: COMPANY_PARTNER.REQUEST });
      axios.post(`${API_URL}/company-partners`, body, optionsFormData);

      // await Promise.all([dispatch(getCompanyPartner())]);

      dispatch({ type: COMPANY_PARTNER_FORM.RESET });
      history.goBack();
    } catch (err) {
      console.error(err);
      dispatch({ type: COMPANY_PARTNER.FAILED, payload: err });
    }
  };

interface IUpdateRevenueShare {
  companyId: string;
  distributorId: string;
  body: Object;
}
export const updateRevenueSharePercentage =
  ({ companyId, distributorId, body }: IUpdateRevenueShare) =>
  async (dispatch: any) => {
    try {
      dispatch({ type: COMPANY_PARTNER.REQUEST });
      await axios.put(`${API_URL}/distributors/${distributorId}`, body);
      await Promise.all([
        dispatch(getCompanyPartner(companyId)),

        dispatch({
          type: COMPANY_PARTNER.SET,
          payload: {
            selectedDistributorName: "",
            revenueSharePercentage: 0,
          },
        }),
      ]);
    } catch (err) {
      console.error(err);
      dispatch({ type: COMPANY_PARTNER.FAILED, payload: err });
    }
  };

interface IUpdateCollectable {
  companyId: string;
  body: Object;
}
export const updateCollectablePercentage =
  ({ companyId, body }: IUpdateCollectable) =>
  async (dispatch: any) => {
    try {
      dispatch({ type: COMPANY_PARTNER.REQUEST });
      await axios.put(`${API_URL}/company-partners/${companyId}`, body);
      await Promise.all([dispatch(getCompanyPartner(companyId))]);
      dispatch({
        type: COMPANY_PARTNER.SET,
        payload: {
          isEditCollectable: false,
          collectablePercentage: 0,
        },
      });
    } catch (err) {
      console.error(err);
      dispatch({ type: COMPANY_PARTNER.FAILED });
    }
  };

export const deleteCompanyPartner = (id: string) => async (dispatch: any) => {
  try {
    dispatch({ type: COMPANY_PARTNER.REQUEST });
    await axios.delete(`${API_URL}/company-partners/${id}`);
    await Promise.all([dispatch(getCompanyPartner())]);
    dispatch({ type: COMPANY_PARTNER.TOGGLE_DELETING });
  } catch (err) {
    console.error(err);
    dispatch({ type: COMPANY_PARTNER.FAILED, payload: err });
  }
};
