import { ICompanyPartnerFormState } from "../interfaces/companyPartnerForm";
import { postCompanyPartner } from "./companyPartner";

interface ICreate {
  body: ICompanyPartnerFormState;
  history: any;
}
export const createCompanyPartner =
  ({ body, history }: ICreate) =>
  (dispatch: any) => {
    const {
      abbreviation,
      contactNumber,
      collectablePercentage,
      name,
      serverDomain,
      profilePicture,
    } = body;

    const isValid = validation(body);
    if (isValid.length) return alert(isValid.join("\n"));

    const formData = new FormData();
    if (profilePicture) formData.append("profilePicture", profilePicture);
    formData.append("abbreviation", abbreviation);
    formData.append("contactNumber", contactNumber);
    formData.append("name", name);
    formData.append("serverDomain", serverDomain);
    formData.append(
      "collectablePercentage",
      (parseInt(collectablePercentage) / 100).toString()
    );

    dispatch(postCompanyPartner(formData, history));
  };

const validation = (e: ICompanyPartnerFormState) => {
  let msg = [];

  if (!e.profilePicture) msg.push("- Logo Perusahaan wajib diisi.");
  if (!e.name) msg.push("- Nama Perusahaan wajib diisi.");
  if (!e.abbreviation) msg.push("- Inisial Nama Perusahaan wajib diisi.");
  if (!e.contactNumber) msg.push("- Nomor Telepon wajib diisi.");
  if (!e.serverDomain) msg.push("- Server Domain wajib diisi.");

  return msg;
};
