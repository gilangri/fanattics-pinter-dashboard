import { UilUserPlus } from "@iconscout/react-unicons";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  Button,
  ButtonIcon,
  ContainerBody,
  ContainerBodyContent,
  ContainerHead,
  Pagination,
  ParentContainer,
  Searchbar,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
  usePagination,
} from "../../components";
import { RootState } from "../../redux/store";
import { ICompanyPartner } from "../../redux/_interface";
import {
  filterCompanyPartner,
  getCompanyPartner,
} from "./_state/actions/companyPartner";
import { COMPANY_PARTNER } from "./_state/interfaces/companyPartner";

export default function CompanyPartnerPage() {
  const dispatch = useDispatch();
  const history = useHistory();

  const { data, filtered, lastUpdate, loading, searchQuery } = useSelector(
    (state: RootState) => state.companyPartner
  );

  const { next, prev, jump, currentData, pages, currentPage, maxPage } =
    usePagination({
      data: filtered,
      itemsPerPage: 10,
    });

  useEffect(() => {
    dispatch({ type: COMPANY_PARTNER.SET_VALUE, name: "data", payload: [] });
    dispatch(getCompanyPartner());
  }, [dispatch, lastUpdate]);

  useEffect(() => {
    dispatch(filterCompanyPartner(data, searchQuery));
  }, [data, dispatch, searchQuery]);

  return (
    <ParentContainer title="Daftar Partner">
      <ContainerHead
        title="Daftar Partner"
        onRefresh={() => dispatch({ type: COMPANY_PARTNER.ON_REFRESH })}
        refreshing={loading}
      >
        <Button
          theme="primary"
          onClick={() => history.push("/company-partner/form")}
        >
          <ButtonIcon type="prepend">
            <UilUserPlus />
          </ButtonIcon>
          &nbsp; Tambah Company Partner
        </Button>
      </ContainerHead>

      <ContainerBody>
        <ContainerBodyContent>
          <div className="row mb-4">
            <div className="col-3">
              <Searchbar
                autoFocus
                placeholder="Cari Perusahaan"
                value={searchQuery}
                onChange={(payload) =>
                  dispatch({
                    type: COMPANY_PARTNER.SET_VALUE,
                    name: "searchQuery",
                    payload,
                  })
                }
              />
            </div>
          </div>

          <Table>
            <TableHead>
              <TableRow>
                <TableHeadCell width="40%">Nama Perusahaan</TableHeadCell>
                <TableHeadCell width="15%">Inisial</TableHeadCell>
                <TableHeadCell width="25%">Nomor Kontak</TableHeadCell>
                <TableHeadCell width="20%">Total Distributor</TableHeadCell>
              </TableRow>
            </TableHead>

            <RenderBody
              data={currentData()}
              onClick={(payload) => {
                dispatch({
                  type: COMPANY_PARTNER.SET_COMPANY_PARTNER,
                  payload,
                });
                history.push("/company-partner/detail");
              }}
            />

            <TableFooter>
              <TableRow>
                <TableCell colSpan={10} align="center">
                  <Pagination
                    next={next}
                    prev={prev}
                    jump={jump}
                    pages={pages}
                    currentPage={currentPage}
                    maxPage={maxPage}
                  />
                </TableCell>
              </TableRow>
            </TableFooter>
          </Table>
        </ContainerBodyContent>
      </ContainerBody>
    </ParentContainer>
  );
}

const RenderBody = ({
  data,
  isLoading,
  onClick,
}: {
  data: ICompanyPartner[];
  isLoading?: boolean;
  onClick?: (item: ICompanyPartner) => void;
}) => {
  return (
    <TableBody>
      {!data.length ? (
        <TableRow>
          <TableCell align="center" colSpan={10}>
            {isLoading ? "loading" : "kosong"}
          </TableCell>
        </TableRow>
      ) : (
        data.map((item, i) => {
          return (
            <TableRow key={i} onClick={() => onClick && onClick(item)}>
              <TableCell width="40%">{item.name}</TableCell>
              <TableCell width="15%">{item.abbreviation}</TableCell>
              <TableCell width="25%">{item.contactNumber}</TableCell>
              <TableCell width="20%">{item.distributors.length}</TableCell>
            </TableRow>
          );
        })
      )}
    </TableBody>
  );
};
