import {
  DISTRIBUTOR_FORM,
  IDistributorFormAction,
  IDistributorFormState,
} from "../interfaces/distributorForm";

const init_state: IDistributorFormState = {
  isEditingRevenueShare: false,
  isPopupCreatedOpen: false,

  companyPartners: [],
  banks: [],

  companyPartner: undefined,
  selectedCompanyPartner: "",

  name: "",
  email: "",
  phoneNumber: "",
  profilePicture: undefined,
  bank: "",
  bankAccountNumber: "",
  revenueSharePercentage: 0,

  representatives: [
    {
      id: new Date().getMilliseconds().toString(),
      distributor: "",
      email: "",
      firstName: "",
      jobPosition: "",
      lastName: "",
      phoneNumber: "",
    },
  ],
};

export function distributorFormReducer(
  state = init_state,
  { payload, id, name, type }: IDistributorFormAction
): IDistributorFormState {
  switch (type) {
    case DISTRIBUTOR_FORM.TOGGLE_EDIT_REVENUE_SHARE:
      return { ...state, isEditingRevenueShare: !state.isEditingRevenueShare };
    case DISTRIBUTOR_FORM.TOGGLE_POPUP_CREATED:
      return { ...state, isPopupCreatedOpen: !state.isPopupCreatedOpen };

    case DISTRIBUTOR_FORM.SET:
      return { ...state, ...payload };
    case DISTRIBUTOR_FORM.SET_VALUE:
      return { ...state, [name]: payload };

    case DISTRIBUTOR_FORM.ADD_REPRESENTATIVE:
      return {
        ...state,
        representatives: [
          ...state.representatives,

          // add new representative with all empty value
          {
            id: new Date().getMilliseconds().toString(),
            distributor: "",
            email: "",
            firstName: "",
            jobPosition: "",
            lastName: "",
            phoneNumber: "",
          },
        ],
      };
    case DISTRIBUTOR_FORM.REMOVE_REPRESENTATIVE:
      return {
        ...state,

        // omit representative with the same id with payload sent
        representatives: state.representatives.filter((val) => val.id !== id),
      };
    case DISTRIBUTOR_FORM.CHANGE_REPRESENTATIVE_VALUE:
      return {
        ...state,
        representatives: state.representatives.map((item) => {
          // if item's id different with id sent
          // then return as it is (skip the changes)
          if (item.id !== id) return item;

          // if item's id similar with id sent, then
          // change the value of name sent
          return { ...item, [name]: payload };
        }),
      };

    case DISTRIBUTOR_FORM.RESET:
      return init_state;
    default:
      return state;
  }
}
