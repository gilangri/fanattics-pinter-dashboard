import {
  IRepresentativeFormAction,
  IRepresentativeFormState,
  REPRESENTATIVE_FORM,
} from "../interfaces/representativeForm";

const init_state: IRepresentativeFormState = {
  isOpen: false,
  isDeleting: undefined,
  isEditing: undefined,

  _id: "",
  distributor: "",
  email: "",
  firstName: "",
  jobPosition: "",
  lastName: "",
  phoneNumber: "",
};

export function representativeFormReducer(
  state = init_state,
  { payload, name, type }: IRepresentativeFormAction
): IRepresentativeFormState {
  switch (type) {
    case REPRESENTATIVE_FORM.SET:
      return { ...state, ...payload };
    case REPRESENTATIVE_FORM.SET_VALUE:
      return { ...state, [name]: payload };

    case REPRESENTATIVE_FORM.SET_EDIT:
      return { ...state, isOpen: true, isEditing: payload, ...payload };

    case REPRESENTATIVE_FORM.TOGGLE_FORM:
      return { ...state, isOpen: !state.isOpen };
    case REPRESENTATIVE_FORM.TOGGLE_EDIT:
      return { ...state, isEditing: !payload ? undefined : payload };
    case REPRESENTATIVE_FORM.TOGGLE_DELETE:
      return { ...state, isDeleting: !payload ? undefined : payload };

    case REPRESENTATIVE_FORM.RESET:
      return init_state;
    default:
      return state;
  }
}
