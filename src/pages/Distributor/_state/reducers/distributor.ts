import {
  DISTRIBUTOR,
  IDistributorAction,
  IDistributorState,
} from "../interfaces/distributor";

const init_state: IDistributorState = {
  data: [],
  filtered: [],

  item: undefined,
  umkmUsers: [],

  tab: "overview",
  isMenuOpen: false,

  searchQuery: "",
  lastUpdate: new Date(),

  loading: false,
  error: "",
};

export function distributorReducer(
  state = init_state,
  { payload, name, type }: IDistributorAction
): IDistributorState {
  switch (type) {
    case DISTRIBUTOR.REQUEST:
      return { ...state, loading: true, error: "" };
    case DISTRIBUTOR.FAILED:
      return { ...state, loading: false, error: payload };

    case DISTRIBUTOR.SET_DISTRIBUTORS:
      return { ...state, loading: false, data: payload };
    case DISTRIBUTOR.SET_DISTRIBUTOR:
      return { ...state, loading: false, item: payload };
    case DISTRIBUTOR.SET_UMKM_USERS:
      return { ...state, loading: false, umkmUsers: payload };

    case DISTRIBUTOR.SET:
      return { ...state, ...payload };
    case DISTRIBUTOR.SET_VALUE:
      return { ...state, [name]: payload };
    case DISTRIBUTOR.SET_TAB:
      return { ...state, tab: payload };
    case DISTRIBUTOR.TOGGLE_DROPDOWN_MENU:
      return { ...state, isMenuOpen: !state.isMenuOpen };

    case DISTRIBUTOR.RESET:
      return init_state;
    default:
      return state;
  }
}
