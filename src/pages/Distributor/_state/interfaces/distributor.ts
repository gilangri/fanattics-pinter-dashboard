import { AnyAction } from "redux";
import { IMember } from "../../../../redux/_interface";
import { IDistributor } from "../../../../redux/_interface/distributor";

export interface IDistributorState {
  data: IDistributor[];
  filtered: IDistributor[];

  item?: IDistributor;
  umkmUsers: IMember[];

  tab: "overview" | "sharing_partners" | "members";
  isMenuOpen: boolean;

  searchQuery: string;
  lastUpdate: Date;

  loading: boolean;
  error: string;
}

export enum DISTRIBUTOR {
  REQUEST = "DISTRIBUTOR_REQUEST",
  FAILED = "DISTRIBUTOR_FAILED",
  RESET = "DISTRIBUTOR_RESET",
  SET_DISTRIBUTORS = "DISTRIBUTOR_SET_DISTRIBUTORS",
  SET_DISTRIBUTOR = "DISTRIBUTOR_SET_DISTRIBUTOR",
  SET_UMKM_USERS = "DISTRIBUTOR_SET_UMKM_USERS",
  SET = "DISTRIBUTOR_SET",
  SET_VALUE = "DISTRIBUTOR_SET_VALUE",
  SET_TAB = "DISTRIBUTOR_SET_TAB",
  TOGGLE_DROPDOWN_MENU = "DISTRIBUTOR_TOGGLE_DROPDOWN_MENU",
}

export interface IDistributorAction extends AnyAction {
  payload: any;
  name: string;
  type: DISTRIBUTOR;
}
