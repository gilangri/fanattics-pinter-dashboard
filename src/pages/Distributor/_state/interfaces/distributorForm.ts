import { AnyAction } from "redux";
import { IBank, ICompanyPartner } from "../../../../redux/_interface";

export interface IDistributorFormState {
  isEditingRevenueShare: boolean;
  isPopupCreatedOpen: boolean;

  companyPartners: ICompanyPartner[];
  banks: IBank[];

  companyPartner?: ICompanyPartner;
  selectedCompanyPartner: string;

  name: string;
  email: string;
  phoneNumber: string;
  profilePicture?: File;
  bank: string;
  bankAccountNumber: string;
  revenueSharePercentage: number;
  representatives: IRepresentativeForm[];
}

export enum DISTRIBUTOR_FORM {
  RESET = "DISTRIBUTOR_FORM_RESET",
  SET = "DISTRIBUTOR_FORM_SET",
  SET_VALUE = "DISTRIBUTOR_FORM_SET_VALUE",

  TOGGLE_EDIT_REVENUE_SHARE = "DISTRIBUTOR_TOGGLE_EDIT_REVENUE_SHARE",
  TOGGLE_POPUP_CREATED = "DISTRIBUTOR_TOGGLE_POPUP_CREATED",

  CHANGE_REPRESENTATIVE_VALUE = "DISTRIBUTOR_FORM_CHANGE_REPRESENTATIVE_VALUE",
  ADD_REPRESENTATIVE = "DISTRIBUTOR_FORM_ADD_REPRESENTATIVE",
  REMOVE_REPRESENTATIVE = "DISTRIBUTOR_FORM_REMOVE_REPRESENTATIVE",
}

export interface IDistributorFormAction extends AnyAction {
  payload: any;
  name: string;
  id: string;
  type: DISTRIBUTOR_FORM;
}

export interface IRepresentativeForm {
  id: string;
  distributor: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  jobPosition: string;
}
