import { AnyAction } from "redux";
import { IRepresentative } from "../../../../redux/_interface";

export interface IRepresentativeFormState {
  isOpen: boolean;
  isEditing?: IRepresentative;
  isDeleting?: IRepresentative;

  _id: string;
  distributor: string;
  firstName: string;
  lastName: string;
  phoneNumber: string;
  email: string;
  jobPosition: string;
}

export enum REPRESENTATIVE_FORM {
  RESET = "REPRESENTATIVE_FORM_RESET",

  TOGGLE_FORM = "REPRESENTATIVE_FORM_TOGGLE_FORM",
  TOGGLE_EDIT = "REPRESENTATIVE_FORM_TOGGLE_EDIT",
  TOGGLE_DELETE = "REPRESENTATIVE_FORM_TOGGLE_DELETE",

  SET = "REPRESENTATIVE_FORM_SET",
  SET_VALUE = "REPRESENTATIVE_FORM_SET_VALUE",
  SET_EDIT = "REPRESENTATIVE_FORM_SET_EDIT",
}

export interface IRepresentativeFormAction extends AnyAction {
  payload: any;
  name: string;
  type: REPRESENTATIVE_FORM;
}

// distributor: "60b26286b77a2b70c4bc1aee"
// email: "agus@berjaya.com"
// firstName: "Agus"
// jobPosition: "Kepala Penjualan"
// lastName: "Marwoto"
// phoneNumber: "086667676"
