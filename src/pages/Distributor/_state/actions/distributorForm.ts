import axios from "axios";
import { API_URL } from "../../../../constants";
import { sortArrByKey } from "../../../../utils";
import {
  DISTRIBUTOR_FORM,
  IDistributorFormState,
} from "../interfaces/distributorForm";
import { postDistributor } from "./distributor";

export const getDefaultData = () => async (dispatch: any) => {
  try {
    const resCompanyPartners = await axios.get(`${API_URL}/company-partners`);
    const resBanks = await axios.get(`${API_URL}/banks`);

    const banks = !resBanks.data.result.length
      ? []
      : sortArrByKey(resBanks.data.result, "name");

    const companyPartners = !resCompanyPartners.data.result.length
      ? []
      : sortArrByKey(resCompanyPartners.data.result, "name");

    dispatch({
      type: DISTRIBUTOR_FORM.SET,
      payload: { banks, companyPartners },
    });
  } catch (err) {
    console.error(err);
  }
};

interface ICreateDistributor {
  formState: IDistributorFormState;
  bodyAddress: {
    address: string;
    province: string;
    city: string;
    district: string;
    subdistrict: string;
    postalCode: number;
  };
}
export const createDistributor =
  ({ formState, bodyAddress }: ICreateDistributor) =>
  (dispatch: any) => {
    const {
      bank,
      bankAccountNumber,
      companyPartner,
      email,
      name,
      phoneNumber,
      profilePicture,
      representatives,
      revenueSharePercentage,
    } = formState;
    const { address, province, city, district, subdistrict, postalCode } =
      bodyAddress;

    let validationMessages = [];
    if (!profilePicture) validationMessages.push("- Foto distributor tidak valid");
    if (!name) validationMessages.push("- Nama wajib diisi");
    if (!email) validationMessages.push("- Email wajib diisi");
    if (!phoneNumber) validationMessages.push("- Nomor Telepon wajib diisi");

    if (!address || !province || !city || !district || !subdistrict || !postalCode)
      validationMessages.push("- Alamat belum lengkap");

    if (!bank) validationMessages.push("- Bank wajib diisi");
    if (!bankAccountNumber) validationMessages.push("- Nomor Rekening wajib diisi");
    if (!companyPartner) validationMessages.push("- Nama Perusahaan tidak valid");
    if (!revenueSharePercentage)
      validationMessages.push("- Revenue Share wajib diisi");

    const formData = new FormData();

    if (profilePicture) formData.append("profilePicture", profilePicture);
    formData.append("name", name);
    formData.append("phoneNumber", phoneNumber);
    formData.append("email", email);
    formData.append("bank", bank);
    formData.append("bankAccountNumber", bankAccountNumber);
    formData.append(
      "revenueSharePercentage",
      (revenueSharePercentage / 100).toString()
    );

    if (validationMessages.length) return alert(validationMessages.join("\n"));
    if (!companyPartner) return;

    dispatch(
      postDistributor({
        bodyAddress,
        bodyDistributor: formData,
        bodyRepresentatives: representatives,
        companyId: companyPartner._id,
      })
    );
  };
