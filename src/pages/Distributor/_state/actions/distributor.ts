/* eslint-disable @typescript-eslint/no-unused-vars */
import axios from "axios";
import { promises } from "dns";
import { API_URL, optionsFormData } from "../../../../constants";
import { IDistributor } from "../../../../redux/_interface/distributor";
import { sortArrByKey } from "../../../../utils";
import { DISTRIBUTOR } from "../interfaces/distributor";
import {
  DISTRIBUTOR_FORM,
  IRepresentativeForm,
} from "../interfaces/distributorForm";

export const getDistributor =
  (id: string = "") =>
  async (dispatch: any) => {
    try {
      dispatch({ type: DISTRIBUTOR.REQUEST });

      if (id) {
        const resDistributor = await axios.get(`${API_URL}/distributors/${id}`);
        const resUmkmUsers = await axios.get(`${API_URL}/users`);

        const distributor = resDistributor.data.result;

        const selectedUmkmUsers = resUmkmUsers.data.result;
        const allUmkmUsers = !selectedUmkmUsers.length
          ? []
          : selectedUmkmUsers.filter((item: any) => item.distributor?._id === id);

        dispatch({ type: DISTRIBUTOR.SET_DISTRIBUTOR, payload: distributor });
        dispatch({ type: DISTRIBUTOR.SET_UMKM_USERS, payload: allUmkmUsers });
      } else {
        const { data } = await axios.get(`${API_URL}/distributors`);
        const result = !data.result ? [] : data.result;
        const payload = sortArrByKey(result, "name");

        dispatch({ type: DISTRIBUTOR.SET_DISTRIBUTORS, payload });
      }
    } catch (err) {
      console.error(err);
      dispatch({ type: DISTRIBUTOR.FAILED, payload: err });
    }
  };

/**
 * Filter data by search query based on it's name
 * @param data fetched data
 * @param searchQuery inputted search query string
 * @returns filtered data based on searchQuery
 */
export const filterDistributor =
  (data: IDistributor[] = [], searchQuery: string = "") =>
  async (dispatch: any) => {
    const payload = !data.length
      ? []
      : data && !searchQuery
      ? data
      : data.filter((item) => {
          const query = searchQuery.toLowerCase();
          const name = item.name.toLowerCase();
          return name.includes(query);
        });

    dispatch({ type: DISTRIBUTOR.SET_VALUE, name: "filtered", payload });
  };

interface IEdit {
  id: string;
  body: Object;
}
export const editDistributor =
  ({ id, body }: IEdit) =>
  async (dispatch: any) => {
    try {
      dispatch({ type: DISTRIBUTOR.REQUEST });

      await axios.put(`${API_URL}/distributors/${id}`, body);
      await Promise.all([dispatch(getDistributor(id))]);
    } catch (err) {
      console.error(err);
      dispatch({ type: DISTRIBUTOR.FAILED, payload: err });
    }
  };

export const editRevenueSharePercentage =
  ({ id, body }: IEdit) =>
  async (dispatch: any) => {
    try {
      await Promise.all([
        dispatch(editDistributor({ id, body })),
        dispatch({
          type: DISTRIBUTOR.SET,
          payload: { isMenuOpen: false },
        }),
        dispatch({
          type: DISTRIBUTOR_FORM.SET,
          payload: { isEditingRevenueShare: false },
        }),
      ]);
    } catch (err) {
      console.error(err);
    }
  };

interface IPostDistributor {
  bodyAddress: Object;
  bodyDistributor: FormData;
  bodyRepresentatives: IRepresentativeForm[];
  companyId: string;
}
export const postDistributor =
  ({
    bodyAddress,
    bodyDistributor,
    bodyRepresentatives,
    companyId,
  }: IPostDistributor) =>
  async (dispatch: any) => {
    try {
      dispatch({ type: DISTRIBUTOR.REQUEST });

      const resAddress = await axios.post(`${API_URL}/addresses`, bodyAddress);

      bodyDistributor.append("companyPartner", companyId);
      bodyDistributor.append("address", resAddress.data.result._id);

      const resDistributor = await axios.post(
        `${API_URL}/distributors`,
        bodyDistributor,
        optionsFormData
      );

      const representatives = bodyRepresentatives.map((item) => {
        return {
          firstName: item.firstName,
          lastName: item.lastName,
          phoneNumber: item.phoneNumber,
          email: item.email,
          jobPosition: item.jobPosition,
          distributor: resDistributor.data.result._id,
        };
      });

      await axios.post(`${API_URL}/representatives`, representatives);

      await Promise.all([
        dispatch(getDistributor()),
        dispatch({ type: DISTRIBUTOR_FORM.TOGGLE_POPUP_CREATED }),
      ]);
    } catch (err) {
      console.error(err);
      dispatch({ type: DISTRIBUTOR.FAILED, payload: err });
    }
  };

export const deleteDistributor =
  (id: string, history: any) => async (dispatch: any) => {
    try {
      dispatch({ type: DISTRIBUTOR.REQUEST });
      await axios.delete(`${API_URL}/distributors/${id}`);
      await Promise.all([dispatch(getDistributor())]);
      history.goBack();
    } catch (err) {
      console.error(err);
      dispatch({ type: DISTRIBUTOR.FAILED, payload: err });
    }
  };
