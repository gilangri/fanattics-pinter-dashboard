import axios from "axios";
import { API_URL } from "../../../../constants";
import { DISTRIBUTOR } from "../interfaces/distributor";
import {
  IRepresentativeFormState,
  REPRESENTATIVE_FORM,
} from "../interfaces/representativeForm";
import { getDistributor } from "./distributor";

interface IPost {
  body: Object;
  distributorId?: string;
}
export const postRepresentative =
  ({ body, distributorId }: IPost) =>
  async (dispatch: any) => {
    if (!distributorId) return alert("Distributor tidak valid");

    try {
      dispatch({ type: DISTRIBUTOR.REQUEST });
      await axios.post(`${API_URL}/representatives`, [body]);
      await Promise.all([dispatch(getDistributor(distributorId))]);
      dispatch({ type: REPRESENTATIVE_FORM.RESET });
    } catch (err) {
      console.error(err);
      dispatch({ type: DISTRIBUTOR.FAILED, payload: err });
    }
  };

interface IUpdate {
  body: Object;
  distributorId?: string;
  id: string;
}
export const updateRepresentative =
  ({ body, distributorId, id }: IUpdate) =>
  async (dispatch: any) => {
    if (!distributorId) return alert("Distributor tidak valid");

    try {
      dispatch({ type: DISTRIBUTOR.REQUEST });
      await axios.put(`${API_URL}/representatives/${id}`, body);
      await Promise.all([dispatch(getDistributor(distributorId))]);
      dispatch({ type: REPRESENTATIVE_FORM.RESET });
    } catch (err) {
      console.error(err);
      dispatch({ type: DISTRIBUTOR.FAILED, payload: err });
    }
  };

interface IDelete {
  id?: string;
  distributorId?: string;
}
export const deleteRepresentative =
  ({ id, distributorId }: IDelete) =>
  async (dispatch: any) => {
    if (!id || !distributorId) return alert("ID tidak valid");

    try {
      dispatch({ type: DISTRIBUTOR.REQUEST });
      await axios.delete(`${API_URL}/representatives/${id}`);
      await Promise.all([dispatch(getDistributor(distributorId))]);
      dispatch({ type: REPRESENTATIVE_FORM.RESET });
    } catch (err) {
      console.error(err);
      dispatch({ type: DISTRIBUTOR.FAILED, payload: err });
    }
  };

interface ISave {
  form: IRepresentativeFormState;
  distributorId?: string;
  id?: string;
}
export const saveRepresentative =
  ({ form, distributorId, id }: ISave) =>
  (dispatch: any) => {
    const body = {
      email: form.email,
      firstName: form.firstName,
      lastName: form.lastName,
      jobPosition: form.jobPosition,
      phoneNumber: form.phoneNumber,
      distributor: distributorId,
    };

    if (id) {
      dispatch(updateRepresentative({ body, distributorId, id }));
    } else {
      dispatch(postRepresentative({ body, distributorId }));
    }
  };
