import "./distributorDetail.scss";

import { useDispatch, useSelector } from "react-redux";
import { Button, Input, Modal, ModalBody, ModalHeader } from "../../../components";
import { RootState } from "../../../redux/store";
import { REPRESENTATIVE_FORM } from "../_state/interfaces/representativeForm";

interface P {
  isLoading?: boolean;
  isOpen?: boolean;
  onConfirm: () => void;
  toggle: () => void;
}
export default function FormRepresentativeDistributor({
  isLoading,
  isOpen,
  onConfirm,
  toggle,
}: P) {
  const { isEditing, email, firstName, jobPosition, lastName, phoneNumber } =
    useSelector((state: RootState) => state.representativeForm);

  return (
    <Modal isOpen={isOpen} size="lg">
      <ModalHeader
        title={`${isEditing ? "Edit" : "Tambah"} Representative`}
        toggle={() => !isLoading && toggle()}
      />
      <ModalBody className="popup-form-representative-distributor">
        <div className="row">
          <CustomInput
            className="col-6"
            label="Nama Depan"
            name="firstName"
            value={firstName}
          />
          <CustomInput
            className="col-6"
            label="Nama Belakang"
            name="lastName"
            value={lastName}
          />
        </div>

        <div className="row">
          <CustomInput
            className="col-6"
            label="Nomor Telepon"
            name="phoneNumber"
            value={phoneNumber}
          />

          <CustomInput className="col-6" label="Email" name="email" value={email} />
        </div>

        <CustomInput
          label="Posisi Pekerjaan"
          name="jobPosition"
          value={jobPosition}
        />

        <div className="button-action">
          <Button
            disabled={isLoading}
            isLoading={isLoading}
            minWidth={150}
            onClick={onConfirm}
            theme="primary-radius"
          >
            Simpan
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
}

const CustomInput = ({
  className = "",
  label,
  name,
  value,
}: {
  className?: string;
  label: string;
  name: string;
  value: string;
}) => {
  const dispatch = useDispatch();

  return (
    <Input
      label={label}
      placeholder={`Masukkan ${label}`}
      containerClassName={className}
      name={name}
      value={value}
      onChange={({ target }) =>
        dispatch({
          type: REPRESENTATIVE_FORM.SET_VALUE,
          name: target.name,
          payload: target.value,
        })
      }
    />
  );
};
