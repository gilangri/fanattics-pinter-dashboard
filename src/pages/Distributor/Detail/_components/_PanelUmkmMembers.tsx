import {
  Pagination,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
  usePagination,
} from "../../../../components";
import { IMember } from "../../../../redux/_interface";

interface Props {
  data?: IMember[];
}
export function PanelUmkmMembersDistributor({ data }: Props) {
  const { next, prev, jump, currentData, pages, currentPage, maxPage } =
    usePagination({
      data: data || [],
      itemsPerPage: 10,
    });

  return (
    <div className="panel-umkm-members-distributor">
      <Table className="table-members">
        <TableHead>
          <TableRow>
            <TableHeadCell>User</TableHeadCell>
            <TableHeadCell>User ID</TableHeadCell>
            <TableHeadCell>Nama UMKM</TableHeadCell>
            <TableHeadCell>Nomor Telepon</TableHeadCell>
          </TableRow>
        </TableHead>

        <RenderBody data={currentData()} />

        <TableFooter>
          <TableRow>
            <TableCell colSpan={10} align="center">
              <Pagination
                next={next}
                prev={prev}
                jump={jump}
                pages={pages}
                currentPage={currentPage}
                maxPage={maxPage}
              />
            </TableCell>
          </TableRow>
        </TableFooter>
      </Table>
    </div>
  );
}

const RenderBody = ({
  data,
  onClick,
}: {
  data: IMember[];
  onClick?: (item: IMember) => void;
}) => {
  return (
    <TableBody>
      {!data.length
        ? null
        : data.map((item, i) => {
            return (
              <TableRow key={i} onClick={() => onClick && onClick(item)}>
                <TableCell style={{ width: "25%" }}>{item.fullName}</TableCell>
                <TableCell style={{ width: "25%" }}>{item._id}</TableCell>
                <TableCell style={{ width: "25%" }}>{item.storeName}</TableCell>
                <TableCell style={{ width: "25%" }}>{item.phoneNumber}</TableCell>
              </TableRow>
            );
          })}
    </TableBody>
  );
};
