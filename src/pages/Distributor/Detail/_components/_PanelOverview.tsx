/* eslint-disable @typescript-eslint/no-unused-vars */
import { UilPen, UilTrashAlt } from "@iconscout/react-unicons";
import { FC } from "react";
import { useDispatch } from "react-redux";
import { Button, ButtonIcon } from "../../../../components";
import { IDistributor } from "../../../../redux/_interface";
import { REPRESENTATIVE_FORM } from "../../_state/interfaces/representativeForm";

interface P {
  data?: IDistributor;
}
export const PanelOverviewDistributor: FC<P> = ({ children, data }) => {
  const dispatch = useDispatch();

  return (
    <div className="panel-overview-distributor">
      {children}

      <div className="panel-detail company-bank-account">
        <div className="title">Company Representative</div>

        <div className="bank-account">
          <div className="logo">{data?.bank?.name || ""}</div>
          <div className="bank-account-info">
            <span>Virtual Account</span>
            <span>{data?.bankAccountNumber}</span>
          </div>
        </div>
      </div>

      <div className="panel-detail company-representatives">
        <div className="title">Company Representative</div>

        <div className="representatives">
          {!data || !data.representatives || !data.representatives.length
            ? null
            : data.representatives.map((item, i) => {
                return (
                  <div key={i} className="detail-representative">
                    <div className="header">
                      <div className="fullname">
                        {item.firstName}&nbsp;{item.lastName}
                      </div>

                      <div className="action">
                        <Button
                          onClick={() => {
                            dispatch({
                              type: REPRESENTATIVE_FORM.SET_EDIT,
                              payload: item,
                            });
                          }}
                          theme="icon"
                        >
                          <ButtonIcon type="prepend">
                            <UilPen size={16} />
                          </ButtonIcon>
                          Edit
                        </Button>
                        &nbsp; &nbsp;
                        <Button
                          theme="icon"
                          onClick={() =>
                            dispatch({
                              type: REPRESENTATIVE_FORM.TOGGLE_DELETE,
                              payload: item,
                            })
                          }
                        >
                          <UilTrashAlt size={16} color="#E74353" />
                        </Button>
                      </div>
                    </div>

                    <div className="representative-info email">
                      <span>Email</span>
                      <span>{item.email}</span>
                    </div>

                    <div className="representative-info phonenumber">
                      <span>Phone Number</span>
                      <span>{item.phoneNumber}</span>
                    </div>

                    <div className="representative-info occupation">
                      <span>Occupation</span>
                      <span>{item.jobPosition}</span>
                    </div>
                  </div>
                );
              })}
        </div>

        <Button
          onClick={() => dispatch({ type: REPRESENTATIVE_FORM.TOGGLE_FORM })}
          className="m-0 mt-3"
          theme="link"
        >
          Tambah
        </Button>
      </div>
    </div>
  );
};
