/* eslint-disable @typescript-eslint/no-unused-vars */
import moment from "moment";
import { FC } from "react";
import { useDispatch } from "react-redux";
import { Dropdown, DropdownItem } from "../../../../components";
import { IDistributor } from "../../../../redux/_interface";
import { DISTRIBUTOR } from "../../_state/interfaces/distributor";
import { DISTRIBUTOR_FORM } from "../../_state/interfaces/distributorForm";

interface Props {
  data?: IDistributor;
  isMenuOpen?: boolean;
}
export const PanelSharingPartnersDistributor: FC<Props> = ({
  children,
  data,
  isMenuOpen = false,
}) => {
  const dispatch = useDispatch();

  const timestamp = !data ? "" : moment(data.updatetAt).format("DD MMMM YYYY");
  const today = moment();
  const created = moment(!data ? undefined : data.updatetAt);
  const diffInDays = today.diff(created, "days");

  return (
    <div className="panel-sharing-partners">
      {children}

      <div className="title">Active Sharing Partners</div>
      <div className="sharing-partner-item">
        <div className="name-info">{data?.companyPartner.name}</div>

        <div className="initial-code-info">{data?.companyPartner.abbreviation}</div>

        <div className="timestamp-info">
          Last changed: {timestamp} ({diffInDays} Days)
        </div>

        <div className="percentage">
          {!data ? 0 : data.revenueSharePercentage * 100}%
        </div>

        <div className="menu-action">
          <Dropdown
            isOpen={isMenuOpen}
            toggle={() =>
              dispatch({
                type: DISTRIBUTOR.TOGGLE_DROPDOWN_MENU,
              })
            }
          >
            <DropdownItem
              onClick={() => {
                dispatch({
                  type: DISTRIBUTOR_FORM.SET_VALUE,
                  name: "revenueSharePercentage",
                  payload: !data ? 0 : data.revenueSharePercentage * 100,
                });
                dispatch({ type: DISTRIBUTOR_FORM.TOGGLE_EDIT_REVENUE_SHARE });
              }}
            >
              Edit
            </DropdownItem>
          </Dropdown>
        </div>
      </div>
    </div>
  );
};
