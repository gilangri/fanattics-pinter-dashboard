/* eslint-disable @typescript-eslint/no-unused-vars */
import "./distributorDetail.scss";

import { FC, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  ContainerBody,
  ContainerBodyContent,
  ContainerHead,
  DetailContainer,
  Tabs,
} from "../../../components";
import { RootState } from "../../../redux/store";
import { joinAddress } from "../../../utils";
import { DISTRIBUTOR } from "../_state/interfaces/distributor";
import {
  deleteDistributor,
  editRevenueSharePercentage,
  getDistributor,
} from "../_state/actions/distributor";
import {
  PanelOverviewDistributor,
  PanelSharingPartnersDistributor,
  PanelUmkmMembersDistributor,
} from "./_components";
import { FormSharingPartnerDistributor } from "./FormSharingPartner";
import { DISTRIBUTOR_FORM } from "../_state/interfaces/distributorForm";
import Confirmation from "../../../components/Popup/Confirmation";
import { useHistory } from "react-router-dom";
import { REPRESENTATIVE_FORM } from "../_state/interfaces/representativeForm";
import {
  deleteRepresentative,
  saveRepresentative,
} from "../_state/actions/representativeForm";
import FormRepresentativeDistributor from "./FormRepresentative";

export default function DistributorDetailPage() {
  const dispatch = useDispatch();
  const history = useHistory();

  const { item, tab, umkmUsers, isMenuOpen, loading } = useSelector(
    (state: RootState) => state.distributor
  );
  const { isEditingRevenueShare } = useSelector(
    (state: RootState) => state.distributorForm
  );
  const isFormOpen = useSelector(
    (state: RootState) => state.representativeForm.isOpen
  );
  const form = useSelector((state: RootState) => state.representativeForm);
  const { isDeleting, isEditing } = useSelector(
    (state: RootState) => state.representativeForm
  );

  const [isDeletingDistributor, setIsDeletingDistributor] = useState(false);
  const toggleDeleting = () => setIsDeletingDistributor(!isDeletingDistributor);

  useEffect(() => {
    if (item) dispatch(getDistributor(item._id));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch]);

  return (
    <DetailContainer title="Distributor Detail">
      <Confirmation
        title={`Hapus ${item?.name}?`}
        confirmButtonTheme="danger"
        isLoading={loading}
        isOpen={isDeletingDistributor}
        toggle={toggleDeleting}
        onCancel={toggleDeleting}
        onConfirm={() =>
          !item ? null : dispatch(deleteDistributor(item._id, history))
        }
        size="md"
      />

      <ContainerHead
        title="Distributor Detail"
        backButton
        backButtonHandler={() => {
          dispatch({ type: DISTRIBUTOR.SET_TAB, payload: "overview" });
        }}
      >
        <Button
          onClick={toggleDeleting}
          theme="danger"
          disabled={isDeletingDistributor}
        >
          DELETE
        </Button>
      </ContainerHead>

      <ContainerBody className="distributor-detail">
        <ContainerBodyContent col={3} className="detail_head">
          <div className="profile-picture">
            <img src={item?.profilePicture || ""} alt="profile" />
          </div>

          <DetailInfoWrapper title="ID">{item?._id}</DetailInfoWrapper>
          <DetailInfoWrapper title="Email">{item?.email}</DetailInfoWrapper>
          <DetailInfoWrapper title="Contact Number">
            {item?.phoneNumber}
          </DetailInfoWrapper>
          <DetailInfoWrapper title="Office Address">
            {joinAddress(item?.address)}
          </DetailInfoWrapper>
        </ContainerBodyContent>

        <ContainerBodyContent col={9} className="detail_content">
          <Tabs
            width={175}
            textAlign="center"
            className="detail-tab-panel"
            activeTab={tab}
            options={["Overview", "Sharing Partners", "UMKM Members"]}
            slug={["overview", "sharing_partners", "members"]}
            setActiveTab={(payload) =>
              dispatch({ type: DISTRIBUTOR.SET_TAB, payload })
            }
          />

          {tab === "overview" ? (
            <PanelOverviewDistributor data={item}>
              <FormRepresentativeDistributor
                isLoading={loading}
                isOpen={isFormOpen}
                toggle={() => dispatch({ type: REPRESENTATIVE_FORM.RESET })}
                onConfirm={() =>
                  dispatch(
                    saveRepresentative({
                      distributorId: item?._id,
                      id: isEditing?._id,
                      form,
                    })
                  )
                }
              />

              <Confirmation
                isOpen={isDeleting !== undefined}
                title={`Hapus ${isDeleting?.firstName} ${isDeleting?.lastName}?`}
                size="md"
                isLoading={loading}
                confirmButtonTheme="danger"
                toggle={() => dispatch({ type: REPRESENTATIVE_FORM.TOGGLE_DELETE })}
                onCancel={() =>
                  dispatch({ type: REPRESENTATIVE_FORM.TOGGLE_DELETE })
                }
                onConfirm={() =>
                  dispatch(
                    deleteRepresentative({
                      id: isDeleting?._id,
                      distributorId: isDeleting?.distributor,
                    })
                  )
                }
              />
            </PanelOverviewDistributor>
          ) : tab === "sharing_partners" ? (
            <PanelSharingPartnersDistributor data={item} isMenuOpen={isMenuOpen}>
              <FormSharingPartnerDistributor
                isOpen={isEditingRevenueShare}
                isLoading={loading}
                toggle={() =>
                  dispatch({ type: DISTRIBUTOR_FORM.TOGGLE_EDIT_REVENUE_SHARE })
                }
                companyName={item?.companyPartner.name}
                onConfirm={(value) =>
                  dispatch(
                    editRevenueSharePercentage({
                      id: item?._id || "",
                      body: { revenueSharePercentage: value },
                    })
                  )
                }
              />
            </PanelSharingPartnersDistributor>
          ) : (
            <PanelUmkmMembersDistributor data={umkmUsers} />
          )}
        </ContainerBodyContent>
      </ContainerBody>
    </DetailContainer>
  );
}

const DetailInfoWrapper: FC<{ title: string }> = ({ title, children }) => {
  return (
    <div className="detail-info-wrapper">
      <div className="title">{title}</div>
      <div className="value">{children}</div>
    </div>
  );
};
