import { useDispatch, useSelector } from "react-redux";
import { Input } from "../../../components";
import { RootState } from "../../../redux/store";
import { DISTRIBUTOR_FORM } from "../_state/interfaces/distributorForm";

export default function PanelRevenueSharingDistributor() {
  const dispatch = useDispatch();

  const {
    selectedCompanyPartner,
    companyPartner,
    companyPartners,
    revenueSharePercentage,
  } = useSelector((state: RootState) => state.distributorForm);

  console.log(companyPartner);
  return (
    <div className="form-panel partners">
      <div className="panel-title">
        <span>Revenue Sharing Partners</span>
      </div>

      <div className="panel-content">
        <div className="row">
          <Input
            containerClassName="col-6"
            label="Nama Perusahaan"
            type="select"
            value={selectedCompanyPartner}
            onChange={({ target }) => {
              dispatch({
                type: DISTRIBUTOR_FORM.SET_VALUE,
                name: "selectedCompanyPartner",
                payload: target.value,
              });
              dispatch({
                type: DISTRIBUTOR_FORM.SET_VALUE,
                name: "companyPartner",
                payload: companyPartners.find((e) => e._id === target.value),
              });
            }}
          >
            <option value="">Pilih Perusahaan</option>
            {companyPartners.map((e, i) => (
              <option key={i} value={e._id}>
                {e.abbreviation} - {e.name}
              </option>
            ))}
          </Input>

          <Input
            containerClassName="col-4"
            label="Revenue Share Percentage"
            value={revenueSharePercentage}
            onChange={({ target }) => {
              var value;
              if (!target.value) value = 0;
              value = parseInt(target.value);

              dispatch({
                type: DISTRIBUTOR_FORM.SET_VALUE,
                name: "revenueSharePercentage",
                payload: value,
              });
            }}
            append="%"
          />
        </div>
      </div>
    </div>
  );
}
