import { useDispatch, useSelector } from "react-redux";
import { Input, SingleImageUploader } from "../../../components";
import { RootState } from "../../../redux/store";
import { DISTRIBUTOR_FORM } from "../_state/interfaces/distributorForm";

export default function PanelDetailDistributor() {
  const dispatch = useDispatch();

  const { email, name, phoneNumber, profilePicture } = useSelector(
    (state: RootState) => state.distributorForm
  );

  return (
    <div className="form-panel detail">
      <div className="panel-title">
        <span>Detail Distributor</span>
      </div>

      <div className="panel-content">
        <SingleImageUploader
          className="mb-4"
          file={profilePicture}
          onChange={(payload) =>
            dispatch({
              type: DISTRIBUTOR_FORM.SET_VALUE,
              name: "profilePicture",
              payload,
            })
          }
          handleDelete={() =>
            dispatch({
              type: DISTRIBUTOR_FORM.SET_VALUE,
              name: "profilePicture",
              payload: undefined,
            })
          }
        />

        <Input
          label="Nama Distributor"
          name="name"
          placeholder="Masukkan Nama Distributor"
          value={name}
          onChange={({ target }) =>
            dispatch({
              type: DISTRIBUTOR_FORM.SET_VALUE,
              name: target.name,
              payload: target.value,
            })
          }
        />

        <Input
          label="Email"
          name="email"
          placeholder="Masukkan Email"
          value={email}
          onChange={({ target }) =>
            dispatch({
              type: DISTRIBUTOR_FORM.SET_VALUE,
              name: target.name,
              payload: target.value,
            })
          }
        />

        <Input
          label="Nomor Telepon"
          name="phoneNumber"
          placeholder="Masukkan Nomor Telepon"
          value={phoneNumber}
          onChange={({ target }) =>
            dispatch({
              type: DISTRIBUTOR_FORM.SET_VALUE,
              name: target.name,
              payload: target.value,
            })
          }
        />
      </div>
    </div>
  );
}
