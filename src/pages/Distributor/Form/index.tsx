/* eslint-disable @typescript-eslint/no-unused-vars */
import "./form.scss";

import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  ContainerBody,
  ContainerBodyContent,
  ContainerHead,
  DetailContainer,
} from "../../../components";
import { RootState } from "../../../redux/store";
import { useGenerateAddresses } from "../../../utils/hooks";
import PanelDetailDistributor from "./PanelDetail";
import PanelAddressDistributor from "./PanelAddress";
import { useEffect } from "react";
import {
  createDistributor,
  getDefaultData,
} from "../_state/actions/distributorForm";
import PanelBankAccountDistributor from "./PanelBankAccount";
import PanelRepresentativeDistributor from "./PanelRepresentatives";
import { DISTRIBUTOR_FORM } from "../_state/interfaces/distributorForm";
import PanelRevenueSharingDistributor from "./PanelRevenueSharing";
import { useHistory } from "react-router-dom";
import { DISTRIBUTOR } from "../_state/interfaces/distributor";
import PopupCreated from "./_PopupCreated";

export default function DistributorForm() {
  const dispatch = useDispatch();
  const history = useHistory();

  const formState = useSelector((state: RootState) => state.distributorForm);
  const { loading } = useSelector((state: RootState) => state.distributor);
  const isOpen = useSelector(
    (state: RootState) => state.distributorForm.isPopupCreatedOpen
  );

  const addressGenerator = useGenerateAddresses();
  const {
    address,
    changeAddress,
    provinces,
    provinceId,
    province,
    changeProvince,
    cities,
    cityId,
    city,
    changeCity,
    districts,
    districtId,
    district,
    changeDistrict,
    subdistricts,
    subdistrictId,
    subdistrict,
    changeSubdistrict,
    postalCode,
    changePostalCode,
  } = addressGenerator;

  useEffect(() => {
    dispatch(getDefaultData());
  }, [dispatch]);

  return (
    <DetailContainer title="Tambah Distributor">
      <PopupCreated
        isOpen={isOpen}
        onClose={() => {
          dispatch({ type: DISTRIBUTOR_FORM.RESET });
          history.goBack();
        }}
      />

      <ContainerHead backButton />

      <ContainerBody>
        <ContainerBodyContent className="distributor-form">
          <h3>Tambah Distributor Baru</h3>

          <PanelDetailDistributor />

          <PanelAddressDistributor hooks={addressGenerator} />

          <PanelBankAccountDistributor />

          <PanelRepresentativeDistributor />

          <PanelRevenueSharingDistributor />

          <div className="button-action">
            <Button
              disabled={loading}
              minWidth={165}
              theme="primary-outline"
              onClick={history.goBack}
            >
              Batalkan
            </Button>
            &nbsp; &nbsp;
            <Button
              isLoading={loading}
              disabled={loading}
              minWidth={165}
              theme="primary"
              onClick={async () => {
                dispatch(
                  createDistributor({
                    formState,
                    bodyAddress: {
                      address,
                      province,
                      city,
                      district,
                      subdistrict,
                      postalCode,
                    },
                  })
                );
              }}
            >
              Simpan
            </Button>
          </div>
        </ContainerBodyContent>
      </ContainerBody>
    </DetailContainer>
  );
}
