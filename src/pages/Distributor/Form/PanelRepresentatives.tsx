import { UilPlus } from "@iconscout/react-unicons";
import { useDispatch, useSelector } from "react-redux";
import { Button, ButtonIcon, Input } from "../../../components";
import { RootState } from "../../../redux/store";
import { DISTRIBUTOR_FORM } from "../_state/interfaces/distributorForm";

export default function PanelRepresentativesDistributor() {
  const dispatch = useDispatch();

  const { representatives } = useSelector(
    (state: RootState) => state.distributorForm
  );

  return (
    <div className="form-panel representatives">
      <div className="panel-title">
        <span>Detail Representatif</span>
      </div>

      <div className="panel-content">
        {representatives.map((item, i) => {
          return (
            <div className="mb-5" key={i}>
              <div className="row">
                <CustomInput
                  className="col-6"
                  id={item.id}
                  label="Nama Depan"
                  name="firstName"
                  value={item.firstName}
                />

                <CustomInput
                  className="col-6"
                  id={item.id}
                  label="Nama Belakang"
                  name="lastName"
                  value={item.lastName}
                />
              </div>

              <div className="row">
                <CustomInput
                  className="col-6"
                  id={item.id}
                  label="Nomor Telepon"
                  name="phoneNumber"
                  value={item.phoneNumber}
                />

                <CustomInput
                  className="col-6"
                  id={item.id}
                  label="Email"
                  name="email"
                  value={item.email}
                />
              </div>

              <CustomInput
                id={item.id}
                label="Posisi Pekerjaan"
                name="jobPosition"
                value={item.jobPosition}
              />

              <Button
                theme="danger"
                onClick={() =>
                  dispatch({
                    type: DISTRIBUTOR_FORM.REMOVE_REPRESENTATIVE,
                    id: item.id,
                  })
                }
              >
                Hapus Representatif - {item.firstName} {item.lastName}
              </Button>
            </div>
          );
        })}

        <div>
          <Button
            theme="primary-radius"
            onClick={() => dispatch({ type: DISTRIBUTOR_FORM.ADD_REPRESENTATIVE })}
          >
            <ButtonIcon type="prepend">
              <UilPlus size={16} />
            </ButtonIcon>
            Tambah Representatif
          </Button>
        </div>
      </div>
    </div>
  );
}

const CustomInput = ({
  className = "",
  id,
  label,
  name,
  value,
}: {
  className?: string;
  id: string;
  label: string;
  name: string;
  value: string;
}) => {
  const dispatch = useDispatch();

  return (
    <Input
      label={label}
      placeholder={`Masukkan ${label}`}
      containerClassName={className}
      name={name}
      value={value}
      onChange={({ target }) =>
        dispatch({
          type: DISTRIBUTOR_FORM.CHANGE_REPRESENTATIVE_VALUE,
          id: id,
          name: target.name,
          payload: target.value,
        })
      }
    />
  );
};
