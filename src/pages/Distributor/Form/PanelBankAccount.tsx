import { useDispatch, useSelector } from "react-redux";
import { Input } from "../../../components/Form";
import { RootState } from "../../../redux/store";
import { DISTRIBUTOR_FORM } from "../_state/interfaces/distributorForm";

export default function PanelBankAccountDistributor() {
  const dispatch = useDispatch();

  const { bankAccountNumber, banks } = useSelector(
    (state: RootState) => state.distributorForm
  );

  return (
    <div className="form-panel bank">
      <div className="panel-title">
        <span>Informasi Akun Distributor</span>
      </div>

      <div className="panel-content">
        <div className="row">
          <Input
            label="Pilih Bank"
            type="select"
            name="bank"
            onChange={({ target }) =>
              dispatch({
                type: DISTRIBUTOR_FORM.SET_VALUE,
                name: target.name,
                payload: target.value,
              })
            }
            containerClassName="col-6"
          >
            <option value="">Pilih Bank</option>
            {!banks.length
              ? null
              : banks.map((item, i) => (
                  <option key={i} value={item._id}>
                    {item.name}
                  </option>
                ))}
          </Input>

          <Input
            label="Nomor Rekening Bank"
            containerClassName="col-6"
            value={bankAccountNumber}
            name="bankAccountNumber"
            onChange={({ target }) =>
              dispatch({
                type: DISTRIBUTOR_FORM.SET_VALUE,
                name: target.name,
                payload: target.value,
              })
            }
          />
        </div>
      </div>
    </div>
  );
}
