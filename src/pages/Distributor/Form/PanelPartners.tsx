/* eslint-disable @typescript-eslint/no-unused-vars */
import { UilPlus, UilTrashAlt } from "@iconscout/react-unicons";
import { useDispatch, useSelector } from "react-redux";
import { Button, ButtonIcon, Input } from "../../../components";
import { RootState } from "../../../redux/store";
import { DISTRIBUTOR_FORM } from "../_state/interfaces/distributorForm";

export default function PanelPartnersDistributor() {
  const dispatch = useDispatch();

  // const { partners, sharingPartners } = useSelector(
  //   (state: RootState) => state.distributorForm
  // );

  return (
    <div className="form-panel partners">
      <div className="panel-title">
        <span>Revenue Sharing Partners</span>
      </div>

      <div className="panel-content">
        {/* {partners.map((item, i) => {
          return (
            <div className="row" key={i}>
              <Input
                containerClassName="col-6"
                label="Nama Perusahaan"
                type="select"
                value={item.sharingPartner}
                onChange={({ target }) =>
                  dispatch({
                    type: DISTRIBUTOR_FORM.CHANGE_PARTNER,
                    id: item.id,
                    payload: target.value,
                  })
                }
              >
                <option value="">Pilih Perusahaan</option>
                {sharingPartners.map((e, i) => (
                  <option key={i} value={e._id}>
                    {e.initialCode} - {e.name}
                  </option>
                ))}
              </Input>

              <Input
                containerClassName="col-4"
                label="Revenue Sharing Percentage"
                value={item.revenueSharePercentage}
                onChange={({ target }) =>
                  dispatch({
                    type: DISTRIBUTOR_FORM.CHANGE_REVENUE_SHARE_VALUE,
                    id: item.id,
                    payload: target.value,
                  })
                }
                append="%"
              />

              <div className="col-2 d-flex">
                <Button
                  isRounded={false}
                  theme="danger"
                  onClick={() =>
                    dispatch({ type: DISTRIBUTOR_FORM.REMOVE_PARTNER, id: item.id })
                  }
                  className="m-auto"
                >
                  <ButtonIcon>
                    <UilTrashAlt />
                  </ButtonIcon>
                </Button>
              </div>
            </div>
          );
        })} */}

        {/* <div>
          <Button
            theme="primary-radius"
            onClick={() => dispatch({ type: DISTRIBUTOR_FORM.ADD_PARTNER })}
          >
            <ButtonIcon type="prepend">
              <UilPlus size={16} />
            </ButtonIcon>
            Tambah Perusahaan
          </Button>
        </div> */}
      </div>
    </div>
  );
}
