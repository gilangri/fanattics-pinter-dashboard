import "./form.scss";

import { Modal, ModalBody, ModalHeader } from "../../../components";
import Logo from "../../../assets/images/img-files-sync.png";

interface P {
  isOpen: boolean;
  onClose?: () => void;
}
export default function PopupCreated({ isOpen, onClose }: P) {
  return (
    <Modal isOpen={isOpen}>
      <ModalHeader title=" " toggle={onClose} />
      <ModalBody className="popup-distributor-created">
        <div className="logo">
          <img src={Logo} alt="done" />
        </div>

        <div className="title">Akun Distributor Berhasil Didaftarkan!</div>

        <div className="subtitle">
          Proses pengaktifan akun distributor akan berjalan dalam waktu 3 x 24 jam,
          setelah itu akun distributor sudah aktif.
        </div>
      </ModalBody>
    </Modal>
  );
}
