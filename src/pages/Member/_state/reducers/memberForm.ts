import {
  IMemberFormAction,
  IMemberFormState,
  MEMBER_FORM,
} from "../interfaces/memberForm";

const init_state: IMemberFormState = {
  fullName: "",
  email: "",
  password: "",
  phoneNumber: "",
  profilePicture: undefined,
  ktpNumber: "",
  ktpUrl: undefined,
  npwpNumber: "",
  gender: "",
  lastEducation: "",
  biologicalMotherName: "",
  maritalStatus: "",

  address: "",
  province: "",
  city: "",
  district: "",
  subdistrict: "",
  postalCode: "",

  storeName: "",
  storeAddress: {
    address: "",
    province: "",
    city: "",
    district: "",
    subdistrict: "",
    postalCode: "",
  },

  bank: "",
  companyPartner: "",
  distributor: "",

  selectedBank: undefined,
  selectedCompanyPartner: undefined,
  selectedDistributor: undefined,
};

export function userFormReducer(
  state = init_state,
  { payload, name, type }: IMemberFormAction
): IMemberFormState {
  switch (type) {
    case MEMBER_FORM.SET:
      return { ...state, ...payload };
    case MEMBER_FORM.SET_VALUE:
      return { ...state, [name]: payload };
    case MEMBER_FORM.SET_VALUE_STORE_ADDRESS:
      return {
        ...state,
        storeAddress: {
          ...state.storeAddress,
          [name]: payload,
        },
      };

    case MEMBER_FORM.RESET:
      return init_state;
    default:
      return state;
  }
}
