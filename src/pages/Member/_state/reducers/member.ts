import { IMemberAction, IMemberState, MEMBER } from "../interfaces/member";

const init_state: IMemberState = {
  data: [],
  filtered: [],

  item: undefined,

  tab: "affiliation",
  selectedKtp: "",

  isDeleting: false,

  searchQuery: "",
  lastUpdate: new Date(),
  loading: false,
  error: "",
};

export function userReducer(
  state = init_state,
  { payload, name, type }: IMemberAction
): IMemberState {
  switch (type) {
    case MEMBER.REQUEST:
      return { ...state, loading: true, error: "" };
    case MEMBER.FAILED:
      return { ...state, loading: false, error: "" };

    case MEMBER.SET_USERS:
      return { ...state, loading: false, data: payload };
    case MEMBER.SET_USER:
      return { ...state, loading: false, item: payload };

    case MEMBER.SET:
      return { ...state, ...payload };
    case MEMBER.SET_VALUE:
      return { ...state, [name]: payload };
    case MEMBER.SET_TAB:
      return { ...state, tab: payload };

    case MEMBER.ON_REFRESH:
      return { ...state, lastUpdate: new Date() };

    case MEMBER.TOGGLE_KTP:
      return { ...state, selectedKtp: payload };
    case MEMBER.TOGGLE_DELETING:
      return { ...state, isDeleting: !state.isDeleting };

    case MEMBER.RESET:
      return init_state;
    default:
      return state;
  }
}
