import { AnyAction } from "redux";
import { IBank, ICompanyPartner, IDistributor } from "../../../../redux/_interface";

export interface IMemberFormState {
  fullName: string;
  email: string;
  password: string;
  phoneNumber: string;
  profilePicture?: File;
  ktpNumber: string;
  ktpUrl?: File;
  npwpNumber: string;
  gender: string;
  lastEducation: string;
  biologicalMotherName: string;
  maritalStatus: string;

  address: string;
  province: string;
  city: string;
  district: string;
  subdistrict: string;
  postalCode: string;

  storeName: string;
  storeAddress: {
    address: string;
    province: string;
    city: string;
    district: string;
    subdistrict: string;
    postalCode: string;
  };

  bank: string;
  companyPartner: string;
  distributor: string;

  selectedBank?: IBank;
  selectedCompanyPartner?: ICompanyPartner;
  selectedDistributor?: IDistributor;
}

export enum MEMBER_FORM {
  RESET = "USER_FORM_RESET",
  SET = "USER_FORM_SET",
  SET_VALUE = "USER_FORM_SET_VALUE",
  SET_VALUE_STORE_ADDRESS = "USER_FORM_SET_VALUE_STORE_ADDRESS",
}

export interface IMemberFormAction extends AnyAction {
  payload: any;
  name: string;
  type: MEMBER_FORM;
}
