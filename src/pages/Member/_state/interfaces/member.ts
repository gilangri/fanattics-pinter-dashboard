import { AnyAction } from "redux";
import { IMember } from "../../../../redux/_interface";

export interface IMemberState {
  data: IMember[];
  filtered: IMember[];

  item?: IMember;

  tab: "affiliation" | "store";
  selectedKtp: string;

  isDeleting: boolean;

  searchQuery: string;
  lastUpdate: Date;
  loading: boolean;
  error: string;
}

export enum MEMBER {
  REQUEST = "MEMBER_REQUEST",
  FAILED = "MEMBER_FAILED",
  RESET = "MEMBER_RESET",
  SET_USERS = "MEMBER_SET_USERS",
  SET_USER = "MEMBER_SET_USER",
  SET = "MEMBER_SET",
  SET_VALUE = "MEMBER_SET_VALUE",
  SET_TAB = "MEMBER_SET_TAB",
  ON_REFRESH = "MEMBER_ON_REFRESH",

  TOGGLE_KTP = "MEMBER_TOGGLE_KTP",
  TOGGLE_DELETING = "MEMBER_TOGGLE_DELETING",
}

export interface IMemberAction extends AnyAction {
  payload: any;
  name: string;
  type: MEMBER;
}
