import axios from "axios";
import { API_URL, optionsFormData } from "../../../../constants";
import { IMember } from "../../../../redux/_interface";
import { sortArrByKey } from "../../../../utils";
import { MEMBER } from "../interfaces/member";

export const getMembers =
  (id: string = "") =>
  async (dispatch: any) => {
    try {
      dispatch({ type: MEMBER.REQUEST });

      if (id) {
        const resMember = await axios.get(`${API_URL}/users/${id}`);
        const payload = resMember.data.result;
        dispatch({ type: MEMBER.SET_USER, payload });
      } else {
        const { data } = await axios.get(`${API_URL}/users`);

        const filtered = !data.result.length ? [] : data.result;
        const payload = !filtered.length ? [] : sortArrByKey(filtered, "fullName");
        dispatch({ type: MEMBER.SET_USERS, payload });
      }
    } catch (err) {
      console.error(err);
      dispatch({
        type: MEMBER.FAILED,
        payload: err.response?.data?.message || "failed",
      });
    }
  };

/**
 * Filter data by search query based on it's name
 * @param data fetched data
 * @param searchQuery inputted search query string
 * @returns filtered data based on searchQuery
 */
export const filterMember =
  (data: IMember[] = [], searchQuery: string = "") =>
  async (dispatch: any) => {
    const payload = !data.length
      ? []
      : data && !searchQuery
      ? data
      : data.filter((item) => {
          const query = searchQuery.toLowerCase();
          const name = item.fullName.toLowerCase();
          return name.includes(query);
        });

    dispatch({ type: MEMBER.SET_VALUE, name: "filtered", payload });
  };

interface IPost {
  body: FormData;
  storeAddress: Object;
}
export const postMember =
  ({ body, storeAddress }: IPost) =>
  async (dispatch: any) => {
    try {
      dispatch({ type: MEMBER.REQUEST });

      const resAddress = await axios.post(`${API_URL}/addresses`, storeAddress);
      body.append("storeAddress", resAddress.data.result._id);

      await axios.post(`${API_URL}/users`, body, optionsFormData);
      await Promise.all([dispatch(getMembers())]);
    } catch (err) {
      console.error(err);
      dispatch({ type: MEMBER.FAILED });
    }
  };

interface IDelete {
  history: any;
  id?: string;
}
export const deleteMember =
  ({ history, id }: IDelete) =>
  async (dispatch: any) => {
    if (!id) return alert("ID tidak valid");
    try {
      dispatch({ type: MEMBER.REQUEST });
      await axios.delete(`${API_URL}/users/${id}`);
      await Promise.all([dispatch(getMembers())]);
      history.goBack();
      dispatch({ type: MEMBER.RESET });
    } catch (err) {
      console.error(err);
      dispatch({ type: MEMBER.FAILED, payload: err });
    }
  };
