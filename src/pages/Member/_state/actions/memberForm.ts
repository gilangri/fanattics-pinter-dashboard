import { IMemberFormState } from "../interfaces/memberForm";
import { postMember } from "./member";

interface IAddressBody {
  address: string;
  province: string;
  city: string;
  district: string;
  subdistrict: string;
  postalCode: number;
}
interface ICreateMember {
  formState: IMemberFormState;
  memberAddress: IAddressBody;
  storeAddress: IAddressBody;
}

export const createMember =
  ({ formState, memberAddress, storeAddress }: ICreateMember) =>
  (dispatch: any) => {
    console.log("formState", formState);
    console.log("memberAddress", memberAddress);
    console.log("store", storeAddress);

    const validations = validate({
      formState,
      memberAddress,
      storeAddress,
    });
    if (validations.length) {
      alert(validations.join("\n"));
      throw new Error(`validation error\n${validations.join("\n")}`);
    }

    const {
      fullName,
      email,
      phoneNumber,
      npwpNumber,
      ktpNumber,
      ktpUrl,
      storeName,
      selectedCompanyPartner,
      selectedDistributor,
      selectedBank,
    } = formState;
    const { address, province, city, district, subdistrict, postalCode } =
      memberAddress;
    const companyPartner = selectedCompanyPartner?._id;
    const distributor = selectedDistributor?._id;
    const bank = selectedBank?._id;

    const formData = new FormData();
    formData.append("fullName", fullName);
    formData.append("email", email);
    formData.append("phoneNumber", phoneNumber);
    formData.append("address", address);
    formData.append("province", province);
    formData.append("city", city);
    formData.append("district", district);
    formData.append("subdistrict", subdistrict);
    formData.append("postalCode", postalCode.toString());
    formData.append("npwpNumber", npwpNumber);
    formData.append("ktpNumber", ktpNumber);

    if (ktpUrl) formData.append("ktp", ktpUrl);
    if (companyPartner) formData.append("companyPartner", companyPartner);
    if (distributor) formData.append("distributor", distributor);
    if (bank) formData.append("bank", bank);
    if (storeName) formData.append("storeName", storeName);

    dispatch(postMember({ body: formData, storeAddress }));
  };

const validate = ({ formState, memberAddress, storeAddress }: ICreateMember) => {
  let msg = [];

  if (!formState.fullName) msg.push("- Nama wajib diisi");
  if (!formState.email) msg.push("- Email wajib diisi");
  if (!formState.phoneNumber) msg.push("- Nomor Telepon wajib diisi");
  if (!validateAddress(memberAddress)) msg.push("- Alamat Member belum lengkap");
  if (!formState.npwpNumber) msg.push("- Nomor NPWP wajib diisi");
  if (!formState.ktpNumber) msg.push("- Nomor KTP wajib diisi");
  if (!formState.ktpUrl) msg.push("- Foto KTP wajib diisi");

  if (!formState.selectedCompanyPartner) msg.push("- Company Partner tidak valid");
  if (!formState.selectedDistributor?._id) msg.push("- Distributor tidak valid");
  if (!formState.selectedBank?._id) msg.push("- Bank tidak valid");

  if (!formState.storeName) msg.push("- Nama Toko wajib diisi");
  if (!validateAddress(storeAddress)) msg.push("- Alamat Toko belum lengkap");

  return msg;
};

const validateAddress = (body: IAddressBody) => {
  const { address, province, city, district, subdistrict, postalCode } = body;
  if (!address || !province || !city || !district || !subdistrict || !postalCode) {
    return false;
  }
  return true;
};
