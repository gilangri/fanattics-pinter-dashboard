/* eslint-disable @typescript-eslint/no-unused-vars */
import "./user.scss";

import { UilUserPlus, UilUserSquare } from "@iconscout/react-unicons";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import {
  Button,
  ButtonIcon,
  ContainerBody,
  ContainerBodyContent,
  ContainerHead,
  Pagination,
  ParentContainer,
  Searchbar,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableHeadCell,
  TableRow,
  usePagination,
} from "../../components";
import { IMember, RootState } from "../../redux/store";
import { MEMBER } from "./_state/interfaces/member";
import { useEffect } from "react";
import { filterMember, getMembers } from "./_state/actions/member";
import { MEMBER_FORM } from "./_state/interfaces/memberForm";

export default function MemberPage() {
  const dispatch = useDispatch();
  const history = useHistory();

  const { data, filtered, lastUpdate, loading, searchQuery } = useSelector(
    (state: RootState) => state.member
  );

  const { next, prev, jump, currentData, pages, currentPage, maxPage } =
    usePagination({
      data: filtered,
      itemsPerPage: 10,
    });

  useEffect(() => {
    dispatch({ type: MEMBER.SET_USERS, payload: [] });
    dispatch(getMembers());
  }, [dispatch, lastUpdate]);

  useEffect(() => {
    dispatch(filterMember(data, searchQuery));
  }, [data, dispatch, searchQuery]);

  return (
    <ParentContainer title="Daftar Member UMKM">
      <ContainerHead
        title="Daftar Member UMKM"
        onRefresh={() => dispatch({ type: MEMBER.ON_REFRESH })}
        refreshing={loading}
      >
        <Button
          theme="primary"
          onClick={() => {
            dispatch({ type: MEMBER_FORM.RESET });
            history.push(`${history.location.pathname}/form`);
          }}
        >
          <ButtonIcon type="prepend">
            <UilUserPlus />
          </ButtonIcon>
          &nbsp; Tambah Member
        </Button>
      </ContainerHead>

      <ContainerBody className="user-page-body">
        <ContainerBodyContent className="user-page-body-content">
          <div className="row mb-4">
            <div className="col-3">
              <Searchbar
                autoFocus
                placeholder="Cari Member"
                value={searchQuery}
                onChange={(payload) =>
                  dispatch({
                    type: MEMBER.SET_VALUE,
                    name: "searchQuery",
                    payload,
                  })
                }
              />
            </div>
          </div>
          <Table className="table-content">
            <TableHead>
              <TableRow>
                <TableHeadCell width="25%">Nama Member</TableHeadCell>
                <TableHeadCell width="15%">Member ID</TableHeadCell>
                <TableHeadCell width="10%">Tipe Kredit</TableHeadCell>
                <TableHeadCell width="25%">Distributor Partner</TableHeadCell>
                <TableHeadCell width="25%">Akun Bank</TableHeadCell>
              </TableRow>
            </TableHead>

            <RenderBody
              data={currentData()}
              onClick={(payload) => {
                dispatch({ type: MEMBER.SET_USER, payload });
                history.push(`${history.location.pathname}/detail`);
              }}
            />

            <TableFooter>
              <TableRow>
                <TableCell colSpan={10} align="center">
                  <Pagination
                    next={next}
                    prev={prev}
                    jump={jump}
                    pages={pages}
                    currentPage={currentPage}
                    maxPage={maxPage}
                  />
                </TableCell>
              </TableRow>
            </TableFooter>
          </Table>
        </ContainerBodyContent>
      </ContainerBody>
    </ParentContainer>
  );
}

const RenderBody = ({
  data,
  isLoading,
  onClick,
}: {
  data: IMember[];
  isLoading?: boolean;
  onClick?: (item: IMember) => void;
}) => {
  return (
    <TableBody>
      {!data.length ? (
        <TableRow>
          <TableCell align="center" colSpan={10}>
            {isLoading ? "loading" : "kosong"}
          </TableCell>
        </TableRow>
      ) : (
        data.map((item, i) => {
          return (
            <TableRow key={i} onClick={() => onClick && onClick(item)}>
              <TableCell width="25%">
                <div className="nested-info">
                  <div className="profile-picture">
                    <UilUserSquare size={20} />
                  </div>
                  <div className="user-info">
                    <div className="fullname">{item.fullName || "fullname"}</div>
                    <div className="storename">{item.storeName || "storename"}</div>
                  </div>
                </div>
              </TableCell>
              <TableCell width="20%">{item._id}</TableCell>
              <TableCell width="10%">UMKM Binaan</TableCell>
              <TableCell width="25%">{item.distributor?.name || ""}</TableCell>
              <TableCell width="20%">{item.bank?.name || ""}</TableCell>
            </TableRow>
          );
        })
      )}
    </TableBody>
  );
};
