import "./_components.scss";

import { FC } from "react";
import { IMember } from "../../../../redux/_interface";

interface P {
  item?: IMember;
}
export default function PanelInformasiTokoUsahaMember({ item }: P) {
  return (
    <div className="panel-informasi-toko-usaha-member">
      <div className="store-content">
        <InfoWrapper title="Nama Toko">{item?.storeName}</InfoWrapper>
        <InfoWrapper title="Alamat">{item?.storeAddress?.address}</InfoWrapper>

        <div className="address-components">
          <InfoWrapper title="Kelurahan">
            {item?.storeAddress?.subdistrict}
          </InfoWrapper>
          <InfoWrapper title="Kecamatan">{item?.storeAddress?.district}</InfoWrapper>
          <InfoWrapper title="Kota">{item?.storeAddress?.city}</InfoWrapper>
          <InfoWrapper title="Provinsi">{item?.storeAddress?.province}</InfoWrapper>
          <InfoWrapper title="Kode Pos">
            {item?.storeAddress?.postalCode}
          </InfoWrapper>
        </div>
      </div>
    </div>
  );
}

const InfoWrapper: FC<{ title: string }> = ({ children, title }) => {
  return (
    <div className="info-wrapper">
      <div className="title">{title}</div>
      <div className="value">{children}</div>
    </div>
  );
};
