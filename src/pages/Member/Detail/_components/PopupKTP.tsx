import "./_components.scss";

import { Modal, ModalBody } from "../../../../components";

interface P {
  image?: string;
  toggle?: () => void;
}
export default function PopupKTPMember({ image, toggle }: P) {
  return (
    <Modal size="md" isOpen={image !== ""} toggle={toggle}>
      <ModalBody>
        <div className="popup-ktp-image">
          <img src={image} alt="ktp" />
        </div>
      </ModalBody>
    </Modal>
  );
}
