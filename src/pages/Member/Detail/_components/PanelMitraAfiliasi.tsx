import "./_components.scss"

import { FC } from "react";

interface P {
  bank?: string
  companyPartner?: string;
  distributor?: string
}
export default function PanelMitraAfiliasiMember({ bank, companyPartner, distributor }: P) {
  return (
    <div className="panel-mitra-afiliasi-member">
      <div className="affiliated-content">
        <InfoWrapper title="Affiliated Pinter Member">{companyPartner}</InfoWrapper>
        <InfoWrapper title="Affiliated Distributor">
          {distributor}
        </InfoWrapper>
        <InfoWrapper title="Affiliated Bank">{bank}</InfoWrapper>
      </div>
    </div>
  );
}

const InfoWrapper: FC<{ title: string }> = ({ children, title }) => {
  return (
    <div className="info-wrapper">
      <div className="title">{title}</div>
      <div className="value">{children}</div>
    </div>
  );
};
