import "./memberDetail.scss";

import { FC } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  ContainerBody,
  ContainerBodyContent,
  ContainerHead,
  DetailContainer,
  Tabs,
} from "../../../components";
import { RootState } from "../../../redux/store";
import { MEMBER } from "../_state/interfaces/member";
import PanelMitraAfiliasiMember from "./_components/PanelMitraAfiliasi";
import PanelInformasiTokoUsahaMember from "./_components/PanelInformasiTokoUsaha";
import PopupKTPMember from "./_components/PopupKTP";
import { ImageGenerator } from "../../../utils";
import Confirmation from "../../../components/Popup/Confirmation";
import { deleteMember } from "../_state/actions/member";
import { useHistory } from "react-router-dom";

export default function MemberDetailPage() {
  const dispatch = useDispatch();
  const history = useHistory();

  const { isDeleting, selectedKtp, item, tab, loading } = useSelector(
    (state: RootState) => state.member
  );

  return (
    <DetailContainer title="Detail Member">
      <PopupKTPMember
        image={selectedKtp}
        toggle={() => dispatch({ type: MEMBER.TOGGLE_KTP, payload: "" })}
      />
      <Confirmation
        size="md"
        title={`Hapus ${item?.fullName}?`}
        isOpen={isDeleting}
        isLoading={loading}
        confirmButtonTheme="danger"
        onCancel={() => dispatch({ type: MEMBER.TOGGLE_DELETING })}
        toggle={() => dispatch({ type: MEMBER.TOGGLE_DELETING })}
        onConfirm={() => dispatch(deleteMember({ history, id: item?._id }))}
      />

      <ContainerHead
        backButton
        title="Profil UMKM"
        backButtonHandler={() => dispatch({ type: MEMBER.RESET })}
      >
        <Button
          disabled={isDeleting}
          onClick={() => dispatch({ type: MEMBER.TOGGLE_DELETING })}
          theme="danger"
        >
          DELETE
        </Button>
      </ContainerHead>

      <ContainerBody className="member-detail">
        <ContainerBodyContent col={3} className="panel-content">
          <ImageGenerator
            alt="member"
            src={item?.profilePicture}
            value={item?.fullName}
            className="profile-picture"
          />

          <div className="see-ktp">
            <Button
              theme="primary-radius"
              minWidth={150}
              disabled={typeof item?.ktpUrl !== "string"}
              onClick={() => {
                dispatch({ type: MEMBER.TOGGLE_KTP, payload: item?.ktpUrl });
              }}
            >
              Lihat Foto KTP
            </Button>
          </div>

          <InfoWrapper title="Nama Member">{item?.fullName}</InfoWrapper>
          <InfoWrapper title="Nomor Telepon">{item?.phoneNumber}</InfoWrapper>
          <InfoWrapper title="Nomor NPWP">{item?.npwpNumber}</InfoWrapper>
          <InfoWrapper title="Nomor KTP">{item?.ktpNumber}</InfoWrapper>
          <InfoWrapper title="Alamat Pribadi">{item?.address}</InfoWrapper>
          <InfoWrapper title="Kelurahan">{item?.subdistrict}</InfoWrapper>
          <InfoWrapper title="Kecamatan">{item?.district}</InfoWrapper>
          <InfoWrapper title="Kota">{item?.city}</InfoWrapper>
          <InfoWrapper title="Provinsi">{item?.province}</InfoWrapper>
          <InfoWrapper title="Kode Pos">{item?.postalCode}</InfoWrapper>
        </ContainerBodyContent>

        <ContainerBodyContent col={9} className="panel-content">
          <Tabs
            width={175}
            textAlign="center"
            activeTab={tab}
            options={["Mitra Afiliasi", "Informasi Toko Usaha"]}
            slug={["affiliation", "store"]}
            setActiveTab={(payload) => dispatch({ type: MEMBER.SET_TAB, payload })}
          />

          {tab === "affiliation" ? (
            <PanelMitraAfiliasiMember
              bank={item?.bank.name}
              companyPartner={item?.companyPartner?.name}
              distributor={item?.distributor.name}
            />
          ) : (
            <PanelInformasiTokoUsahaMember item={item} />
          )}
        </ContainerBodyContent>
      </ContainerBody>
    </DetailContainer>
  );
}

const InfoWrapper: FC<{ title: string }> = ({ children, title }) => {
  return (
    <div className="info-wrapper">
      <div className="title">{title}</div>
      <div className="value">{children}</div>
    </div>
  );
};
