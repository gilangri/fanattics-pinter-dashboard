import { FC } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Input as InputComponent, SingleImageUploader } from "../../../components";
import { RootState } from "../../../redux/store";
import { IUseGenerateAddresses } from "../../../utils";
import { MEMBER_FORM } from "../_state/interfaces/memberForm";

interface P {
  hooks: IUseGenerateAddresses;
}
export default function PanelDetailMember({ hooks }: P) {
  const dispatch = useDispatch();

  const {
    address,
    changeAddress,
    provinces,
    provinceId,
    changeProvince,
    cities,
    cityId,
    changeCity,
    districts,
    districtId,
    changeDistrict,
    subdistricts,
    subdistrictId,
    changeSubdistrict,
    postalCode,
    changePostalCode,
  } = hooks;

  const { email, fullName, npwpNumber, ktpNumber, ktpUrl, phoneNumber } =
    useSelector((state: RootState) => state.memberForm);

  return (
    <div className="form-panel detail">
      <div className="panel-title">
        <span>Detail Member</span>
      </div>

      <div className="panel-content">
        <TextInput label="Nama Member" name="fullName" value={fullName} />
        <TextInput label="Email" name="email" value={email} />
        <TextInput label="Nomor Telepon" name="phoneNumber" value={phoneNumber} />

        <TextInput
          label="Alamat Pribadi"
          name="address"
          value={address}
          onChange={changeAddress}
        />

        <div className="row">
          <SelectInput label="Provinsi" value={provinceId} onChange={changeProvince}>
            <option value={0}>Pilih Provinsi</option>
            {provinces.map((e, i) => (
              <option key={i} value={e._id}>
                {e.province}
              </option>
            ))}
          </SelectInput>

          <SelectInput label="Kota" value={cityId} onChange={changeCity}>
            <option value={0}>Pilih Kota</option>
            {cities.map((e, i) => (
              <option key={i} value={e._id}>
                {e.name}
              </option>
            ))}
          </SelectInput>
        </div>

        <div className="row">
          <SelectInput
            label="Kecamatan"
            value={districtId}
            onChange={changeDistrict}
          >
            <option value={0}>Pilih Kecamatan</option>
            {districts.map((e, i) => (
              <option key={i} value={e._id}>
                {e.name}
              </option>
            ))}
          </SelectInput>

          <SelectInput
            label="Kelurahan"
            value={subdistrictId}
            onChange={changeSubdistrict}
          >
            <option value={0}>Pilih Kelurahan</option>
            {subdistricts.map((e, i) => (
              <option key={i} value={e._id}>
                {e.name}
              </option>
            ))}
          </SelectInput>
        </div>

        <TextInput
          label="Kode Pos"
          name="postalCode"
          value={postalCode}
          onChange={changePostalCode}
        />

        <TextInput label="Nomor NPWP" name="npwpNumber" value={npwpNumber} />
        <TextInput label="Nomor KTP" name="ktpNumber" value={ktpNumber} />

        <SingleImageUploader
          className="mb-4"
          label="Unggah Foto KTP Member"
          file={ktpUrl}
          onChange={(payload) =>
            dispatch({
              type: MEMBER_FORM.SET_VALUE,
              name: "ktpUrl",
              payload,
            })
          }
        />
      </div>
    </div>
  );
}

const TextInput: FC<{
  value: string | number;
  label: string;
  onChange?: (value: string) => void;
  name: string;
}> = ({ value, label, onChange, name }) => {
  const dispatch = useDispatch();

  return (
    <InputComponent
      label={label}
      name={name}
      placeholder={`Masukkan ${label}`}
      value={value}
      onChange={({ target }) =>
        onChange
          ? onChange(target.value)
          : dispatch({
              type: MEMBER_FORM.SET_VALUE,
              name: target.name,
              payload: target.value,
            })
      }
    />
  );
};

const SelectInput: FC<{
  label: string;
  onChange: (value: string) => void;
  value: number;
}> = ({ children, label, onChange, value }) => {
  return (
    <InputComponent
      label={label}
      type="select"
      value={value}
      onChange={({ target }) => onChange(target.value)}
      containerClassName="col-6"
    >
      {children}
    </InputComponent>
  );
};
