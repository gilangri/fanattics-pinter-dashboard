/* eslint-disable @typescript-eslint/no-unused-vars */
import { useDispatch, useSelector } from "react-redux";
import { Input } from "../../../components";
import { RootState } from "../../../redux/store";
import {
  useGetBankList,
  useGetCompanyPartners,
  useGetDistributors,
} from "../../../utils/hooks";
import { MEMBER_FORM } from "../_state/interfaces/memberForm";

export default function PanelMitraAfiliasiMember() {
  const dispatch = useDispatch();

  const { companyPartner, distributor, selectedBank } = useSelector(
    (state: RootState) => state.memberForm
  );

  const { banks } = useGetBankList();
  const { companyPartners } = useGetCompanyPartners();
  const { distributors } = useGetDistributors(companyPartner);

  return (
    <div className="form-panel detail">
      <div className="panel-title">
        <span>Mitra Afiliasi</span>
      </div>

      <div className="panel-content">
        <Input
          label="Affiliated Pinter Partner"
          type="select"
          value={companyPartner}
          onChange={({ target }) => {
            let id = target.value;
            dispatch({
              type: MEMBER_FORM.SET,
              payload: {
                companyPartner: id,
                selectedCompanyPartner: companyPartners.find((e) => e._id === id),
                selectedDistributor: undefined,
                selectedBank: undefined,
              },
            });
          }}
        >
          <option value="">Pilih Company Partner</option>
          {!companyPartners.length
            ? null
            : companyPartners.map((item, i) => {
                return (
                  <option key={i} value={item._id}>
                    {item.name}
                  </option>
                );
              })}
        </Input>

        <Input
          label="Affiliated Distributor"
          type="select"
          value={distributor}
          onChange={({ target }) => {
            let id = target.value;
            let selected = distributors.find((e) => e._id === id);
            console.log(selected);
            dispatch({
              type: MEMBER_FORM.SET,
              payload: {
                distributor: id,
                selectedDistributor: selected,
                selectedBank: banks.find((e) => e._id === selected?.bank._id),
              },
            });
          }}
        >
          <option value="">Pilih Distributor</option>
          {!distributors.length
            ? null
            : distributors.map((item, i) => {
                return (
                  <option key={i} value={item._id}>
                    {item.name}
                  </option>
                );
              })}
        </Input>

        <Input
          label="Affiliated Bank"
          value={selectedBank?.name || "Pilih Distributor terlebih dahulu"}
          disabled
        />
      </div>
    </div>
  );
}
