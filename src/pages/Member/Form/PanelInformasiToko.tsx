import { useDispatch, useSelector } from "react-redux";
import { Input } from "../../../components";
import { RootState } from "../../../redux/store";
import { IUseGenerateAddresses } from "../../../utils";
import { MEMBER_FORM } from "../_state/interfaces/memberForm";

interface P {
  hooks: IUseGenerateAddresses;
}
export default function PanelInformasiTokoMember({ hooks }: P) {
  const dispatch = useDispatch();

  const { storeName } = useSelector((state: RootState) => state.memberForm);

  const {
    address,
    changeAddress,
    provinces,
    provinceId,
    changeProvince,
    cities,
    cityId,
    changeCity,
    districts,
    districtId,
    changeDistrict,
    subdistricts,
    subdistrictId,
    changeSubdistrict,
    postalCode,
    changePostalCode,
  } = hooks;

  return (
    <div className="form-panel ">
      <div className="panel-title">
        <span>Alamat Distributor</span>
      </div>

      <div className="panel-content">
        <Input
          label="Nama Toko"
          placeholder="Masukkan Nama Toko"
          value={storeName}
          onChange={({ target }) =>
            dispatch({
              type: MEMBER_FORM.SET_VALUE,
              name: "storeName",
              payload: target.value,
            })
          }
        />

        <Input
          label="Alamat Toko"
          placeholder="Masukkan Alamat Toko"
          value={address}
          onChange={(e) => changeAddress(e.target.value)}
        />

        <div className="row">
          <Input
            label="Provinsi"
            type="select"
            value={provinceId}
            onChange={(e) => changeProvince(e.target.value)}
            containerClassName="col-6"
          >
            <option value={0}>Pilih Provinsi</option>
            {provinces.map((e, i) => (
              <option key={i} value={e._id}>
                {e.province}
              </option>
            ))}
          </Input>

          <Input
            label="Kota"
            type="select"
            value={cityId}
            onChange={(e) => changeCity(e.target.value)}
            containerClassName="col-6"
          >
            <option value={0}>Pilih Kota</option>
            {cities.map((e, i) => (
              <option key={i} value={e._id}>
                {e.name}
              </option>
            ))}
          </Input>
        </div>

        <div className="row">
          <Input
            label="Kecamatan"
            type="select"
            value={districtId}
            onChange={(e) => changeDistrict(e.target.value)}
            containerClassName="col-6"
          >
            <option value={0}>Pilih Kecamatan</option>
            {districts.map((e, i) => (
              <option key={i} value={e._id}>
                {e.name}
              </option>
            ))}
          </Input>

          <Input
            label="Kelurahan"
            type="select"
            value={subdistrictId}
            onChange={(e) => changeSubdistrict(e.target.value)}
            containerClassName="col-6"
          >
            <option value={0}>Pilih Kelurahan</option>
            {subdistricts.map((e, i) => (
              <option key={i} value={e._id}>
                {e.name}
              </option>
            ))}
          </Input>
        </div>

        <Input
          label="Kode Pos"
          placeholder="Masukkan Kode Pos"
          value={postalCode}
          onChange={(e) => changePostalCode(e.target.value)}
        />
      </div>
    </div>
  );
}
