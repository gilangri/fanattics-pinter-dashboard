/* eslint-disable @typescript-eslint/no-unused-vars */
import "./memberForm.scss";

import {
  Button,
  ContainerBody,
  ContainerBodyContent,
  ContainerHead,
  DetailContainer,
} from "../../../components";
import PanelDetailMember from "./PanelDetail";
import PanelMitraAfiliasiMember from "./PanelMitraAfiliasi";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../redux/store";
import { useGenerateAddresses } from "../../../utils";
import PanelInformasiTokoMember from "./PanelInformasiToko";
import { createMember } from "../_state/actions/memberForm";
import { MEMBER } from "../_state/interfaces/member";
import { MEMBER_FORM } from "../_state/interfaces/memberForm";

export default function MemberFormPage() {
  const history = useHistory();
  const dispatch = useDispatch();

  const memberAddressGenerator = useGenerateAddresses();
  const storeAddressGenerator = useGenerateAddresses();

  const formState = useSelector((state: RootState) => state.memberForm);
  const { loading } = useSelector((state: RootState) => state.member);

  return (
    <DetailContainer title="Tambah Member">
      <ContainerHead backButton />

      <ContainerBody>
        <ContainerBodyContent className="user-form">
          <h3>Tambah Member</h3>

          <PanelDetailMember hooks={memberAddressGenerator} />
          <PanelMitraAfiliasiMember />
          <PanelInformasiTokoMember hooks={storeAddressGenerator} />

          <div className="button-action">
            <Button
              disabled={loading}
              theme="primary-outline"
              onClick={history.goBack}
            >
              Batalkan
            </Button>
            &nbsp; &nbsp;
            <Button
              disabled={loading}
              isLoading={loading}
              theme="primary"
              onClick={async () => {
                try {
                  await Promise.all([
                    dispatch(
                      createMember({
                        formState,
                        storeAddress: {
                          address: storeAddressGenerator.address,
                          province: storeAddressGenerator.province,
                          city: storeAddressGenerator.city,
                          district: storeAddressGenerator.district,
                          subdistrict: storeAddressGenerator.subdistrict,
                          postalCode: storeAddressGenerator.postalCode,
                        },
                        memberAddress: {
                          address: memberAddressGenerator.address,
                          province: memberAddressGenerator.province,
                          city: memberAddressGenerator.city,
                          district: memberAddressGenerator.district,
                          subdistrict: memberAddressGenerator.subdistrict,
                          postalCode: memberAddressGenerator.postalCode,
                        },
                      })
                    ),
                  ]);
                  history.goBack();
                  dispatch({ type: MEMBER.RESET });
                  dispatch({ type: MEMBER_FORM.RESET });
                } catch (err) {
                  console.error(
                    err?.response?.data?.message || err?.message || "failed"
                  );
                }
              }}
            >
              Simpan
            </Button>
          </div>
        </ContainerBodyContent>
      </ContainerBody>
    </DetailContainer>
  );
}
