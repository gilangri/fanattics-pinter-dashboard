/* eslint-disable @typescript-eslint/no-unused-vars */
import "./admin.scss";

import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeadCell,
  TableRow,
  Searchbar,
  ParentContainer,
  ContainerHead,
  ContainerBody,
  TableFooter,
  usePagination,
  Pagination,
  Button,
  ContainerBodyContent,
} from "../../components";
import { RootState, IAdmin } from "../../redux/store";
import { deleteAdmin, filterAdmin, getAdmin } from "./_state/actions/admin";
import { ADMIN } from "./_state/interfaces/admin";
import { PopupManageAdmin } from "./PopupManageAdmin";
import Confirmation from "../../components/Popup/Confirmation";
import { handleSaveAdmin } from "./_state/actions/adminForm";
import { ADMIN_FORM } from "./_state/interfaces/adminForm";
import { enumAdmin } from "../../utils";

export default function AdminPage() {
  const dispatch = useDispatch();

  //* GLOBAL STATE
  const { admins, filtered, isDeleting, searchQuery, loading } = useSelector(
    (state: RootState) => state.admin
  );
  const { isEditing, isOpen } = useSelector((state: RootState) => state.adminForm);

  const [sortedBy, setSortedBy] = useState("name-desc");

  const { next, prev, jump, currentData, pages, currentPage, maxPage } =
    usePagination({
      data: filtered,
      itemsPerPage: 10,
    });

  //* FETCH DATA
  useEffect(() => {
    dispatch(filterAdmin(admins, searchQuery, sortedBy));
  }, [admins, dispatch, searchQuery, sortedBy]);

  useEffect(() => {
    dispatch(getAdmin());
  }, [dispatch]);

  return (
    <ParentContainer title="Daftar Admin">
      <PopupManageAdmin
        isEditing={isEditing}
        isOpen={isOpen}
        toggle={() => dispatch({ type: ADMIN_FORM.RESET })}
        isLoading={!isDeleting && isEditing && loading}
        handleSimpan={(item) => dispatch(handleSaveAdmin(item, isEditing))}
        handleDelete={
          !isEditing ? undefined : () => dispatch({ type: ADMIN.HANDLE_DELETE })
        }
      />
      <Confirmation
        isOpen={isDeleting}
        title={`Hapus ${isEditing?.name}?`}
        onCancel={() => dispatch({ type: ADMIN.CANCEL_DELETE })}
        isLoading={isDeleting && isEditing && loading}
        onConfirm={() => isEditing && dispatch(deleteAdmin(isEditing._id))}
        confirmButtonTheme="danger"
      />

      <ContainerHead title="Daftar Admin" />

      <ContainerBody>
        <ContainerBodyContent>
          <div className="admin-catalog row mb-4">
            <div className="col-9">
              <div className="body-title">Total User: {admins.length}</div>
            </div>

            <div className="col-3">
              <Searchbar
                autoFocus
                placeholder="Cari Admin"
                value={searchQuery}
                onChange={(payload) =>
                  dispatch({ type: ADMIN.SET_VALUE, name: "searchQuery", payload })
                }
              />
            </div>
          </div>

          <Table className="body-table">
            <TableHead>
              <TableRow>
                <TableHeadCell onClick={setSortedBy} name="name" sortedBy={sortedBy}>
                  Nama
                </TableHeadCell>
                <TableHeadCell
                  onClick={setSortedBy}
                  name="phone"
                  sortedBy={sortedBy}
                >
                  Nomor Telepon
                </TableHeadCell>
                <TableHeadCell>User ID</TableHeadCell>
                <TableHeadCell onClick={setSortedBy} name="role" sortedBy={sortedBy}>
                  Jabatan
                </TableHeadCell>
                <TableHeadCell
                  onClick={setSortedBy}
                  name="status"
                  sortedBy={sortedBy}
                >
                  Status
                </TableHeadCell>
              </TableRow>
            </TableHead>

            <RenderBody
              data={currentData()}
              onClick={(payload) => {
                console.log("EDIT", payload.distributor);
                dispatch({ type: ADMIN_FORM.EDITING, payload });
              }}
            />

            <TableFooter>
              <TableRow>
                <TableCell colSpan={10}>
                  <div className="row px-3">
                    <div className="col-2 flex-auto" />
                    <div className="col-8 d-flex justify-content-center">
                      <Pagination
                        next={next}
                        prev={prev}
                        jump={jump}
                        pages={pages}
                        currentPage={currentPage}
                        maxPage={maxPage}
                      />
                    </div>
                    <Button
                      className="col-2"
                      theme="primary"
                      onClick={() => dispatch({ type: ADMIN_FORM.TOGGLE_IS_OPEN })}
                    >
                      Tambah Admin
                    </Button>
                  </div>
                </TableCell>
              </TableRow>
            </TableFooter>
          </Table>
        </ContainerBodyContent>
      </ContainerBody>
    </ParentContainer>
  );
}

const RenderBody = ({
  data,
  onClick,
}: {
  data: IAdmin[];
  onClick: (item: IAdmin) => void;
}) => {
  return (
    <TableBody>
      {!data.length
        ? null
        : data.map((item, i) => {
            return (
              <TableRow key={i} onClick={() => onClick(item)}>
                <TableCell style={{ width: "20%" }}>{item.name}</TableCell>
                <TableCell style={{ width: "15%" }}>{item.phone}</TableCell>
                <TableCell style={{ width: "25%" }}>{item._id}</TableCell>
                <TableCell style={{ width: "20%" }}>
                  {enumAdmin(item.role)}
                </TableCell>
                <TableCell style={{ width: "15%" }}>{item.active}</TableCell>
              </TableRow>
            );
          })}
    </TableBody>
  );
};
