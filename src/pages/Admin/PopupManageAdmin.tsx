/* eslint-disable @typescript-eslint/no-unused-vars */
import { useEffect } from "react";
import { FC } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Button,
  Form,
  Input,
  Modal,
  ModalBody,
  ModalHeader,
  SubjectTitle,
} from "../../components";
import { RootState } from "../../redux/store";
import { IAdmin } from "../../redux/_interface/admin";
import { enumAdmin, useGetCompanyPartners } from "../../utils";
import { ADMIN_FORM } from "./_state/interfaces/adminForm";

interface Props {
  isOpen?: boolean;
  isLoading?: boolean;
  isEditing?: IAdmin;
  toggle?: () => void;
  handleSimpan: (item: Object) => void;
  handleDelete?: () => void;
}

export const PopupManageAdmin = ({
  isOpen,
  isLoading,
  isEditing,
  toggle,
  handleSimpan,
  handleDelete,
}: Props) => {
  const dispatch = useDispatch();

  const {
    name,
    phone,
    email,
    role,
    distributor,
    companyPartner,
    password,
    password2,
  } = useSelector((state: RootState) => state.adminForm);
  const currentCompanyPartner = useSelector(
    (state: RootState) => state.auth.user?.companyPartner
  );
  // const distributors = useSelector(
  //   (state: RootState) => state.myCompany.item?.distributors
  // );
  const distributors: any[] = [];

  const { companyPartners } = useGetCompanyPartners();

  //* FUNCTION
  const onChangeValue = (name: string, payload: string) => {
    dispatch({ type: ADMIN_FORM.SET_VALUE, name, payload });
  };

  function validation() {
    let errMsg = [];
    if (!name) errMsg.push("- Nama harus diisi.");
    if (!phone) errMsg.push("- Nomor HP harus diisi.");
    if (!email) errMsg.push("- Email harus diisi.");
    if (!role) errMsg.push("- Jabatan harus diisi.");
    if (!isEditing) {
      if (role && role !== "super-company" && !distributor)
        errMsg.push("- Pilih Distributor");
      if (!password) errMsg.push("- Password harus diisi.");
      if (password !== password2) errMsg.push("- Password tidak sesuai.");
    }
    return errMsg;
  }

  const onSimpan = async () => {
    if (!toggle || isLoading) return;
    const errMsg = validation();
    if (errMsg.length) return alert(errMsg.join("\n"));

    try {
      const body = {
        name,
        phone,
        email,
        role,
        distributor,
        companyPartner,
        password,
      };
      const bodyEdit = {
        name,
        phone,
        email,
      };

      handleSimpan(isEditing ? bodyEdit : body);
    } catch (err) {
      console.error("onSimpan PopupManageAdmin", err);
    }
  };

  // set companyPartner id from current logged in admin
  useEffect(() => {
    console.log("currentCompanyPartner", currentCompanyPartner);
    currentCompanyPartner &&
      dispatch({
        type: ADMIN_FORM.SET_VALUE,
        name: "companyPartner",
        payload: currentCompanyPartner,
      });
  }, [currentCompanyPartner, dispatch]);

  // reset distributor every role changes
  useEffect(() => {
    dispatch({
      type: ADMIN_FORM.SET_VALUE,
      name: "distributor",
      payload: "",
    });
  }, [dispatch, role]);

  return (
    <Modal className="modal__add-admin" size="lg" isOpen={isOpen}>
      <ModalHeader
        title={isEditing ? `Edit ${isEditing.name}` : "Pembuatan Admin Baru"}
        toggle={() => toggle && toggle()}
      />
      <ModalBody>
        <ContentItem title="Informasi Akun">
          <Input
            label="Nama User"
            value={name}
            name="name"
            onChange={({ target }) => onChangeValue(target.name, target.value)}
            placeholder="Masukkan Nama User"
            containerClassName="col-6"
          />
          <Input
            label="Nomor HP"
            value={phone}
            name="phone"
            onChange={({ target }) => onChangeValue(target.name, target.value)}
            placeholder="Masukkan Nomor HP"
            containerClassName="col-6"
          />
          <Input
            label="Email"
            value={email}
            name="email"
            onChange={({ target }) => onChangeValue(target.name, target.value)}
            placeholder="Masukkan Email"
            containerClassName="col-6"
          />
        </ContentItem>

        {isEditing ? (
          <ContentItem title="Role Akun">
            <Input
              containerClassName="col-6"
              label="Jabatan"
              value={enumAdmin(role)}
              disabled
            />
            <Input
              containerClassName="col-6"
              label="Distributor"
              value={isEditing.distributor?.name || "-"}
              disabled
            />
          </ContentItem>
        ) : (
          <ContentItem title="Role Akun">
            <Input
              label="Jabatan"
              value={role}
              name="role"
              onChange={({ target }) => onChangeValue(target.name, target.value)}
              type="select"
              containerClassName="col-6"
            >
              <option value="">Pilih Jabatan</option>
              <option value="super-pinter">Super Admin Pinter</option>
              <option value="super-company">Super Admin Company</option>
              {/* <option value="super-distributor">Super Distributor</option>
              <option value="distributor-courier">Kurir Distributor</option> */}
            </Input>

            <Input
              label="Company Partner"
              value={companyPartner}
              name="companyPartner"
              onChange={({ target }) => onChangeValue(target.name, target.value)}
              type="select"
              containerClassName="col-6"
              disabled={!role || role === "super-pinter"}
            >
              <option value="">Pilih Company Partner</option>
              {!companyPartners.length
                ? null
                : companyPartners.map((item, i) => {
                    return (
                      <option key={i} value={item._id}>
                        {item.name}
                      </option>
                    );
                  })}
            </Input>
          </ContentItem>
        )}

        <ContentItem title="Password">
          <Input
            disabled={isEditing !== undefined}
            label="Password"
            value={password}
            name="password"
            onChange={({ target }) => onChangeValue(target.name, target.value)}
            placeholder="Password"
            containerClassName="col-6"
          />
          <Input
            disabled={isEditing !== undefined}
            label="Ulangi Password"
            value={password2}
            name="password2"
            onChange={({ target }) => onChangeValue(target.name, target.value)}
            placeholder="Ulangi Password"
            containerClassName="col-6"
          />
        </ContentItem>

        <div className="button-action d-flex justify-content-end">
          {!isEditing ? null : (
            <Button
              disabled={isLoading}
              onClick={handleDelete}
              minWidth={80}
              className="mt-3"
              theme="danger"
            >
              Hapus
            </Button>
          )}
          &nbsp; &nbsp;
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={onSimpan}
            minWidth={80}
            className="mt-3"
            theme="primary"
          >
            Simpan
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
};

const ContentItem: FC<{ title: string; isRow?: boolean }> = ({
  children,
  title,
  isRow = true,
}) => {
  return (
    <div className="content-item">
      <div className="header">
        <SubjectTitle bold>{title}</SubjectTitle>
      </div>

      <Form className={`body ${isRow && "row"}`}>{children}</Form>
    </div>
  );
};
