import { AnyAction } from "redux";
import { IAdmin } from "../../../../redux/_interface/admin";

export interface IAdminState {
  isDeleting: boolean;

  admins: IAdmin[];
  admin?: IAdmin;

  filtered: IAdmin[];

  searchQuery: string;
  searched: IAdmin[];
  isSearching: boolean;

  loading: boolean;
  error: string;
}

export enum ADMIN {
  REQUEST = "ADMIN_REQUEST",
  FAILED = "ADMIN_FAILED",
  RESET = "ADMIN_RESET",
  SEARCH = "ADMIN_SEARCH",
  SEARCH_DONE = "ADMIN_SEARCH_DONE",
  SEARCH_RESET = "ADMIN_SEARCH_RESET",
  SET_ADMINS = "ADMIN_SET_ADMINS",
  SET_ADMIN = "ADMIN_SET_ADMIN",
  SET = "ADMIN_SET",
  SET_VALUE = "ADMIN_SET_VALUE",
  HANDLE_DELETE = "ADMIN_HANDLE_DELETE",
  CANCEL_DELETE = "ADMIN_CANCEL_DELETE",
}

export interface IAdminAction extends AnyAction {
  payload: any;
  name: string;
  type: ADMIN;
}
