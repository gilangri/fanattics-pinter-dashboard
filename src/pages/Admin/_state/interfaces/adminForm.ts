import { AnyAction } from "redux";
import { IAdmin, ICompanyPartner } from "../../../../redux/_interface";

export interface IAdminFormState {
  isOpen: boolean;
  isEditing?: IAdmin;

  pinterId?: string;
  name: string;
  phone: string;
  email: string;
  role: string;
  distributor: string;
  companyPartner: string;
  active: boolean;
  password: string;
  password2: string;

  selectedCompanyPartner?: ICompanyPartner;
}

export enum ADMIN_FORM {
  SET = "ADMIN_FORM_SET",
  SET_VALUE = "ADMIN_FORM_SET_VALUE",
  RESET = "ADMIN_FORM_RESET",
  EDITING = "ADMIN_FORM_EDITING",
  TOGGLE_IS_OPEN = "ADMIN_TOGGLE_IS_OPEN",
}

export interface IAdminFormAction extends AnyAction {
  payload: any;
  name: string;
  type: ADMIN_FORM;
}
