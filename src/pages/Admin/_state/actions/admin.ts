import axios from "axios";
import { API_URL } from "../../../../constants";
import { IAdmin } from "../../../../redux/_interface";
import { sortArrByKey } from "../../../../utils";
import { ADMIN } from "../interfaces/admin";
import { ADMIN_FORM } from "../interfaces/adminForm";

/**
 * fetching admin(s) data
 * @param id (optional) - provide id to get singular admin
 */
export const getAdmin =
  (id: string = "") =>
  async (dispatch: any) => {
    try {
      dispatch({ type: ADMIN.REQUEST });
      if (id) {
        const { data } = await axios.get(`${API_URL}/admins/${id}`);
        const selected = data.result.filter((val: IAdmin) => val._id === id);
        const payload = selected.length ? selected[0] : undefined;
        dispatch({ type: ADMIN.SET_ADMIN, payload });
      } else {
        const { data } = await axios.get(`${API_URL}/admins/${id}`);
        const result = !data.result.length
          ? []
          : data.result.filter(
              (item: any) =>
                item.role === "super-pinter" || item.role === "super-company"
            );

        console.log("GET ADMIN", data.result, result);
        const payload = sortArrByKey(result, "name");
        dispatch({ type: ADMIN.SET_ADMINS, payload });
      }
    } catch (err) {
      console.error(err);
      dispatch({ type: ADMIN.FAILED, payload: err });
    }
  };

/**
 * Delete admin by its id
 * @param id deleting admin id
 */
export const deleteAdmin = (id: string) => async (dispatch: any) => {
  try {
    dispatch({ type: ADMIN.REQUEST });
    await axios.delete(`${API_URL}/admins/${id}`);
    await Promise.all([dispatch(getAdmin())]);
    dispatch({ type: ADMIN.CANCEL_DELETE });
    dispatch({ type: ADMIN_FORM.RESET });
  } catch (err) {
    console.error(err);
    dispatch({ type: ADMIN.FAILED, payload: err });
  }
};

/**
 * Filter data by search query based on it's name
 * @param data fetched data
 * @param searchQuery inputted search query string
 * @param sortedBy parameter for sorting data
 * @returns filtered data based on searchQuery
 */
export const filterAdmin =
  (data: IAdmin[] = [], searchQuery: string = "", sortedBy: string) =>
  async (dispatch: any) => {
    const payload = !data.length
      ? []
      : data && !searchQuery
      ? data
      : data.filter((item) => {
          const query = searchQuery.toLowerCase();
          const name = item.name.toLowerCase();
          return name.includes(query);
        });

    const objectKey = sortedBy.split("-")[0];
    const inverse = sortedBy.split("-")[1] === "asc";

    const sorted = !payload.length ? [] : sortArrByKey(payload, objectKey, inverse);

    dispatch({ type: ADMIN.SET_VALUE, name: "filtered", payload: sorted });
  };
