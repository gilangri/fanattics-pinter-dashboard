import axios from "axios";
import { API_URL } from "../../../../constants";
import { IAdmin } from "../../../../redux/_interface";
import { ADMIN } from "../interfaces/admin";
import { ADMIN_FORM } from "../interfaces/adminForm";
import { getAdmin } from "./admin";

/**
 * Handler for create new admin or save edited admin if second parameter is provided
 * @param body object of new created admin
 * @param isEditing (optional) - currently edit admin data
 */
export const handleSaveAdmin =
  (
    body: Object,
    isEditing: IAdmin | undefined = undefined
  ) =>
  async (dispatch: any) => {
    try {
      await Promise.all(
        isEditing
          ? [dispatch(editAdmin(isEditing._id, body))]
          : [dispatch(createAdmin(body))]
      );
      console.log("saved");

      dispatch({ type: ADMIN_FORM.RESET });
    } catch (err) {
      console.log("error save");
      console.error(err);
    }
  };

/**
 * create new admin
 * @param body body of new admin
 */
export const createAdmin =
  (body: Object) => async (dispatch: any) => {
    try {
      dispatch({ type: ADMIN.REQUEST });
      const { data } = await axios.post(`${API_URL}/admins`, body);
      console.log(data);
      if (data.result.hasOwnProperty("errors")) throw new Error(data.result.message);
      await Promise.all([dispatch(getAdmin())]);
    } catch (err) {
      console.log("error create");
      console.error(err);
      dispatch({ type: ADMIN.FAILED, payload: err });

      throw new Error(err);
    }
  };

/**
 * Edit admin information
 * @param id selected admin's id
 * @param body body of edited admin
 */
export const editAdmin =
  (id: string, body: Object) => async (dispatch: any) => {
    try {
      dispatch({ type: ADMIN.REQUEST });
      await axios.put(`${API_URL}/admins/${id}`, body);
      await Promise.all([dispatch(getAdmin())]);
    } catch (err) {
      console.log("error edit");
      console.error(err);
      dispatch({ type: ADMIN.FAILED, payload: err });
    }
  };
