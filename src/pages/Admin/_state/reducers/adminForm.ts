import {
  ADMIN_FORM,
  IAdminFormAction,
  IAdminFormState,
} from "../interfaces/adminForm";

const init_state: IAdminFormState = {
  isOpen: false,
  isEditing: undefined,

  pinterId: "",
  name: "",
  phone: "",
  email: "",
  role: "",
  active: false,
  distributor: "",
  companyPartner: "",
  password: "",
  password2: "",

  selectedCompanyPartner: undefined,
};

export function adminFormReducer(
  state = init_state,
  { payload, name, type }: IAdminFormAction
): IAdminFormState {
  switch (type) {
    case ADMIN_FORM.SET:
      return { ...state, ...payload };
    case ADMIN_FORM.SET_VALUE:
      return { ...state, [name]: payload };

    case ADMIN_FORM.EDITING:
      return {
        ...state,
        ...payload,
        password: "",
        isOpen: true,
        isEditing: payload,
      };

    case ADMIN_FORM.TOGGLE_IS_OPEN:
      return { ...state, isOpen: !state.isOpen, isEditing: undefined };

    case ADMIN_FORM.RESET:
      return init_state;
    default:
      return state;
  }
}
