import { IAdminAction, IAdminState, ADMIN } from "../interfaces/admin";

const init_state: IAdminState = {
  isDeleting: false,

  admins: [],
  admin: undefined,

  filtered: [],

  searchQuery: "",
  searched: [],
  isSearching: false,

  loading: false,
  error: "",
};

export function adminReducer(
  state = init_state,
  { payload, name, type }: IAdminAction
): IAdminState {
  switch (type) {
    case ADMIN.REQUEST:
      return { ...state, loading: true, error: "" };
    case ADMIN.FAILED:
      return { ...state, loading: false, error: payload };

    case ADMIN.SEARCH:
      return { ...state, isSearching: true };
    case ADMIN.SEARCH_DONE:
      return { ...state, searched: payload };
    case ADMIN.SEARCH_RESET:
      return { ...state, isSearching: false, searched: [] };

    case ADMIN.SET_ADMINS:
      return { ...state, loading: false, admins: payload };
    case ADMIN.SET_ADMIN:
      return { ...state, loading: false, admin: payload };

    case ADMIN.SET:
      return { ...state, ...payload };
    case ADMIN.SET_VALUE:
      return { ...state, [name]: payload };

    case ADMIN.HANDLE_DELETE:
      return { ...state, isDeleting: true };
    case ADMIN.CANCEL_DELETE:
      return { ...state, isDeleting: false };

    case ADMIN.RESET:
      return init_state;
    default:
      return state;
  }
}
