export const role = {
  SUPER_ADMIN: "super-pinter",
  ADMIN: "admin",
  TRANSACTION_ADMIN: "transaction",
};

export const adminRole = {
  ...role,
  navbarOptions: [role.ADMIN, role.TRANSACTION_ADMIN],
};
