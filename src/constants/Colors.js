export const background = "#f5f6f8";
export const primary = "#323130";
export const secondary = "#605e5c";
export const disabled = "#a19f9d";
export const lighter = "#deecf9";

export const primaryBrand = "#3AA0F3";
export const primaryTint1 = "#78a25e";
export const primaryTint2 = "#87ad70";
export const primaryTint3 = "#96b782";
export const primaryTint4 = "#a5c194";
export const primaryTint5 = "#b4cca6";
export const primaryTint6 = "#c3d6b7";
export const primaryTint7 = "#d2e0c9";
export const primaryTint30 = "#daebcc";
export const primaryShade1 = "#5f8944";
export const primaryShade2 = "#547a3d";
export const primaryShade3 = "#4a6a35";
export const primaryShade4 = "#3f5b2e";
export const primaryShade5 = "#354c26";
export const primaryShade6 = "#2a3d1e";
export const primaryShade7 = "#1f2e17";

export const secondaryBrand = "#3f3f3f";
export const secondaryShade1 = "#393939";
export const secondaryShade2 = "#323232";
export const secondaryShade3 = "#2c2c2c";

export const white = "#fff";
export const black = "#000";
export const warning = "#dcab00";
export const danger = "#e05454";

export const gray20 = "#f3f2f1";
export const gray25 = "#f8f8f8";
export const gray30 = "#edebe9";
export const gray50 = "#f1f1f1";
export const gray60 = "#c8c6c4";
export const gray90 = "#a19f9d";
export const gray100 = "#e1e1e1";
export const gray110 = "#8a8886";
export const gray130 = "#605e5c";
export const gray160 = "#323130";
export const gray190 = "#201f1e";
export const gray200 = "#c8c8c8";
export const gray300 = "#acacac";
export const gray400 = "#919191";
export const gray500 = "#6e6e6e";
export const gray600 = "#404040";
export const gray700 = "#303030";
export const gray800 = "#292929";
export const gray900 = "#212121";

export const errorStatus = "#c70000";
export const errorDarker = "#a80000";
export const themeDark = "#005a9e";
export const teal = "#00B294";
