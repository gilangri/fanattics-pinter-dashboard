export const optionsFormData = {
  headers: { "Content-Type": "multipart/form-data" },
};

export const optionsFormUrlencoded = {
  headers: { "Content-Type": "application/x-www-form-urlencoded" },
};
