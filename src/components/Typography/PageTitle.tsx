import { ITypography } from "./ITypography";

export default function PageTitle({
  children,
  accent,
  bold,
  className,
  color,
  style,
  ...props
}: ITypography) {
  return (
    <div
      {...props}
      style={{ color, ...style }}
      className={`component-typography page-title ${bold ? "bold" : ""} ${
        accent ? "accent" : ""
      } ${className}`}
    >
      {children}
    </div>
  );
}
