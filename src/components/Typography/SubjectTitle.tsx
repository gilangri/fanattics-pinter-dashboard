import { ITypography } from "./ITypography";

export default function SubjectTitle({
  children,
  accent,
  bold,
  className,
  color,
  style,
  ...props
}: ITypography) {
  return (
    <div
      {...props}
      style={{ color, ...style }}
      className={`component-typography subject-title ${bold ? "bold" : ""} ${
        accent ? "accent" : ""
      } ${className}`}
    >
      {children}
    </div>
  );
}
