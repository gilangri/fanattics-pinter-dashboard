import { ITypography } from "./ITypography";

export default function PaneHeader({
  children,
  accent,
  bold,
  className,
  color,
  style,
  ...props
}: ITypography) {
  return (
    <div
      {...props}
      style={{ color, ...style }}
      className={`component-typography pane-header ${bold ? "bold" : ""} ${
        accent ? "accent" : ""
      } ${className}`}
    >
      {children}
    </div>
  );
}
