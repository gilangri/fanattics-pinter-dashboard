import React from "react";

interface IPaginationHook {
  data: any;
  itemsPerPage: number;
  pageNeighbours?: number;
}

export default function usePagination({ data, itemsPerPage, pageNeighbours = 2 }: IPaginationHook) {
  const [currentPage, setCurrentPage] = React.useState(1);
  const maxPage = Math.ceil(data.length / itemsPerPage);

  const setPage = (page = 1) => setCurrentPage(page);

  const currentData = () => {
    const begin = (currentPage - 1) * itemsPerPage;
    const end = begin + itemsPerPage;
    return data.slice(begin, end);
  };

  const next = () => {
    setCurrentPage((currentPage) => Math.min(currentPage + 1, maxPage));
  };

  const prev = () => {
    setCurrentPage((currentPage) => Math.max(currentPage - 1, 1));
  };

  const jump = (page: number) => {
    const pageNumber = Math.max(1, page);
    setCurrentPage((currentPage) => Math.min(pageNumber, maxPage));
  };

  //* Showing list of page numbers
  const range = (from: number, to: number, step = 1) => {
    let i = from;
    const range: number[] = [];
    while (i <= to) {
      range.push(i);
      i += step;
    }
    return range;
  };
  const fetchPageNumbers = () => {
    const LEFT_PAGE = "LEFT";
    const RIGHT_PAGE = "RIGHT";
    const totalNumbers = pageNeighbours * 2 + 3;
    const totalBlocks = totalNumbers + 2;

    if (maxPage > totalBlocks) {
      const startPage = Math.max(2, currentPage - pageNeighbours);
      const endPage = Math.min(maxPage - 1, currentPage + pageNeighbours);

      let pages: any[] = range(startPage, endPage);

      const hasLeftSpill = startPage > 2;
      const hasRightSpill = maxPage - endPage > 1;
      const spillOffset = totalNumbers - (pages.length + 1);

      switch (true) {
        // handle: (1) < {5 6} [7] {8 9} (10)
        case hasLeftSpill && !hasRightSpill: {
          const extraPages = range(startPage - spillOffset, startPage - 1);
          pages = [LEFT_PAGE, ...extraPages, ...pages];
          break;
        }
        // handle: (1) { 2 3} [4] {5 6} > (10)
        case !hasLeftSpill && hasRightSpill: {
          const extraPages = range(endPage + 1, endPage + spillOffset);
          pages = [...pages, ...extraPages, RIGHT_PAGE];
          break;
        }
        // handle: (1) < {4 5} [6] {7 8} > (10)
        case hasLeftSpill && hasRightSpill:
        default: {
          pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
          break;
        }
      }
      return [1, ...pages, maxPage];
    }
    return range(1, maxPage);
  };
  const pages = fetchPageNumbers() || [];

  return { next, prev, jump, currentData, setPage, pages, currentPage, maxPage };
}
