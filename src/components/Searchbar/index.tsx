/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { CSSProperties } from "react";
import { MdSearch, MdClose } from "react-icons/md";

interface ISearchbar {
  autoFocus?: boolean;
  value: string;
  onChange?: (value: string) => void;
  placeholder?: string;
  width?: number | string;
  containerStyle?: CSSProperties;
  inputStyle?: CSSProperties;
  className?: string;
  onKeyDown?: React.KeyboardEventHandler<HTMLInputElement>;
  onSubmit?: React.FormEventHandler<HTMLInputElement>;
}

export default function Searchbar({
  autoFocus,
  value = "",
  onChange,
  placeholder = "Cari ...",
  width,
  containerStyle = {},
  inputStyle = {},
  className = "",
  onKeyDown = (e) => e.key === "Enter" && e.preventDefault(),
  onSubmit,
}: ISearchbar) {
  const [searching, setSearching] = React.useState(false);
  const toggleSearching = () => setSearching(!searching);

  return (
    <div
      style={{ width: width ? width : "100%", ...containerStyle }}
      className={`searchbar ${className}`}
    >
      <div className={`icon icon-search ${searching ? "hide" : ""}`}>
        <MdSearch />
      </div>
      <input
        // onBlur={toggleSearching}
        // onFocus={toggleSearching}
        onChange={(e) => onChange && onChange(e.target.value)}
        value={value}
        onKeyDown={onKeyDown}
        // onSubmit={onSubmit}
        autoFocus={autoFocus}
        type="text"
        placeholder={placeholder}
        style={inputStyle}
      />
      <div
        role="button"
        onClick={() => onChange && onChange("")}
        className={`icon icon-clear ${value.length ? "" : "hide"}`}
      >
        <MdClose />
      </div>
    </div>
  );
}
