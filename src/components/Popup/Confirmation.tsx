import { Button } from "../Button";
import { Modal, ModalBody } from "../Modal";

interface Props {
  title?: string;
  isOpen?: boolean;
  isLoading?: boolean;
  onCancel?: () => void;
  onConfirm?: () => void;
  cancelButtonTheme?: "primary" | "secondary" | "danger";
  confirmButtonTheme?: "primary" | "secondary" | "danger";
  buttonMinWidth?: number;
  size?: "sm" | "md" | "lg" | "xl" | "auto";
  toggle?: () => void;
}
export default function Confirmation({
  title = "Konfirmasi?",
  isOpen,
  isLoading,
  onCancel,
  onConfirm,
  cancelButtonTheme = "secondary",
  confirmButtonTheme = "primary",
  buttonMinWidth = 80,
  size = "sm",
  toggle,
}: Props) {
  return (
    <Modal
      isOpen={isOpen}
      size={size}
      toggle={() => !isLoading && toggle && toggle()}
    >
      <ModalBody>
        <h5 className="d-flex justify-content-center text-center">{title}</h5>

        <div className="button-action d-flex justify-content-center">
          <Button
            disabled={isLoading}
            onClick={onCancel}
            minWidth={buttonMinWidth}
            className="mt-3"
            theme={cancelButtonTheme}
          >
            Tidak
          </Button>
          &nbsp; &nbsp;
          <Button
            isLoading={isLoading}
            disabled={isLoading}
            onClick={onConfirm}
            minWidth={buttonMinWidth}
            className="mt-3"
            theme={confirmButtonTheme}
          >
            Ya
          </Button>
        </div>
      </ModalBody>
    </Modal>
  );
}
