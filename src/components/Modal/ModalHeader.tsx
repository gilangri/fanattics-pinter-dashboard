import React from "react";
import { MdClose } from "react-icons/md";
import { ModalHeader as MyModalHeader } from "reactstrap";
import { Button } from "../Button";

interface IModal {
  className?: string;
  children?: React.ReactNode;
  title?: string;
  toggle?: () => void;
  onDelete?: () => void;
}

export function ModalHeader({
  className = "",
  children,
  title,
  toggle,
  onDelete,
}: IModal) {
  return (
    <MyModalHeader tag="div" className={`popup-header ${className}`}>
      {title ? (
        <div className="popup-header__title">
          <div className="title">{title}</div>

          {toggle ? (
            <Button onClick={toggle} theme="text">
              <MdClose size={24} />
            </Button>
          ) : null}

          {onDelete ? (
            <Button isRounded={false} onClick={onDelete} theme="danger">
              Hapus
            </Button>
          ) : null}
        </div>
      ) : (
        children
      )}
    </MyModalHeader>
  );
}
