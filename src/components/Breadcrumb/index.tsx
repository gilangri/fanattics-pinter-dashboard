import React from "react";
import { useHistory } from "react-router-dom";
import { gray500, gray700 } from "../../constants";
import { BodyText } from "../Typography";

interface Props {
  customHandler?: any;
  list: string[];
  onlyGoBack?: boolean;
  slug?: string[];
}

export function Breadcrumb({ customHandler, list, onlyGoBack, slug = [] }: Props) {
  const history = useHistory();
  return (
    <nav aria-label="breadcrumb">
      <ol className="component-breadcrumb">
        {list.map((val: string, i: number) => {
          const handleClick = () => {
            if (onlyGoBack) return history.goBack();
            if (slug.length && i === list.length - 2) {
              customHandler && customHandler();
              history.push(slug[i]);
            }
          };
          const className = () => {
            let string = "component-breadcrumb-item";
            if (!slug.length) return string;
            if (i === list.length - 2 && i !== 0) string += " clickable";
            return string;
          };
          return (
            <li onClick={handleClick} className={className()} key={i}>
              <BodyText
                color={i === list.length - 1 ? gray700 : gray500}
                bold={i === list.length - 1}
              >
                {val}
              </BodyText>
            </li>
          );
        })}
      </ol>
    </nav>
  );
}
