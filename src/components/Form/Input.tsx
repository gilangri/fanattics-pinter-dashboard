import React from "react";
import {
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
} from "reactstrap";
import { gray600 } from "../../constants";
import { MetadataText } from "../Typography";
import TextCounter from "./TextCounter";
import { Label } from "../Form";

interface IInput {
  append?: string;
  checked?: boolean;
  children?: React.ReactNode;
  className?: string;
  containerClassName?: string;
  containerStyle?: React.CSSProperties;
  inputStyle?: React.CSSProperties;
  defaultValue?: any;
  defaultChecked?: boolean;
  disabled?: boolean;
  id?: any;
  label?: string;
  max?: number | string | undefined;
  min?: number | string | undefined;
  maxLength?: number;
  name?: string;
  placeholder?: string;
  prepend?: any;
  onBlur?: any;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onFocus?: any;
  onKeyPress?: React.KeyboardEventHandler<HTMLInputElement>;
  type?:
    | "text"
    | "textarea"
    | "number"
    | "password"
    | "file"
    | "select"
    | "radio"
    | "checkbox"
    | "date";
  value?: any;
  helperText?: string;
}

export default function InputComponent({
  append,
  checked,
  children,
  className = "",
  containerClassName = "",
  defaultValue,
  defaultChecked,
  disabled = false,
  id,
  label,
  max,
  min,
  maxLength,
  name,
  placeholder,
  prepend,
  onBlur = () => null,
  onChange = () => null,
  onFocus = () => null,
  onKeyPress = () => null,
  type,
  value,
  containerStyle,
  helperText,
  inputStyle,
}: IInput) {
  return (
    <FormGroup
      className={containerClassName}
      style={{ marginBottom: label ? "1.5rem" : 0, ...containerStyle }}
    >
      {!label ? null : <Label bold>{label}</Label>}
      <InputGroup
        className={type === "radio" || type === "checkbox" ? "input-group-hide" : ""}
      >
        {!prepend ? null : (
          <InputGroupAddon addonType="prepend">
            <InputGroupText>{prepend}</InputGroupText>
          </InputGroupAddon>
        )}

        <Input
          id={id}
          checked={checked}
          className={`component-input ${className}`}
          defaultValue={defaultValue}
          defaultChecked={defaultChecked}
          disabled={disabled}
          max={max}
          min={min}
          maxLength={maxLength}
          name={name}
          onBlur={onBlur}
          onChange={(e) => onChange(e)}
          onFocus={onFocus}
          onKeyPress={onKeyPress}
          placeholder={placeholder}
          type={type}
          value={value}
          style={inputStyle}
        >
          {children}
        </Input>

        {type === "file" ? (
          <label className="component-btn component-btn-secondary">
            Browse File
          </label>
        ) : null}

        {!append ? null : (
          <InputGroupAddon addonType="append">
            <InputGroupText>{append}</InputGroupText>
          </InputGroupAddon>
        )}
      </InputGroup>

      {!maxLength ? null : (
        <TextCounter length={value.length} maxLength={maxLength} />
      )}

      {!helperText ? null : (
        <MetadataText
          style={{ fontSize: "10px", lineHeight: "12px", marginLeft: "12px" }}
          color={gray600}
        >
          {helperText}
        </MetadataText>
      )}
    </FormGroup>
  );
}
