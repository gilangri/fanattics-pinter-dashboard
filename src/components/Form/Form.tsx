import React from "react";
import { Form as MyForm } from "reactstrap";

interface IForm {
  children?: React.ReactNode;
  className?: string;
}

export default function Form({ children, className = "" }: IForm) {
  return <MyForm className={"component-form " + className}>{children}</MyForm>;
}
