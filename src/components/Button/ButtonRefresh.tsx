import { FC, Fragment, HTMLAttributes } from "react";
import { UilRepeat } from "@iconscout/react-unicons";
import { Spinner } from "./Spinner";

interface IButton extends HTMLAttributes<HTMLButtonElement> {
  isLoading?: boolean;
}

export const ButtonRefresh: FC<IButton> = ({
  isLoading = false,
  onClick,
  children,
}) => {
  return (
    <button className="component-btn button-refresh" onClick={onClick}>
      {isLoading ? (
        <Spinner />
      ) : children ? (
        children
      ) : (
        <Fragment>
          <UilRepeat size="16" color="#212121" />
        </Fragment>
      )}
    </button>
  );
};
