import { CSSProperties, HTMLAttributes, ReactNode } from "react";
import { useHistory } from "react-router-dom";
import { Spinner } from "./Spinner";

interface IButton extends HTMLAttributes<HTMLButtonElement> {
  active?: boolean;
  bold?: boolean;
  children: ReactNode;
  disabled?: boolean;
  to?: string;
  onClick?: any;
  theme?:
    | "primary"
    | "secondary"
    | "danger"
    | "text"
    | "icon"
    | "action"
    | "link"
    | "action-table"
    | "icon-only"
    | "primary-radius"
    | "primary-outline"
    | "outline-radius"
    | "outline";
  className?: string;
  containerStyle?: CSSProperties;
  block?: boolean;
  isLoading?: boolean;
  isRounded?: boolean;
  split?: boolean;
  minWidth?: number;
}

export default function Button({
  active,
  bold,
  children,
  className = "",
  containerStyle = {},
  disabled,
  onClick = () => null,
  // textStyle,
  theme,
  to,
  split,
  block,
  isLoading,
  isRounded = true,
  minWidth,
}: IButton) {
  const history = useHistory();

  return (
    <button
      className={`component-btn component-btn-${theme} component-btn-${
        isRounded && "rounded"
      } ${className} ${bold ? "bold" : ""} ${disabled ? "disabled" : ""} ${
        active ? "active" : ""
      } ${split ? "component-btn-split" : ""}`}
      style={{ ...containerStyle, minWidth }}
      onClick={to ? () => history.push(to) : onClick}
      disabled={disabled}
    >
      {isLoading ? <Spinner /> : children}
    </button>
  );
}
