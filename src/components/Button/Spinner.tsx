import React from "react";

export const Spinner = () => (
  <div className="spinner-border spinner-border-sm">
    <span className="sr-only"></span>
  </div>
);
