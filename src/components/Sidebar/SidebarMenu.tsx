import React from "react";
import { Document32 } from "@carbon/icons-react";
import { useHistory, useRouteMatch } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SIDEBAR_SELECT } from "../../redux/sidebar/types";

interface Props {
  children?: React.ReactNode;
  icon?: any;
  slug?: string;
  title: string;
  onClick?: () => void;
}
export function SidebarMenu({ children, icon, slug, title, onClick }: Props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const match = useRouteMatch();

  const handleClick = () => {
    if (onClick) return onClick();

    if (slug) {
      dispatch({ type: SIDEBAR_SELECT, payload: { title, slug } });
      history.push(slug);
    }
  };
  return (
    <div className="sidebar-wrapper">
      <div
        onClick={handleClick}
        className={`sidebar-menu ${match.path === slug ? "active" : ""}`}
      >
        <div className="icon-wrapper">{icon ? icon : <Document32 />}</div>
        <div className="title">{title}</div>
      </div>
      {children}
    </div>
  );
}
