import React from "react";
import { SidebarMenu } from "./SidebarMenu";
import { SidebarSubmenu } from "./SidebarSubmenu";

export default function Settlement() {
  const slugOpen = "/Settlement";
  return (
    <div className="sidebar-kur">
      <SidebarMenu title="Product Management">
        <SidebarSubmenu
          title="Transaksi Berlangsung"
          slug={`${slugOpen}/on-process`}
          badge={20}
        />
        <SidebarSubmenu title="Transaksi Selesai" slug={`${slugOpen}/`} badge={20} />
        <SidebarSubmenu title="Laporan Transaksi" badge={20} />
      </SidebarMenu>
    </div>
  );
}
