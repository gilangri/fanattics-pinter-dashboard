import React from "react";
import { useDispatch } from "react-redux";
import { useHistory, useRouteMatch } from "react-router-dom";
import { SIDEBAR_SELECT } from "../../redux/sidebar/types";

interface Props {
  slug?: string;
  title: string;
  badge?: number;
}
export function SidebarSubmenu({ slug, title, badge }: Props) {
  const dispatch = useDispatch();
  const match = useRouteMatch();
  const history = useHistory();

  const handleClick = () => {
    if (!slug) return;
    dispatch({ type: SIDEBAR_SELECT, payload: { title, slug } });
    history.push(slug);
  };

  return (
    <div
      className={`sidebar-submenu ${match.url === slug ? "active" : ""}`}
      onClick={handleClick}
    >
      <div className="title">{title}</div>
      {/* {badge ? <div className="badge">{badge}</div> : null} */}
    </div>
  );
}
