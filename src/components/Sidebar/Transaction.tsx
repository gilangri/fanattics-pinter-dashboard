import React from "react";
import { SidebarMenu } from "./SidebarMenu";
import { SidebarSubmenu } from "./SidebarSubmenu";
import { RiErrorWarningLine } from "react-icons/ri";

export default function Transaction() {
  const slugOpen = "/transaction";

  return (
    <div className="sidebar-kur">
      <SidebarMenu title="Transaksi" icon={<RiErrorWarningLine />}>
        <SidebarSubmenu
          title="Pending Transaction Ledger"
          slug={`${slugOpen}/report/pending`}
          badge={20}
        />
        <SidebarSubmenu
          title="Verified Transaction Ledger"
          slug={`${slugOpen}/report/verified`}
          badge={20}
        />
        <SidebarSubmenu
          title="Failed Transaction Ledger"
          slug={`${slugOpen}/report/failed`}
          badge={20}
        />
        <SidebarSubmenu
          title="KUR Transaction Ledger"
          slug={`${slugOpen}/report/kur`}
          badge={20}
        />
      </SidebarMenu>
      <SidebarMenu title="Settlement" icon={<RiErrorWarningLine />}>
        <SidebarSubmenu
          title="Distributor Settlement"
          slug={`${slugOpen}/settlement`}
          badge={20}
        />
      </SidebarMenu>
    </div>
  );
}
