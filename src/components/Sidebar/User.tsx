import React from "react";
import { SidebarMenu } from "./SidebarMenu";
import { SidebarSubmenu } from "./SidebarSubmenu";
import { Settings16 } from "@carbon/icons-react";

export default function DistributorCatalog() {
  const slugOpen = "/all-user";
  return (
    <div className="sidebar-kur">
      <SidebarMenu title="UMKM Catalog">
        <SidebarSubmenu title="All User" slug={`${slugOpen}`} badge={20} />
      </SidebarMenu>
    </div>
  );
}
