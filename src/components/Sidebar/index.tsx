import React from "react";
import { SidebarMenu } from "./SidebarMenu";
import { SidebarSubmenu } from "./SidebarSubmenu";
import fanatticsLogo from "../../assets/images/fanattics-logo.png";

export default function Sidebar() {
  const trxSlug = "/product/transaction";

  return (
    <div className="sidebar-kur">
      <div className="sidebar-wrapper">
        <SidebarMenu title="Transaksi">
          <SidebarSubmenu
            title="Transaksi Berlangsung"
            badge={20}
            slug={`${trxSlug}/on-process`}
          />
          <SidebarSubmenu title="Transaksi Selesai" badge={0} />
          <SidebarSubmenu title="Laporan Transaksi" badge={0} />
        </SidebarMenu>
        <SidebarMenu title="Member UMKM">
          <SidebarSubmenu title="Transaksi Berlangsung" badge={0} />
        </SidebarMenu>
      </div>

      <div className="logo-fanattics">
        <span>Powered By</span>
        <img src={fanatticsLogo} alt="" />
      </div>
    </div>
  );
}
