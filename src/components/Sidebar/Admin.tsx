import React from "react";
import { SidebarMenu } from "./SidebarMenu";
import { SidebarSubmenu } from "./SidebarSubmenu";
// import { FaRegIdCard } from "react-icons/fa";
import { RiErrorWarningLine } from "react-icons/ri";
import { GrGroup } from "react-icons/gr";
import { UilStore } from "@iconscout/react-unicons";
// import { SIDEBAR_SELECT } from "../../redux/sidebar/types";

// import { useHistory } from "react-router";
// import { useDispatch } from "react-redux";

export default function Admin() {
  // const dispatch = useDispatch();
  // const history = useHistory();
  const slugOpen = "/admin";
  const slugCompanyPartner = "/company-partner";
  // const userSlug = "/umkm";

  return (
    <div className="sidebar-kur">
      <SidebarMenu title="Dashboard" />
      <SidebarMenu title="UMKM Catalog" icon={<RiErrorWarningLine />}>
        <SidebarSubmenu title="All User" slug={`${slugOpen}/members`} badge={20} />
      </SidebarMenu>
      <SidebarMenu
        title="Distributor"
        icon={<UilStore />}
        slug={`${slugOpen}/distributors`}
      />
      {/* <SidebarSubmenu
          title="All User"
          slug={`${slugOpen}/distributors`}
          badge={20}
        /> */}
      {/* </SidebarMenu> */}

      <SidebarMenu
        title="Internal Admin"
        icon={<UilStore />}
        slug={`${slugOpen}/internals`}
      />
      {/* <SidebarMenu title="Internal Admin Management">
        <SidebarSubmenu
          title="All Admins"
          slug={`${slugOpen}/internals`}
          badge={20}
        />
      </SidebarMenu> */}

      <SidebarMenu
        title="Company Partner"
        icon={<GrGroup />}
        slug={`${slugCompanyPartner}`}
      />

      {/* <SidebarMenu title="Members" icon={<FaRegIdCard />} /> */}
    </div>
  );
}
