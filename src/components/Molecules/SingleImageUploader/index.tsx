import "./singleImageUploader.scss";

import { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { UilTimes } from "@iconscout/react-unicons";
import { Button } from "../../Button";

interface Props {
  className?: string;
  file?: File;
  handleDelete?: (file?: File) => void;
  label?: string;
  onChange: (file?: File) => void;
}
export function SingleImageUploader({
  className = "",
  file,
  handleDelete,
  label,
  onChange,
}: Props) {
  const { getRootProps, getInputProps } = useDropzone({
    multiple: false,
    accept: [".jpg", ".png", ".jpeg", ".webp"],
    noKeyboard: true,
    onDrop: useCallback(
      (acceptedFiles: File[]) => onChange(acceptedFiles[0]),
      [onChange]
    ),
  });

  return (
    <div className={`dropzone-image-upload-container ${className}`}>
      {!label ? null : <div className="image-label">{label}</div>}

      <div className="image-uploader">
        {file ? null : (
          <div {...getRootProps({ className: "dropzone" })}>
            <div className="image-catalog">
              <label className="component-btn component-btn-primary">Unggah</label>
            </div>
            <input {...getInputProps()} />
          </div>
        )}

        {!file ? null : (
          <div className="image-catalog">
            <Button theme="text" onClick={() => onChange(undefined)}>
              <UilTimes size={16} />
            </Button>
            <img src={URL.createObjectURL(file)} alt="profilePicture" />
          </div>
        )}
      </div>
    </div>
  );
}
