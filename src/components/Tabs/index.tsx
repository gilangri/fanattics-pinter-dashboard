/* eslint-disable import/no-anonymous-default-export */
import { BodyText, SubjectTitle } from "../Typography";

interface ITabs {
  activeTab: string;
  className?: string;
  counter?: number[];
  options: string[];
  subtitle?: string[];
  setActiveTab: (value: string) => void;
  slug?: string[];
  type?: "default" | "block" | "full-block";
  textAlign?: "left" | "center" | "right";
  width?: number | string | "auto" | "min-content" | "max-content" | "fit-content";
}

export default function Tab({
  activeTab,
  className,
  counter,
  options,
  subtitle,
  setActiveTab,
  slug = [],
  type = "default",
  textAlign = "left",
  width = "auto",
}: ITabs) {
  const boldIndicator = (option: string, i: number) => {
    if (slug.length > 0) {
      if (slug[i] === activeTab) return true;
    } else if (option === activeTab) return true;
    return false;
  };

  return type === "full-block" ? (
    <div className={`component-tab-container full-block ${className}`}>
      {options.map((option, i) => {
        const isActive = boldIndicator(option, i);
        return (
          <div
            onClick={() => setActiveTab(slug.length ? slug[i] : option)}
            className={`tab-item ${isActive ? "active" : ""}`}
            key={i}
          >
            <SubjectTitle bold className="title" accent={isActive}>
              {option}
            </SubjectTitle>

            {!subtitle?.length ? null : (
              <BodyText className="subtitle" color="blue">
                {subtitle[i]}
              </BodyText>
            )}
          </div>
        );
      })}
    </div>
  ) : (
    <div className={`component-tab-container ${type} ${className}`}>
      {options.map((option, i) => {
        return (
          <BodyText
            key={i}
            className="tab-item"
            onClick={() => setActiveTab(slug.length ? slug[i] : option)}
            bold={boldIndicator(option, i)}
            style={{ textAlign, width }}
          >
            {option}&nbsp;
            {!counter ? null : !counter[i] ? null : `(${counter[i]})`}
          </BodyText>
        );
      })}
    </div>
  );
}
