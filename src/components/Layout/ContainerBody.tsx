import { FC, ReactNode } from "react";
import "./layout.scss";

interface IBody {
  children: ReactNode;
  className?: string;
}

/**
 * @param {ReactNode} children is required!
 * @param {string} className (optional) default: container__body
 * @param {string} orientation (optional) default: vertical
 */
export function ContainerBody({ children, className }: IBody) {
  return <div className={`container__body ${className}`}>{children}</div>;
}

interface IBodyContent {
  borderRadius?: "0" | "8" | "16" | "24";
  className?: string;
  col?: number;
  padding?: "0" | "8" | "16" | "24";
  transparent?: boolean;
}
/**
 *
 * @param children is required!
 * @param borderRadius "0" | "8" | "16" | "24";
 * @param col grid number on 12-grid basis
 * @param className custom className
 * @param padding "0" | "8" | "16" | "24";
 */
export const ContainerBodyContent: FC<IBodyContent> = ({
  borderRadius = "8",
  children,
  className = "",
  col = 12,
  padding = "16",
  transparent,
}) => {
  return (
    <div className={`body_content column-${col}`}>
      <div
        className={`border-radius-${borderRadius} padding-${padding} ${
          transparent && "transparent"
        } ${className}`}
      >
        {children}
      </div>
    </div>
  );
};
