import { Fragment, MouseEventHandler, ReactNode } from "react";
import { ButtonIcon, ButtonRefresh } from "../Button";
import { UilArrowLeft } from '@iconscout/react-unicons'
import { useHistory } from "react-router";
import "./layout.scss";

interface PropsHead {
  children?: ReactNode;
  className?: string;
  title?: string;
  titleStyle?: {
    bold?: boolean;
  };
  backButton?: boolean;
  backButtonText?: string;
  backButtonHandler?: () => void;
  onRefresh?: MouseEventHandler<HTMLButtonElement>;
  refreshing?: boolean;
  imageLogo?: string | null;
  subtitle?: { [key: string]: string }[];
}

/**
 * Container for header
 * @param {boolean} backButton to show back button with default text
 * @param {string} backButtonText to show back button with custom text
 * @param {() => void} backButtonHandler call a function before calling history.goBack()
 * @param {string} className (optional) default: container__header
 * @param {string} title optional
 * @param titleStyle custom styling for the title
 * @param titleStyle.bold default is true
 * @param onRefresh function called on press refresh button [optional]
 * @param refreshing loading state on refresh button [optional]
 * @param imageLogo show image/logo on container header
 * @param subtitle show subtitle. example:
 *
  ```js
  [
  { "Subitle 1": "the subtitle value 1" }, /// Subtitle 1  the subtitle value 1
  { "Subitle 2": "the subtitle value 2" }, /// Subtitle 2  the subtitle value 2
  ]
  ```
 */
export function ContainerHead({
  backButton,
  backButtonText,
  backButtonHandler,
  children,
  className = "",
  title,
  titleStyle = { bold: true },
  onRefresh,
  refreshing,
  imageLogo,
  subtitle,
}: PropsHead) {
  const history = useHistory();

  const handleBack = () => {
    if (backButtonHandler) backButtonHandler();
    history.goBack();
  };

  return (
    <Fragment>
      <div className={`container__header ${!title && "no-title"} ${className}`}>
        {backButton || backButtonText ? (
          <div className="container__back-button">
            <div className="back-button" onClick={handleBack}>
              <ButtonIcon type="prepend">
                <UilArrowLeft color="#0c70d0" />
              </ButtonIcon>
              {backButtonText || "Kembali"}
            </div>
          </div>
        ) : null}

        <div className="header-body">
          {!imageLogo || imageLogo === "" ? null : (
            <div className="header__image">
              <img src={imageLogo} alt="logo" />
            </div>
          )}

          <div className="header__content">
            <div
              className={`header__title header__title-${titleStyle?.bold && "bold"}`}
            >
              {title}
            </div>

            {!subtitle?.length ? null : (
              <div className="header__subtitle">
                {subtitle.map((e, i) => (
                  <div className="subtitle" key={i}>
                    <span>{Object.keys(e)[0]}</span>

                    {e[Object.keys(e)[0]]}
                  </div>
                ))}
              </div>
            )}
          </div>

          <div className="header__action">
            {children}

            {onRefresh || refreshing ? (
              <ButtonRefresh onClick={onRefresh} isLoading={refreshing} />
            ) : null}
          </div>
        </div>
      </div>
    </Fragment>
  );
}
