/* eslint-disable import/no-anonymous-default-export */
import React, { ReactNode } from "react";

interface IPanel extends React.HTMLAttributes<HTMLDivElement> {
  children: ReactNode;
  padding?: "16" | "24";
  depth?: "4" | "8" | "16" | "64" | "0";
}

export default ({
  children,
  padding = "24",
  depth = "8",
  className,
  ...props
}: IPanel) => {
  return (
    <div
      {...props}
      className={`component-panel component-panel-${padding} component-depth-${depth} ${className}`}
    >
      {children}
    </div>
  );
};
