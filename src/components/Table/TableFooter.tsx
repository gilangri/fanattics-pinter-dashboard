import React from "react";

interface ITableFooter extends React.HTMLAttributes<HTMLTableSectionElement> {
  children: React.ReactNode;
}

export default function TableFooter({ children, ...props }: ITableFooter) {
  return <tfoot {...props}>{children}</tfoot>;
}
