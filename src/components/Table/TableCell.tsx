import React from "react";

interface ITableCell extends React.HTMLAttributes<HTMLTableDataCellElement> {
  children?: React.ReactNode;
  colSpan?: number;
  align?: "left" | "center" | "right" | "justify" | "char";
  width?: number | string;
}

export default function TableCell({
  children,
  colSpan,
  align,
  style,
  width,
  ...props
}: ITableCell) {
  return (
    <td
      {...props}
      colSpan={colSpan}
      style={{ width, ...style }}
      align={align}
    >
      {children}
    </td>
  );
}
