import React from "react";
import { FiChevronDown, FiChevronUp } from "react-icons/fi";
import { gray400 } from "../../constants";

interface ITableHeadCell {
  align?: "left" | "center" | "right";
  children: React.ReactNode;
  className?: string;
  colSpan?: number;
  name?: string;
  sortedBy?: string;
  style?: React.CSSProperties;
  onClick?: (value: string) => void;
  width?: string | number;
}

/**
 * custom TableHead with sort function
 * @param align set text align
 * @param className set custom className
 * @param colSpan set colSpan on each th
 * @param name (optional) set name only for sorting purposes
 * @param sortedBy (optional) current parameter for sorting data
 * @param onClick (optional) change parameter value for sorting data
 * @param width (optional) set a width of cell
 */
export default function TableHeadCell({
  align = "left",
  onClick,
  children,
  className = "",
  colSpan,
  name,
  sortedBy,
  style,
  width,
  ...props
}: ITableHeadCell) {
  const type =
    sortedBy === `${name}-asc`
      ? "up"
      : sortedBy === `${name}-desc`
      ? "down"
      : "default";

  return (
    <th
      {...props}
      style={{ width, ...style }}
      className={`${onClick && "clickable"} text-align-${align} ${className}`}
      onClick={() =>
        onClick &&
        onClick(sortedBy === `${name}-asc` ? `${name}-desc` : `${name}-asc`)
      }
      colSpan={colSpan}
      align={align}
    >
      {!onClick ? (
        children
      ) : (
        <div className="th-item">
          <div className="title">{children}</div>
          &nbsp; &nbsp;
          <div className="status">
            <FiChevronUp size={18} color={type === "up" ? "#000" : gray400} />
            <FiChevronDown size={18} color={type === "down" ? "#000" : gray400} />
          </div>
        </div>
      )}
    </th>
  );
}
