import React from "react";

interface ITableRow extends React.HTMLAttributes<HTMLTableRowElement> {
  children: React.ReactNode;
  onClick?: any;
  onMouseOver?: any;
}

export default function TableRow({
  onClick = () => null,
  onMouseOver,
  children,
  ...props
}: ITableRow) {
  return (
    <tr {...props} onClick={onClick} onMouseOver={onMouseOver}>
      {children}
    </tr>
  );
}
