import "./dropdown.scss";

import { UilEllipsisV } from "@iconscout/react-unicons";
import {
  Dropdown as DropdownReactstrap,
  DropdownMenu,
  DropdownToggle,
} from "reactstrap";

export const Dropdown: React.FC<{
  className?: string;
  isOpen: boolean;
  toggle: React.MouseEventHandler<any>;
}> = ({ children, className = "", isOpen, toggle }) => {
  return (
    <DropdownReactstrap
      isOpen={isOpen}
      toggle={toggle}
      className={`dropdown__menu-button ${className}`}
    >
      <DropdownToggle tag="span">
        <UilEllipsisV />
      </DropdownToggle>

      <DropdownMenu right>{children}</DropdownMenu>
    </DropdownReactstrap>
  );
};
