export const DropdownItem: React.FC<{
  isDisabled?: boolean;
  name?: string;
  onClick?: React.MouseEventHandler<HTMLDivElement>;
}> = ({ isDisabled, children, name = "", onClick }) => {
  return (
    <div
      onClick={onClick}
      className={`menu-item menu-item-${name} ${isDisabled && "disabled"}`}
    >
      {children}
    </div>
  );
};
