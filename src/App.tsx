/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useEffect } from "react";
import NavBar from "./components/Navbar";
import { AppRoutes } from "./routes";
import { Switch, Route, Redirect, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { SIDEBAR_ROLE, SIDEBAR_SELECT } from "./redux/sidebar/types";
import Login from "./pages/Login";
import { RootState } from "./redux/store";
import Cookies from "js-cookie";
import { ReloginAction } from "./redux/auth/actions";

export default function App() {
  const dispatch = useDispatch();
  const history = useHistory();

  const isLogin = useSelector((state: RootState) => state.auth.user);
  const sidebarRole = useSelector((state: RootState) => state.sidebar.role);

  useEffect(() => {
    if (sidebarRole === "super-pinter") {
      dispatch({ type: SIDEBAR_ROLE, payload: "admin" });
    }

    if (sidebarRole === "admin") {
      let title = "Admin Catalog";
      let slug = "/admin/members";
      dispatch({ type: SIDEBAR_SELECT, payload: { title, slug } });
      history.push(slug);
    }

    if (sidebarRole === "transaction") {
      let title = "Settlement";
      let slug = "/transaction/settlement";
      dispatch({ type: SIDEBAR_SELECT, payload: { title, slug } });
      history.push(slug);
    }

    // slug = "/sandbox";
  }, [dispatch, history, sidebarRole]);

  useEffect(() => {
    let token = localStorage.getItem("token");
    if (token) dispatch(ReloginAction(token));
  }, [dispatch]);

  if (!isLogin)
    return (
      <div className="App-wrapper">
        <Route path="/login" component={Login} />
        <Redirect to="/login" />
      </div>
    );
  return (
    <React.Fragment>
      <NavBar />

      <div className="App-wrapper">
        {AppRoutes.map((route) => (
          <Route
            key={route.path}
            path={route.path}
            exact={route.exact}
            component={route.sidebar}
          />
        ))}

        <Switch>
          {AppRoutes.map((route) => (
            <Route
              key={route.path}
              path={route.path}
              exact={route.exact}
              component={route.main}
            />
          ))}
        </Switch>
      </div>
    </React.Fragment>
  );
}
