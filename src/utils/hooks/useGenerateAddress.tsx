import axios from "axios";
import { useEffect, useState } from "react";
import { API_URL } from "../../constants";
import { sortArrByKey } from "../helpers";

export function useGenerateAddresses() {
  const [address, setAddress] = useState("");
  const [provinces, setProvinces] = useState<IProvince[]>([]);
  const [provinceId, setProvinceId] = useState(0);
  const [province, setProvince] = useState("");

  const [cities, setCities] = useState<ICity[]>([]);
  const [cityId, setCityId] = useState(0);
  const [city, setCity] = useState("");

  const [districts, setDistricts] = useState<IDistrict[]>([]);
  const [districtId, setDistrictId] = useState(0);
  const [district, setDistrict] = useState("");

  const [subdistricts, setSubdistricts] = useState<ISubdistrict[]>([]);
  const [subdistrictId, setSubdistrictId] = useState(0);
  const [subdistrict, setSubdistrict] = useState("");
  const [postalCode, setPostalCode] = useState(0);

  //* GET ALL PROVINCES
  useEffect(() => {
    async function getProvinces() {
      const { data } = await axios.get(`${API_URL}/locations/provinces`);
      setProvinces(sortArrByKey(data.result, "province"));
    }
    getProvinces();
  }, []);

  //* GET ALL CITIES BASED ON SELECTED PROVINCE
  useEffect(() => {
    async function getCities(id: number) {
      const { data } = await axios.get(`${API_URL}/locations/cities/${id}`);
      setCities(data.result);
    }
    getCities(provinceId);
  }, [provinceId]);

  //* GET ALL DISTRICTS BASED ON SELECTED CITY
  useEffect(() => {
    async function getDistricts(id: number) {
      const { data } = await axios.get(`${API_URL}/locations/districts/${id}`);
      setDistricts(data.result);
    }
    getDistricts(cityId);
  }, [cityId]);

  //* GET ALL SUBDISTRICTS BASED ON SELECTED DISTRICT
  useEffect(() => {
    async function getSubistricts(id: number) {
      const { data } = await axios.get(`${API_URL}/locations/subdistricts/${id}`);
      setSubdistricts(data.result);
    }
    getSubistricts(districtId);
  }, [districtId]);

  //* ================================================== FUNCTION CHANGE HANDLER

  /**
   * handler for changing address value
   * @param value `string` inputted value
   */
  const changeAddress = (value: string) => setAddress(value);

  /**
   * handler for changing selected province and fetch all the cities based on selected province
   * @param _id `string` selected province id
   */
  function changeProvince(_id: string) {
    let id = parseInt(_id);
    setProvinceId(id);
    setProvince(provinces.find((e) => e._id === id)?.province || "");
    changeCity("0");
  }

  /**
   * handler for changing selected city and fetch all the districts based on selected city
   * @param _id `string` selected city id
   */
  function changeCity(_id: string) {
    let id = parseInt(_id);
    setCityId(id);
    setCity(cities.find((e) => e._id === id)?.name || "");
    changeDistrict("0");
  }

  /**
   * handler for changing selected district and fetch all the subdistricts based on selected district
   * @param _id `string` selected district id
   */
  function changeDistrict(_id: string) {
    let id = parseInt(_id);
    setDistrictId(id);
    setDistrict(districts.find((e) => e._id === id)?.name || "");
    changeSubdistrict("0");
  }

  /**
   * handler for changing selected subdistrict and select default postal code
   * @param _id `string` selected subdistrict id
   */
  function changeSubdistrict(_id: string) {
    let id = parseInt(_id);
    setSubdistrictId(id);
    setSubdistrict(subdistricts.find((e) => e._id === id)?.name || "");
    changePostalCode(subdistricts.find((e) => e._id === id)?.postalCode || 0);
  }

  /**
   * handler for changing postal code value
   * @param value `number` or `string` inputted value
   */
  function changePostalCode(value: string | number) {
    setPostalCode(typeof value === "string" ? parseInt(value) : value);
  }

  return {
    address,
    changeAddress,
    provinces,
    provinceId,
    province,
    changeProvince,
    cities,
    cityId,
    city,
    changeCity,
    districts,
    districtId,
    district,
    changeDistrict,
    subdistricts,
    subdistrictId,
    subdistrict,
    changeSubdistrict,
    postalCode,
    changePostalCode,
  };
}

export interface IUseGenerateAddresses {
  address: string;
  changeAddress: (value: string) => void;

  provinces: IProvince[];
  provinceId: number;
  province: string;
  changeProvince: (_id: string) => void;

  cities: ICity[];
  cityId: number;
  city: string;
  changeCity: (_id: string) => void;

  districts: IDistrict[];
  districtId: number;
  district: string;
  changeDistrict: (_id: string) => void;

  subdistricts: ISubdistrict[];
  subdistrictId: number;
  subdistrict: string;
  changeSubdistrict: (_id: string) => void;

  postalCode: number;
  changePostalCode: (_id: string) => void;
}

interface IProvince {
  _id: number;
  province: string;
  capital: string;
  code: string;
}

interface ICity {
  _id: number;
  name: string;
  capitalName: string;
  code: string;
  provinceId: number;
}

interface IDistrict {
  _id: number;
  name: string;
  cityId: number;
}

interface ISubdistrict {
  _id: number;
  districtId: number;
  name: string;
  postalCode: number;
}
