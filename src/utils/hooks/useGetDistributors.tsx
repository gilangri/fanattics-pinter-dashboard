import axios from "axios";
import { useEffect } from "react";
import { useState } from "react";
import { API_URL } from "../../constants";
import { IDistributor } from "../../redux/_interface";
import { sortArrByKey } from "../helpers";

export function useGetDistributors(companyId: string = "") {
  const [refreshing, onRefresh] = useState(new Date());
  const [distributors, setDistributors] = useState<IDistributor[]>([]);

  useEffect(() => {
    let isMounted = true;

    async function getDistributors(companyId: string = "") {
      try {
        const { data } = await axios.get(`${API_URL}/distributors`);

        if (isMounted) {
          const result = !data.result ? [] : sortArrByKey(data.result, "name");
          const filtered = companyId
            ? result.filter((e: any) => e.companyPartner._id === companyId)
            : [];
          setDistributors(filtered);
        }
      } catch (err) {
        console.error(err);
      }
    }

    getDistributors(companyId);

    return () => {
      isMounted = false;
    };
  }, [companyId, refreshing]);

  return { distributors, refreshing, onRefresh };
}
