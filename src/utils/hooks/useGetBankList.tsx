import axios from "axios";
import { useEffect, useState } from "react";
import { API_URL } from "../../constants";
import { IBank } from "../../redux/_interface";
import { sortArrByKey } from "../helpers";

export const useGetBankList = () => {
  const [refreshing, onRefresh] = useState(new Date());
  const [banks, setBanks] = useState<IBank[]>([]);

  useEffect(() => {
    let isMounted = true;

    async function getBanks() {
      try {
        const { data } = await axios.get(`${API_URL}/banks`);
        const payload = sortArrByKey(data.result, "name");
        if (isMounted) setBanks(payload);
      } catch (err) {
        console.error(err);
      }
    }

    getBanks();

    return () => {
      isMounted = false;
    };
  }, [refreshing]);

  return { banks, refreshing, onRefresh };
};
