import axios from "axios";
import { useEffect } from "react";
import { useState } from "react";
import { API_URL } from "../../constants";
import { ICompanyPartner } from "../../redux/_interface";
import { sortArrByKey } from "../helpers";

export function useGetCompanyPartners() {
  const [refreshing, onRefresh] = useState(new Date());
  const [companyPartners, setCompanyPartners] = useState<ICompanyPartner[]>([]);

  useEffect(() => {
    let isMounted = true;

    async function getCompanyPartners() {
      try {
        const { data } = await axios.get(`${API_URL}/company-partners`);
        const payload = !data.result ? [] : sortArrByKey(data.result, "name");
        if (isMounted) setCompanyPartners(payload);
      } catch (err) {
        console.error(err);
      }
    }

    getCompanyPartners();

    return () => {
      isMounted = false;
    };
  }, [refreshing]);

  return { companyPartners, refreshing, onRefresh };
}
