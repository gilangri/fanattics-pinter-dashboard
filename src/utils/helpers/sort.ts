/**
 * Sort array by any key (except date, use sortArrByDateKey for that!)
 * @param arr any array to sort
 * @param key key object's name
 * @param reverse default = false
 * @returns sorted array by key
 */
export const sortArrByKey = (
  array: any[],
  key: string,
  reverse: boolean = false
) => {
  return array.sort((a: any, b: any) => {
    if (reverse) {
      return b[key] > a[key] ? 1 : b[key] < a[key] ? -1 : 0;
    } else {
      return a[key] > b[key] ? 1 : a[key] < b[key] ? -1 : 0;
    }
  });
};

/**
 * Sort array by date
 * @param array any array to sort
 * @param key key object's name (only datetime value)
 * @param reverse (optional) - default false
 * @returns sorted array by date's key
 */
export const sortArrByDateKey = (
  array: any[],
  key: string,
  reverse: boolean = false
) => {
  return array.sort((a: any, b: any) => {
    const dateA: any = new Date(a[key]);
    const dateB: any = new Date(b[key]);
    return reverse ? dateB - dateA : dateA - dateB;
  });
};
