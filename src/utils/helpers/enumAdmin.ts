export const enumAdmin = (value: string) => {
  if (value === "admin") return "Admin";
  if (value === "transaction") return "Transaction";
  if (value === "super-pinter") return "Super Admin Pinter";
  if (value === "super-company") return "Super Admin Company";
  return "#N/A";
};
