export const joinAddress = (v: IAddress | undefined | null = undefined) => {
  if (!v) return "";

  let arr: any[] = [];

  if (v.address) arr.push(v.address);
  if (v.subdistrict) arr.push(v.subdistrict);
  if (v.district) arr.push(v.district);
  if (v.city) arr.push(v.city);
  if (v.province) arr.push(v.province);
  if (v.postalCode) arr.push(v.postalCode);

  return arr.join(", ");

  // return `${v.address} ${v.subdistrict}, ${v.district}, ${v.city}, ${v.province}, ${v.postalCode}`;
};

export interface IAddress {
  address: string;
  province: string;
  city: string;
  district: string;
  subdistrict: string;
  postalCode: string | number;
}
