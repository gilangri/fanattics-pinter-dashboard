import "./imageGenerator.scss";

interface P {
  alt?: string;
  backgroundColor?: string;
  className?: string;
  color?: string;
  src?: string | null;
  value?: string;
}
export function ImageGenerator({
  alt = "image",
  backgroundColor = "#d9d9d9",
  className = "",
  color = "#8c8c8c",
  src,
  value = "",
}: P) {
  function initialConstructor(text: string = "") {
    var replaced = text.replace(/[^0-9a-z]/gi, " ");
    var splitted = replaced.replace(/  +/g, " ").split(" ");
    var result = splitted;

    var arr = result.map((e) => e[0]).join("");
    return arr.length > 1 ? arr.substring(0, 2) : arr + arr;
  }

  return (
    <div className={className}>
      <div style={{ backgroundColor }} className="image-generator">
        {src ? (
          <img src={src} alt={alt} />
        ) : (
          <div style={{ color }} className="image-initial">
            {initialConstructor(value)}
          </div>
        )}
      </div>
    </div>
  );
}
